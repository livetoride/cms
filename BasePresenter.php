<?php

use AntoninRykalsky as AR;
use Nette\Application\UI\Presenter;
use Nette\Environment;
use Tracy\Debugger;

abstract class BasePresenter extends Presenter  {

	/**
	 * @deprecated
	 * @var \Nette\Security\User
	 */
	var $user;

	/** @var  \SoftwareStudio\Entity\IUserEntity */
	public $userEntity;

	/** @var  \SoftwareStudio\SiteSpecifics\PresenterHandler */
	protected $presenterHandler;

	/** @var  callable[] */
	protected $injectedComponents=[];

	/** @var  \SoftwareStudio\Common\IProjectSpecifics */
	protected $projectSpecifics;

	/**
	 * Set to false whenever you want to use custom breadcrumb
	 * @var bool
	 */
	public $breadDefault = 1;

	protected function startup() {

		parent::startup();
		$this->presenterHandler->check();
	}

	protected function shutdown($response) {

		parent::shutdown($response);
		$this->presenterHandler->shutdown($response);
	}

	protected function createComponent($name) {

		// look for overided component first
		$originalControl = parent::createComponent($name);
		if( $originalControl != null ) {
			return $originalControl;
		}

		// check if not injected
		if( in_array($name, array_keys($this->injectedComponents))) {
			$callback = $this->injectedComponents[$name];
			return $callback();
		}

		Debugger::log(\SoftwareStudio\Common\StringUtils::message("Component [{}] may not does not exists", $name), Debugger::EXCEPTION );
	}

	public function formatLayoutTemplateFiles() {

		$layout = $this->layout ? $this->layout : 'layout';
	 	$return = array(
			APP_DIR."/modules/@$layout.latte",
			APP_DIR."/modules/@$layout.phtml",
			APP_DIR."/modules/@admin.latte",
			APP_DIR."/modules/CmsModule/@admin.latte",
			APP_DIR."/modules/@admin.phtml",
			);
	    $l = parent::formatLayoutTemplateFiles();
	    foreach( $l as $v ) $return[]=$v;
	    return $return;
	}

	/** @deprecated */
	public function tryCatcher( $functionName, $v, $debug = null, $okMessage = null ) {

		$tryCatcher = new AR\TryCatcher\TryCatcher();
		return $tryCatcher->tryCatcher($this, $functionName, $v, $debug, $okMessage );
	}

	/* injects *************************** */

	/**
	 * User entity available in every presetner
	 */
	public function injectUserEntity( SoftwareStudio\Entity\IUserEntity $userEntity) { $this->userEntity = $userEntity; }

	/**
	 * Add callback for injected component
	 *
	 * @param $k
	 * @param $injectedComponents
	 */
	public function addInjectedComponents( $k, $injectedComponents) { $this->injectedComponents[$k] = $injectedComponents; }

	/**
	 * Presenter handler that sets presenter by project definition.
	 *
	 * @param \SoftwareStudio\SiteSpecifics\PresenterHandler $presenterHandler
	 */
	public function injectPresenterHandler(\SoftwareStudio\SiteSpecifics\PresenterHandler $presenterHandler) {  $this->presenterHandler = $presenterHandler; }

	/**
	 * @return \SoftwareStudio\Common\IProjectSpecifics
	 */
	public function getProjectSpecifics() {

		return $this->projectSpecifics;
	}

	/**
	 * @param \SoftwareStudio\Common\IProjectSpecifics $projectSpecifics
	 */
	public function setProjectSpecifics($projectSpecifics) { $this->projectSpecifics = $projectSpecifics; }
}
