<?php

use AntoninRykalsky as AR,
	Nette\Application\UI,
	Nette\Environment,
	Nette\Utils\Html;


/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class MenuBaseControl extends UI\Control
{
    /** @var <array> drží seznam položek pro Control */
    public $items;

    /** @var <array> drží info o položkách Menu pro Control */
    public $structure;

	protected $cache;

	var $section;

	public function prepare()
	{
		// in case is allowed
        $this->addCaching();

		// remove by role
		$forbidden = $this->findForbidden( $this->items );
        $this->removeForbidden($forbidden, $this->items);

        $this->markUrlParams( $this->items );
	}

	public function addCaching()
	{
		// zjisti informace všech menu
		$p = $this->presenter->context->parameters;
		$allow = @$p['modulesSettings']['allowMenuCache'];
		// cache
        if ($allow) {
			$this->cache = Environment::getCache('menu_cache');
            if (!isset($this->cache['menu_cache'])) {
                $this->cache->save( 'menu_cache', AR\Menu::get()->menu_find_active() );
            }
            $items = $this->cache['menu_cache'];

		} else {
			$items = AR\Menu::get()->menu_find_active();
		}
		$this->items=$items;
	}


    /*
     * vrací popis aktuální stránky,
     * pro zlepšení jednoznačnosti stránky je dobré zde vést,
     * doplnkové parametry .. např. NewsPresenter .. News:default item
     */
    protected function findActualPage() {

        $actPg = $this->findBacklinkParam();

        // najde záznam z tbl.Menu aktualni stranky
        if(!isset($actPg['param'])) $actPg['param'] = null;
		$row = MenuDao::get()->findAll()->where('[link]=%s AND [param]=%s',$actPg['backlink'], $actPg['param'] )->fetchAll();


        if( !count($row)){
            // if nothing found by link, check wildcard for the presenter
            $actPg['backlink'] = preg_replace('/:([^:]+):([^$]+)/', ':$1:*', $actPg['backlink'] );
			$row = MenuDao::get()->findAll()->where('[link]=%s',$actPg['backlink'] )->fetchAll();
        }

        //  najde nejvyššího potomka aktualni stranky
        if($row['parent_id']<>0)
        $row = AR\Menu::get()->menu_find_root( $row['parent_id'] );

        return $row;
    }


    /**
     * Najde v poli záznamů tabulky menu, ty které nejsou povoleny
     * podle objektu ACL.
     * @param <type> $menu Pole záznamů z tabulky menu.
     * @return <type> Očištěné pole
     */
    protected function findForbidden( $menu )
    {
        $user = \Nette\Environment::getUser();


        $forbidden = array();

        $count_menu = count($menu);

        #for( $i=0; $i<$count_menu; $i++ )
		$roleTo = 'show';



		/**
		 * zobrazí výsledky z vrstvy ACL
		 */
		$debug = 0;

		if( $debug )

			echo '<table>
				<tr><td colspan=3>'.implode(', ', $user->getRoles() ).'</td></tr>
				<tr><td>ALLOW/DENY</td><td>Resource</td><td>Priviledge</td>
				</tr>';

        foreach( $menu as &$item)
        {
			#if ( $user->isAllowed('muj_strom', 'show'))
			#echo $item['resource'] . ' ' . $roleTo . ' ' . $user->isAllowed( $item['resource'], $roleTo ) . "<br />";
            // neřeš nepovolené a ACL
//			echo $user->isAllowed( $item['resource'], $roleTo ). '<br />';
//			echo $user->isAllowed( $item['resource'], 'show' ). '--<br />';
//			echo $item['resource']. '<br />';
//			echo $roleTo . '<br />';
			#exit;


			if ( 0 ) {
				echo '<tr><td>';
				echo $user->isAllowed( $item['resource'], $roleTo ) ? 'ALLOW' : 'DENY';
				echo '</td><td>';
				echo $item['resource']. '</td><td>';
				echo '</td><td>';
				echo $roleTo;
				echo '</td></tr>';
			}

			if( !$user->isAllowed( $item['resource'], $roleTo )) {
                array_push( $forbidden, $item['id_menu']);

				continue;
			}

            // dej dokupy Destination
            #$menu[$i]['destination'] = ucfirst($menu[$i]['link']);

            // pokud neni param unsetni zbytecny zaznam
            #if( $menu[$i]['param'] == ''){ unset($menu[$i]['param']); }

        }

		if( $debug )
			echo '</table>';

        # array_push( $forbidden, 0);
        return $forbidden;
    }


	function cmp($a, $b)
	{
		return strcmp($a["menu_order"], $b["menu_order"]);
	}

    // vytvor stromovou strukturu
    protected function getTreeStructure( $items, $active = 0, $menu_structure = array() ) {

		usort($items, array($this, 'cmp'));
        foreach( $items as $v ) {
            // pokud existuje potomek
            if( $v['parent_id'] <> $active ) continue;

			$sons = array();
			if( $v['last_rendered_automatically'] == 0 )
			{
				// ziska pole potomku
				$sons = $this->getTreeStructure( $items, $v['id_menu'] );
			}


         #   if( $v['parent_id'] <> 0 )
            // pridej do pole s potomky
         #   array_push( $menu_structure, $v['id_menu'] );
         #   else {
                // pridej do struktury
                if( !count($sons) )
                // list struktury
                array_push( $menu_structure, $v['id_menu'] );
                else
                // uzel struktury
                $menu_structure[$v['id_menu']] = $sons;
         #   }

            // some debug info
            if( 0 ){
                printf( "Rodič %d ma potomka %d ", $v['parent_id'], $v['id_menu'] );
                print_r( $sons );
                print_r( $menu_structure );
                echo "</br>";
            }
        }
        return $menu_structure;
    }

    /** @var <int> drží další krok pro generovani */
    public $nextItemToRender;

    /**
     * vraci informace o položce menu
     * využití v šabloně
     */
    public function getItemInfo( $r, $v )
    {
        if( is_array( $v )){
            $id=$r;
            $this->items[$id]['have_sons'] = 1;
        } else $id=$v;

        $item = $this->items[$id];

        if( $item['link']!='' )
        {
            if(isset( $item['param'] ))
                $this->items[$id]['url'] = $this->getPresenter()->link( $item['link'], $item['param'] );
            else
                $this->items[$id]['url'] = $this->getPresenter()->link( $item['link'] );
        }

        return $this->items[$id];
    }

	var $contents = array(
		'top-menu' => array(
			'link' => array('a' ),
			'text' => array('span' )
		),
		'client' => array(
			'link' => array('a' ),
			'text' => array('span' )
		)
	);


	public function generateTitle( $item )
	{
		if(empty( $item['menunadpis'] ))
			return $el->setText( $item['menu'] );
		else
			return $el->setText( $item['menunadpis'] );
	}

    /** generuje odkaz do šablony podle položky item */
    public function generateLink( $item )
    {
		$content = $this->contents[$this->section];
		if( $this->section == 'client')
		{
//			print_r( $content );exit;

			// pokud nejde o odkaz, vrať jen text, pripadně identifikator dalších položek
			if(isset( $item['link'])){
				foreach( $content['link'] as $sec )
				{
					$el = Html::el( $sec );

					if(empty( $item['menunadpis'] ))
						$el->setText( $item['menu'] );
					else
						$el->setText( $item['menunadpis'] );

					$el->href( $item['url'] );

				}
				return $el;
			} else {}

		}

        // definice odkazu
        $el = Html::el("a");

		$textOdkazu = $item['menunadpis'];
		if(empty( $textOdkazu ))
			$textOdkazu = $item['menu'];

        // pro odkazy které obsahují další podpoložky-potomky
        $text_suffix = Html::el("span");
        if(isset( $item['have_sons'])){
//            $text_suffix->setText(" »");
            #$text_suffix->class("sf-sub-indicator");

            #$text_suffix-> '<span class="sf-sub-indicator"> »</span>';
            #$el->class('accessible');
        }

		$text = Html::el("span");
		$text->setText( $textOdkazu );

        // pokud nejde o odkaz, vrať jen text, pripadně identifikator dalších položek
        if(isset( $item['url'])){
            $el->href( $item['url'] );
        } else {

			$text = Html::el("span");
		$text->setText( $textOdkazu );
			$text->class( 'separation' );
            return $text;
        }

        // popisek odkazu
//        $el->setText( $item['menu'] );
        $el->add( $text );
		$el->class (  "imMnItm" );


        return $el;
    }

    public function render( $section='main-menu' )
    {
		$this->section = $section;

		if( $this->presenter->isAjax() )
		{
			return;
		}
//        Debug::timer('menu-creating');

        // zjisti informace všech menu
//		$cache_config = Environment::getConfig('cache');
//        define('MENU_CACHING', $cache_config->menu);
//		// cache
//        if (MENU_CACHING) {
//			$this->cache = Environment::getCache('menu_cache');
//            if (!isset($this->cache['menu_cache'])) {
//                $this->cache->save( 'menu_cache', AR\Menu::get()->menu_find_active() );
//            }
//            $items = $this->cache['menu_cache'];
//
//		} else {
//
//		}
		$items = AR\Menu::get()->menu_find_active();
		$forbidden = $this->findForbidden( $items );


        $items = $this->removeForbidden($forbidden, $items);
        $items = $this->markUrlParams( $items );

		$sections['main-menu'] = 1;
		$sections['top-menu'] = 2;
		$sections['client'] = 4;
		$templates['main-menu'] = AR\Configs::get()->byContent('settings.template_mainmenu');
		$templates['top-menu'] = AR\Configs::get()->byContent('settings.template_topmenu');
		$templates['client'] = AR\Configs::get()->byContent('settings.template_membermenu');

        // zjisti info o struktuře
        $structure = $this->getTreeStructure( $items, $sections[$section] );

        // předej do controls
        $this->structure = $structure;
        $this->items = $items;

		if( empty($structure)) return;

        // vykresli control
//        $this->template->setFile(dirname(__FILE__) . '/banskaliga.phtml');
        $this->template->setFile(dirname(__FILE__) .'/'. $templates[$section]);
        $this->template->render();

//        Environment::setVariable('menu-creating', Debug::timer('menu-creating') * 1000);

    }



	public function renderSubmenu() {
        // vykresli control
		$action = $this->presenter->getAction(TRUE);
		$items = array();

		if( $action == ':Front:News:reference') $action = 'News:reference';

//		print_r( $this->presenter->getParam('item') );exit;
//		print_r( $action );exit;
//		print_r( $this->structure );exit;


		if( $action == ':Front:News:show')
		{
			$param = $this->presenter->getParam('item');
			$m = AR\Menu::get()->findAll()->where('[link]=%s AND [param]=%s', $action, $param )->fetchAll();
		} else {
			$m = AR\Menu::get()->findAll()->where('[link]=%s', $action )->fetchAll();
		}



		// && empty($this->fakeId)
		// dohledame pĹ™edka aĹľ aby mÄ›l parent_id 0
		if( count( $m ) ) {
			if( $m[0]['parent_id'] != 0 )
				$m = AR\Menu::get()->find( $m[0]->parent_id )->fetchAll();
			if( $m[0]['parent_id'] != 0 )
				$m = AR\Menu::get()->find( $m[0]->parent_id )->fetchAll();
		}


		$items = AR\Menu::get()->menu_find_active();


		$forbidden = $this->findForbidden( $items );


        $items = $this->removeForbidden($forbidden, $items);
        $items = $this->markUrlParams( $items );


		$user = \Nette\Environment::getUser();
		if( count( $items ))
		for( $i=0; $i<=count($items); $i++)

			if( !$user->isAllowed( @$items[$i]['id_resource'], 'show' )) {
//                echo 'odstranuji '.$items[$i]['id_resource'].' '.$items[$i]['menu']."<br />";
				unset( $items[$i] );


			}


		if(!empty( $this->fakeId ))
		{
			$menuId = $this->fakeId;
		} else {
			if(!count( $m ) ) return;
			$menuId = @$m[0]->id_menu;
		}



		// zjisti info o struktuĹ™e
        $structure = $this->getTreeStructure( $items, $menuId );
//print_r( $structure );exit;
        // pĹ™edej do controls
        $this->structureSub = $structure;
//		print_r( $structure );exit;


        $this->template->setFile(dirname(__FILE__) . '/submenu.phtml');
        $this->template->render();
    }

	var $structureSub;


    protected function markUrlParams( $menu ){
        foreach( $menu as &$item)
        if( $item['param'] == ''){ unset($item['param']); }
        return $menu;
    }





    /**
     * Odstraní z pole záznamů tabulky menu zakázané položky
     * @param <type> $forbidden pole zakázaných položek
     * @param <type> $items pole záznamů tabulky menu
     * @return <type> $items očištěné pole
     */
    protected function removeForbidden( $forbidden, $items ){
        $forbidden = array_flip($forbidden);
        $items = $this->items = array_diff_key($items, $forbidden);
        return $items;
    }
}
