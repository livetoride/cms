<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette;

/**
 * @ORM\Table(name="gallery_images")
 * @ORM\Entity
 */
class Images extends Nette\Object
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_image", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="gallery_images_id_image_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
	 * @ORM\Column(name="image_path", type="string", length=64, nullable=true)
	 */
	private $imagePath;

	/**
	 * @ORM\Column(name="short_text", type="string", nullable=true)
	 */
	private $shortText;

	/**
	 * @ORM\Column(name="image_order", type="integer", nullable=true)
	 */
	private $imageOrder;

	/**
	 * @ORM\manyToOne(targetEntity="Carousel", inversedBy="images")
	 * @ORM\joinColumn(name="id_gallery", referencedColumnName="id_gallery")
	 */
	private $carousel;

	public function getId() {
		return $this->id;
	}

	public function getImagePath() {
		return $this->imagePath;
	}

	public function getShortText() {
		return $this->shortText;
	}

	public function getImageOrder() {
		return $this->imageOrder;
	}

	public function getCarousel() {
		return $this->carousel;
	}

	public function setCarousel($carousel) {
		$this->carousel = $carousel;
	}

	public function setImage($imagePath) {
		$this->imagePath = $imagePath;
		$this->imageOrder = 0;
	}

	public function setShortText($shortText) {
		$this->shortText = $shortText;
	}






}
