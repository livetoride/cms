<?php

use AntoninRykalsky as AR,
	Nette\Application\UI,
	Nette\Utils\Html;


/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class GalleryControl extends UI\Control implements \AntoninRykalsky\IControl
{
	var $id_gallery;
	var $id_menu;

	public function setMenuId( $id_menu )
	{
		$this->id_menu = $id_menu;
	}

	public function setGallery( $id_gallery )
	{
		$this->id_gallery = $id_gallery;
	}

	private function _renderByMenu( $section='main-menu' )
	{
		echo "ok";exit;
		$m = AR\Menu::get()->find($this->id_menu)->fetch();

		$r = AR\Gallery::get()->findAll()->where('id_menu=%i', $m->id_menu )->fetchAll();
		$ri = AR\GalleryImages::get()->imagesByMenu($m->id_menu)->orderBy('image_order')->fetchAll();
		$rg = AR\GalleryGroup::get()->find($m->id_menu)->fetch();

		$this->template->title = @$rg->html_title;
		$this->template->htmldesctiption = @$rg->html_description;
		$this->template->htmlkeywords = @$rg->html_keywords;

		$ri2=array();
		foreach( $ri as $v)
		{
			$ri2[ $v['id_gallery'] ][] = $v;
		}

		$this->template->ref = $r;
		$this->template->ri = $ri2;
		$this->template->rg = $rg;

        $this->template->setFile(dirname(__FILE__) .'/gallery.phtml');
        $this->template->render();
	}

	public function setTypeParamByCms( $param )
	{
		$this->id_gallery = $param;
	}

	/** links required css and js */
	public function getRequired()
	{
		 return array(
			  'dir' => __DIR__,
			  'class' => preg_replace('#\\\#', '', __CLASS__),
			  'css' => array('gallery.css'),
			  'js' => array()
		 );
	}

	public function doStyles()
	{
		$template = $this->presenter->createTemplate();
		$template->setFile( dirname(__FILE__).'/includeStyles.latte');
		$this->presenter->template->htmlfoot = $template->__toString();
	}


    public function render(  )
    {
		echo "ok";exit;

		if( is_numeric($this->id_menu))
		{
			return $this->_renderByMenu( );
		}
		if( !is_numeric($this->id_gallery) )
		{
			return;
		}

		$r = AR\Gallery::get()->find( $this->id_gallery )->fetchAll();
		$ri = \DAO\GalleryImages::get()->findAll()->where('id_gallery=%i', $this->id_gallery )->orderBy('image_order')->fetchAll();
		//$rg = AR\GalleryGroup::get()->find($m->id_menu)->fetch();

		$this->template->title = @$rg->html_title;
		$this->template->htmldesctiption = @$rg->html_description;
		$this->template->htmlkeywords = @$rg->html_keywords;

		$ri2=array();
		foreach( $ri as $v)
		{
			$ri2[ $v['id_gallery'] ][] = $v;
		}

		$this->template->ref = $r;
		$this->template->ri = $ri2;
		$this->template->rg = array();#$rg;

        $this->template->setFile(dirname(__FILE__) .'/gallery.phtml');

		$this->doStyles();
        $this->template->render();
    }
}
