<?php
use Nette\Application\UI;

class CachingControl extends UI\Control
{
    public function render( $section='category' )
    {
		$p = $this->presenter->context->parameters;
		@$phpunit = $p['phpunit'];
		if( $phpunit )
			return;
		$this->template->setFile(dirname(__FILE__).'/caching.latte');
		$this->template->allowCache = @$p['modulesSettings']['allowHttpCache'];
		$this->template->render();
	}
}
