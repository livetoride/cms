<?php

namespace AntoninRykalsky;

use AntoninRykalsky as AR,
	Nette\Application\UI,
	Nette\Utils\Html;


/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class GoogleAnaliticsControl extends UI\Control
{
	private $account='';
	private $domain ='';
	
    public function render( )
    {
		if( $_SERVER['SERVER_NAME']=='localhost' ) return;
        $this->template->setFile(dirname(__FILE__) .'/ga.latte');
		$this->template->account = $this->account;
		$this->template->domain = $this->domain;
        $this->template->render();
    }
	
	public function setAccount( $account )
	{
		$this->account = $account;
	}
	
	public function setDomain( $domain )
	{
		$this->domain = $domain;
	}
	
	
}
