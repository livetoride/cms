<?php
use Nette\Application\UI;
use AntoninRykalsky as AR;

class MetaControl extends UI\Control
{
    public function render()
    {
		$p = $this->presenter->context->parameters;
		$this->template->setFile(dirname(__FILE__).'/meta.latte');
		
		if( !empty( $this->presenter->template->title ))
			$this->template->title = $this->presenter->template->title;


		if( !empty($this->presenter->template->htmltitle))
			$this->template->htmltitle = $this->presenter->template->htmltitle;
		if( !empty($this->presenter->template->htmldesctiption))
			$this->template->htmldesctiption = $this->presenter->template->htmldesctiption;
		else
			$this->template->htmldesctiption = @$this->presenter->template->htmltitle;
		if( !empty($this->presenter->template->htmlkeywords))
			$this->template->htmlkeywords = @$this->presenter->template->htmlkeywords;
		if( !empty($this->presenter->template->htmlrobots))
			$this->template->htmlrobots = @$this->presenter->template->htmlrobots;

		$denied = array('Antonín Rykalský');
//		if( in_array( $this->presenter->template->title, $denied ))
//		{
//			$this->template->suffix = '';
//
//		} else {
			$eshop = AR\Configs::get()->byContent('general.eshop_suffix');
			$this->template->suffix = $eshop;
//		}
		$this->template->render();
	}
}
