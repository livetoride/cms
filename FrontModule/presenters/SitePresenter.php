<?php
/**
 * Zobrazování novinek
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace FrontModule;

use AntoninRykalsky;
use AntoninRykalsky\Entity\CmsSite;
use Doctrine\ORM\EntityManager;
use SoftwareStudio\Entity\IAdminUser;

class SitePresenter extends \BasePresenter {

	/** @var AntoninRykalsky\SiteFacade */
	protected $siteFacade;

	/** @var AntoninRykalsky\GridFacade */
	protected $gridFacade;

	/** @var  EntityManager */
	private $em;

	function injectCmsStaff(
			AntoninRykalsky\SiteFacade $siteFacade, AntoninRykalsky\EntityManager $em ) {
		$this->siteFacade = $siteFacade;
		$this->em = $em->getEm();
	}
	
	public function createComponentSiteEditControl()
	{
		return new \SoftwareStudio\SiteControl();
	}

	public function handleSetCmsIconPosition( $right, $top )
	{
		setcookie('cms-icon-position-r', $right, time() + (31536000 * 30), "/"); // 86400 = 1 day
		setcookie('cms-icon-position-t', $top, time() + (31536000 * 30), "/"); // 86400 = 1 day
	}

	public function handleSetBarHidden( $visible )
	{
		setcookie('cms-bar-visible', $visible, time() + (31536000 * 30), "/"); // 86400 = 1 day
	}
	
	public function doAdminStuff( $id )
	{

//		if(!$this->userEntity instanceof IAdminUser) {
//			return;
//		}
		if( !empty( $_COOKIE['cms-icon-position-r'] ))
		{
			$this->template->cmsIconPositionR = $_COOKIE['cms-icon-position-r'];
		} else {
			$this->template->cmsIconPositionR = '10px';
		}
		if( !empty( $_COOKIE['cms-icon-position-t'] ))
		{
			$this->template->cmsIconPositionT = $_COOKIE['cms-icon-position-t'];
		} else {
			$this->template->cmsIconPositionT = '10px';
		}

		if( !empty( $_COOKIE['cms-bar-visible'] ))
		{
			$this->template->cmsBarVisible = $_COOKIE['cms-bar-visible'];
		} else {
			$this->template->cmsBarVisible = false;
		}

		if(!is_numeric( $id ))
		{
			$site = \DAO\CmsSite::get()->findAll()->where('url=%s', $id )->fetch();
			if( !empty( $site->id ))
				$id = $site->id;
		}
		if( empty( $id ))
		{
			$site = \DAO\CmsSite::get()->findAll()->where('homepage=1')->fetch();
			if( !empty( $site->id ))
				$id = $site->id;
		}
		$site = AntoninRykalsky\Cms\Site::getBySite($id);
		$siteEntity = $this->em->find(CmsSite::class, $id );
		$this['siteEditControl']->setSite( $siteEntity );
		
		$this->template->isAdmin = 1;
		$this->template->siteUrl = $siteEntity->getUrl();
	}

	public function handleSaveItems() {

//		if( !\AntoninRykalsky\SystemUser::get()->isAdmin()) {
//			return;
//		}
		
		$stuff = $this->getParameter('stuff');
		
		foreach( $stuff as $item )
		{
			if( !empty( $item['id'] ))
			{
				$g = \DAO\CmsGrid::get()->find( $item['id'] )->fetch();
				if( $g->type === 1 )
				{
					\DAO\CmsArticle::get()->update( $g->type_id, array('article'=>$item['content'] ) );
				}
			}
		}
	}
	
	// <editor-fold defaultstate="collapsed" desc=" CMS2.0 minimal">

	/** @var \AntoninRykalsky\Cms\SiteControlFactory */
	protected $siteControlFactory;

	function injectSiteControlFactory(\AntoninRykalsky\Cms\SiteControlFactory $siteControlFactory) {
		$this->siteControlFactory = $siteControlFactory;
	}


	protected function createComponentSiteControl() {
		/** @var AntoninRykalsky\Cms\SiteControl $siteControl */
		$siteControl = $this->siteControlFactory->create($this, 'siteControl');
		$siteId = $this->getParameter('id');
		$siteControl->setSiteId($siteId);
		return $siteControl;
	}


	public function actionDefault($id = '') {
		$this['siteControl']; # need to create component in action phase
		$this->doAdminStuff( $id );
	}

	// </editor-fold>

}