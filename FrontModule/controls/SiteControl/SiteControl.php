<?php

/**
 * Nette Framework Extras
 *
 * This source file is subject to the New BSD License.
 *
 * For more information please see http://extras.nettephp.com
 *
 * @copyright  Copyright (c) 2009 David Grudl
 * @license    New BSD License
 * @link       http://extras.nettephp.com
 * @package    Nette Extras
 * @version    $Id: VisualPaginator.php 4 2009-07-14 15:22:02Z david@grudl.com $
 */

namespace AntoninRykalsky\Cms;
use AntoninRykalsky as AR;
use AntoninRykalsky\CmsReplacing;
use AntoninRykalsky\Entity\CmsGrid;
use AntoninRykalsky\Entity\CmsSite;
use AntoninRykalsky\SiteFacade;
use Doctrine\ORM\EntityManager;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Control;
use SoftwareStudio\Common\StringUtils;
use Tracy\Debugger;

class SiteControlFactory extends AR\ControlFactory {
	protected $className = 'AntoninRykalsky\Cms\SiteControl';

}

class SiteControl extends Control
{
	public function __construct(
			$parent,
			$name,
			SiteFacade $siteFacade,
			CmsReplacing $cmsReplacing,
			\CmsControlsService $imsControlsService
			,SiteControlConfigService $siteControlConfigService
			,SiteWidthService $siteWidthService
			,AR\EntityManager $em
	) {
		$this->siteFacade = $siteFacade;
		$this->cmsReplacing = $cmsReplacing;
		$this->cmsControlsService = $imsControlsService;
		$this->siteControlConfigService = $siteControlConfigService;
		$this->siteWidthService = $siteWidthService;
		$this->em = $em->getEm();
		parent::__construct( $parent, $name );
	}

	/** @var \AntoninRykalsky\SiteFacade */
	protected $siteFacade;

	/** @var \AntoninRykalsky\CmsReplacing */
	protected $cmsReplacing;

	/** @var \CmsControlsService */
	protected $cmsControlsService;

	/** @var  SiteControlConfigService */
	private $siteControlConfigService;

	/** @var SiteWidthService */
	private $siteWidthService;

	/** @var EntityManager */
	private $em;


	public function render() {

		$this->template->setFile(dirname(__FILE__) . '/site.latte');
		$this->template->getLatte()->addFilter('loader', 'FormatingHelpers');
		$this->template->render();
	}

	public function setSiteId($siteId) {

		/* @var $site \AntoninRykalsky\Entity\CmsSite */
		$site = $this->em->getRepository(CmsSite::class)->getSiteByLink( $siteId );

//		$site = $this->siteFacade->getSiteByLink($siteId);

		if( empty( $site ))
			throw new BadRequestException('Hledaná stránka neexistuje');;

		$this->fillPresenterTemplate( $site );

		CmsGrid::$presenter = $this->presenter;

		$wrapperId=1;
		$wrappers=array();
		$wrappers[] = (object)array('id_wrapper'=>$wrapperId);

		$this->siteControlConfigService->addControlServiceInto( $site );
		$return = $this->siteWidthService->addControlServiceInto( $site );

		$this->template->wrappers = $return[0]; // wrappers
		$this->template->grids = $return[1]; // templateG

		$this->runComponent();

	}

	public function setGridId( $idGrid )
	{
		$grid = $this->siteFacade->getGrid($idGrid);

		CmsGrid::$presenter = $this->presenter;

		$this->siteControlConfigService->addControlServiceIntoG( $grid );
		$return = $this->siteWidthService->addControlServiceIntoG( $grid );

		$this->template->wrappers = $return[0]; // wrappers
		$this->template->grids = $return[1]; // templateG

		$this->runComponent();
	}

	/**
	 * Used for physical creation of component.
	 * @throws \Exception
	 */
	private function runComponent() {

		// spuštění createComponent
		$uc = CmsGrid::$usedControlsKeys;
		foreach( $uc as $iuc => $cmsGrid ) {

			try {
				// fyzické vytvoření komponenty
				$cmsGrid->createComponent( $this, $iuc );

				Debugger::log(StringUtils::message("Created component [{}] / [{}]", $iuc, get_class($cmsGrid) ));

				ob_start();
				$cmsGrid->render();
			} catch( \Exception $e )
			{
				throw $e;
			}
			ob_clean();
		}
	}

	private function fillPresenterTemplate( $site ) {

		$presenterTemplate = $this->presenter->template;
		$presenterTemplate->dontrenderh1 = $site->getDontRenderTitle();

		if( !$site->getDontRenderTitle() ) {
			$presenterTemplate->title = $site->getSite();
		}

		$presenterTemplate->htmltitle = $site->getTitle();
		$presenterTemplate->htmldescription = $site->getDescription();
		$presenterTemplate->htmlkeywords = $site->getKeywords();
	}

}
