<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette;

/**
 * @ORM\Table(name="cms_embed_media")
 * @ORM\Entity(repositoryClass="AntoninRykalsky\EmbedMediaRepository")
 */
class EmbedMedia extends Nette\Object
{
	const YOUTUBE_VIDEO = 1;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cms_embed_media_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;
	
	/**
	 * @ORM\Column(name="media_type", type="integer", nullable=true)
	 */
	private $mediaType;
	
	/**
	 * @ORM\Column(name="link", type="string", length=255, nullable=true)
	 */
	private $link;

	public function setYoutubeVideo( $link ) {

		$this->mediaType = self::YOUTUBE_VIDEO;
		$this->link = $link;
	}
		
	public function getId() {
		return $this->id;
	}

	public function getMediaType() {
		return $this->mediaType;
	}

	public function getLink() {
		return $this->link;
	}
	
	public function isOk() {
		return !empty( $this->link );
	}
	
	
}
