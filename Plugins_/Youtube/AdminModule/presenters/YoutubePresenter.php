<?php

namespace AdminModule\RozvadimeseModule;


use Nette\Application\UI\Form;
use Nette\Utils\Html;


class YoutubePresenter extends \BasePresenter
{
	protected $embedMediaRepository;

	function injectPartner(\AntoninRykalsky\EntityManager $em) {
		$em = $em->getEm();
		$this->embedMediaRepository = $em->getRepository("AntoninRykalsky\Entity\EmbedMedia");
	}
	
	public function ActionEdit($id) {
		
		$this->template->title = 'Nastavení youtube videa';
		
		$video = $this->embedMediaRepository->find( $id );
		$this->template->video = $video;
		if( $video->isOk() )
		{
			$defaults['video'] = 'http://www.youtube.com/watch?v=' . $video->getLink(); 
			$this['pollForm']->setDefaults( $defaults );
		}
	}
	
	
	
	protected function createComponentYoutube() {
		$y = new \YoutubeControl();
		$id=$this->getParameter('id');
		$video = $this->embedMediaRepository->find( $id );
		if( $video->isOk() ) {
			$y->setTypeParamByCms( $id);
		}
		return $y;
		
	}
	protected function createComponentPollForm() {
		$form = new Form();
		$form->addTextArea('video', 'Otázka');

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedPollForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}
	


	function submitedPollForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = (array) $form->getValues();
			
			preg_match('#watch\?v=([^$&]*)#', $v['video'], $matches );
			if( !empty( $matches[1] ))
			{
				$id = $this->getParameter('id');
				
				$this->embedMediaRepository->storeYoutube( $id, $matches[1] );
				
				
				$this->flashMessage("Uloženo", \Flashes::$success );
			} else {
				$this->flashMessage("Nepodařilo se rozpoznat video. Zkontrolujte adresu nebo kontaktuje servis.", \Flashes::$error );
				return;
			}
			$this->redirect('this');
					
		}
	}
	
}
