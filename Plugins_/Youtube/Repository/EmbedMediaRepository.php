<?php
namespace AntoninRykalsky;
use Doctrine\ORM\EntityRepository;

class EmbedMediaRepository extends EntityRepository {

	public function storeYoutube( $id, $youtubeParam ) {
		
		$video = $this->find( $id );
		$video->setYoutubeVideo( $youtubeParam );
		$this->_em->flush();
		return $video;
	}

}
?>