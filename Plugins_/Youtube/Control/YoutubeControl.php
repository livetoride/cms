<?php

use AntoninRykalsky as AR, 
	Nette\Application\UI,
	Nette\Utils\Html;


/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class YoutubeControl extends UI\Control
{
    public function render( )
    {
    	$context = \Nette\Environment::getContext();
		$em = $context->getByType("AntoninRykalsky\EntityManager")->getEm();
		
		
		$yk = $em->getRepository('\AntoninRykalsky\Entity\EmbedMedia')->find($this->youtubeKey);
		$key = $yk->getLink();
		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->setFile(dirname(__FILE__) . '/youtube.latte');
		$this->template->youtubeKey = $key;
		$this->template->render();
    }
	
	private $youtubeKey;
	public function setTypeParamByCms( $youtubeKey )
	{
		$this->youtubeKey = $youtubeKey;
	}

}
