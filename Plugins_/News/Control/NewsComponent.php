<?php

use AntoninRykalsky as AR, 
	Nette\Application\UI,
	Nette\Utils\Html;


/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */

class NewsControl extends UI\Control
{
	private $key;
    public function render( )
    {
    	$context = \Nette\Environment::getContext();
		$em = $context->getByType("AntoninRykalsky\EntityManager")->getEm();
	/*	$yk = $em->getRepository('\AntoninRykalsky\Entity\EmbedMedia')->find($this->youtubeKey);
		$key = $yk->getLink();
		*/
		//$news = $em->getRepository('\Entity\News')->findByGroup($this->key);
		$qb = $em->createQueryBuilder();
		$qb->select( 'a' )
   ->from( '\Entity\News', 'a' )
	->where('a.group = :group')
   ->orderBy( 'a.tsInsert', 'DESC' );
   $qb->setParameter('group', $this->key);
   $news = $qb->getQuery()->getResult();   
		
		

		$this->template->registerHelperLoader('FormatingHelpers::loader');
		$this->template->setFile(dirname(__FILE__) . '/news.latte');
		$this->template->news = $news;
		$this->template->render();
    }
	
	
	public function setTypeParamByCms( $key )
	{
		$this->key = $key;
	}

}
