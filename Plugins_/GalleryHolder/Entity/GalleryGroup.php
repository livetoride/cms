<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="gallery_group")
 * @ORM\Entity
 */
class GalleryGroup
{

	
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="gallery_group_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;
	
	/**
	 * @ORM\Column(name="ts_insert", type="datetime", nullable=true)
	 */
	private $tsInsert;
	
	/**
	 * @ORM\Column(name="name", type="string", length=256, nullable=true)
	 */
	private $name;

	
	/**
	 * @ORM\OneToMany(targetEntity="Gallery", mappedBy="group")
	 */
	private $items;
	
	/**
	 * @ORM\OneToMany(targetEntity="GallerySite", mappedBy="group")
	 * @ORM\OrderBy({"order" = "ASC"})
	 */
	private $sites;
	
	public function __construct() {
		$this->tsInsert = new \DateTime();
	}
	
	public function setName($name) {
		$this->name = $name;
	}

		
	public function getId() {
		return $this->id;
	}
	
	public function getTsInsert() {
		return $this->tsInsert;
	}
	
	public function getName() {
		return $this->name;
	}

	public function getItems() {
		return $this->items;
	}
	
	public function getSites() {
		return $this->sites;
	}


	
	
}
