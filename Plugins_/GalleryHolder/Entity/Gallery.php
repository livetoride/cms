<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette;

/**
 * @ORM\Table(name="gallery")
 * @ORM\Entity
 */
class Gallery extends Nette\Object
{
	
	static $baseUri;
	
    /**
     * @var integer
     *
     * @ORM\Column(name="id_gallery", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="gallery_id_gallery_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
	 * @ORM\Column(name="name", type="string", length=64, nullable=true)
	 */
	private $name;

	/**
	  * @ORM\OneToMany(targetEntity="Images", mappedBy="carousel")
	  */
	private $images;

	/**
	 * @ORM\ManyToOne(targetEntity="GalleryGroup", inversedBy="group")
	 * @ORM\JoinColumn(name="holder_id", referencedColumnName="id")
	 */
	private $group;
	
	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getImages() {
		return $this->images;
	}


	public function setGallery() {
		$this->name = 'Nová galerie';
	}

	public function getHolder() {
		return $this->holder;
	}

	public function setHolder($holder) {
		$this->holder = $holder;
	}

	public function getUrl() {
		return self::$baseUri . 'asdf';
	}
	
	public function getImage() {
		foreach( $this->images as $image )
		{
			return self::$baseUri . '/images/gallery/small/' . $image->getImagePath();
//			\Doctrine\Common\Util\Debug::dump($image);
//			exit;
		}
	}







}
