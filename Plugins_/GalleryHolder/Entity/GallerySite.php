<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette;

/**
 * @ORM\Table(name="gallery_site")
 * @ORM\Entity
 */
class GallerySite extends Nette\Object
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="gallery_site_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="GalleryGroup", inversedBy="sites")
	 * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
	 */
	private $group;

	/**
	 * @ORM\Column(name="url", type="string", length=128, nullable=true)
	 */
	private $url;

	/**
	 * @ORM\Column(name="image", type="string", length=128, nullable=true)
	 */
	private $image;
	
	/**
	 * @ORM\Column(name="`order`", type="integer", nullable=true)
	 */
	private $order;
	
	/**
	 * @ORM\Column(name="name", type="string", length=128, nullable=true)
	 */
	private $name;

	public function getId() {
		return $this->id;
	}

	public function getGroup() {
		return $this->group;
	}

	public function getUrl() {
		return $this->url;
	}

	public function getImage() {
		return $this->image;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function init($name, $group, $url, $image, $order ) {
		$this->name = $name;
		$this->group = $group;
		$this->url = $url;
		$this->image = $image;
		$this->order = $order;
	}
	
	public function edit( $name, $url, $image ) {
		$this->name = $name;
		$this->url = $url;
		$this->image = $image;
	}
	
	public function setOrder($order) {
		$this->order = $order;
	}



}
