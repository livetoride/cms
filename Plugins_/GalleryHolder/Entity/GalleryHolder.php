<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="gallery_holder")
 * @ORM\Entity
 */
class GalleryHolder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="gallery_holder_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	
	/**
	 * @ORM\Column(name="ts_insert", type="datetime", nullable=true)
	 */
	private $tsInsert;

	public function __construct() {
		$this->tsInsert = new \DateTime();
	}
	
	public function getId() {
		return $this->id;
	}
	
	public function getTsInsert() {
		return $this->tsInsert;
	}


	
	
	

}
