<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="gallery_holder_item")
 * @ORM\Entity
 */
class GalleryHolderItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="gallery_holder_item_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
	 * @ORM\manyToOne(targetEntity="CmsSite"	)
	 * @ORM\joinColumn(name="site_id", referencedColumnName="id_site")
	 */
	private $site;
	
	/**
	 * @ORM\Column(name="image", type="string", length=128, nullable=true)
	 */
	private $image;
	
	public function getId() {
		return $this->id;
	}

	public function getSite() {
		return $this->site;
	}

	public function getImage() {
		return $this->image;
	}

	public function setItem($site, $image) {
		$this->site = $site;
		$this->image = $image;
	}
}
