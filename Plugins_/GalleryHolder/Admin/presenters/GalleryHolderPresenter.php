<?php
/**
 * Controller referencí pro Ingenia.cz
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2011 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule;
use Nette\Application\UI\Form;

class GalleryHolderPresenter extends \BasePresenter
{
	/** @var \AntoninRykalsky\EntityManager */
	protected $em;
	
	public function __construct(
		\AntoninRykalsky\EntityManager $em
	){
		$this->em = $em->getEm();
	}

		
	
	public function actionDefault( $id ) 
	{
		$this->template->title = "Nastavení skupiny galerií";
		$group = $this->em->find("AntoninRykalsky\Entity\GalleryGroup", $this->getParameter( 'id' ));
		$this['nameForm']->setDefaults(array(
			'name' => $group->getName()
		));
	}

	protected function createComponentTreeControl()
    {
		$group = $this->em->find("AntoninRykalsky\Entity\GalleryGroup", $this->getParameter( 'id' ));
		$sites = $group->getSites();
		
		$holder = new \AntoninRykalsky\GalleryHolderCategoryControl( );
		$holder->setSites( $sites );
		
		return $holder;
	}
	
	protected function createComponentNameForm() {
		$form = new Form();
//		$renderer = $form->getRenderer();

		$form->addText('name', 'Jméno této skupiny');

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedNameForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}


	function submitedNameForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			$group = $this->em->find("AntoninRykalsky\Entity\GalleryGroup", $this->getParameter( 'id' ) );
			$group->setName( $v->name );
			$this->em->flush();
			
			$this->flashMessage("OK", \Flashes::$success );
			$this->redirect("this");
		}
	}
	
	protected function createComponentHolderForm() {
		$form = new Form();
//		$renderer = $form->getRenderer();

		$form->addHidden('id');
		$form->addText('name', 'popisek');
		$form->addText('url', 'URL galerie');
		$form->addText('image', 'obrázek galerie');
		
		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedHolderForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}
			
	function submitedHolderForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = $form->getValues();
			
			
			
			if( !empty( $v->id ))
			{
				$gallerySite = $this->em->find("AntoninRykalsky\Entity\GallerySite", $v->id );
				$gallerySite->edit( $v->name, $v->url, $v->image );
			}
			
			else 
			{
				$group = $this->em->find("AntoninRykalsky\Entity\GalleryGroup", $this->getParameter("id"));
				$sites = $group->getSites();
				$i=1;
				foreach( $sites as $s )
				{
					$i++;
					$s->setOrder( $i );
				}

				$g = new \AntoninRykalsky\Entity\GallerySite();
				$g->init( $v->name, $group, $v->url, $v->image, 1 );

				$this->em->persist( $g );
			}
			
			$this->em->flush();
			
			$this->flashMessage("OK", \Flashes::$success );
			$this->redirect("this");
		}
	}
	
	public function handleEdit( $galerySiteId )
	{
		$siteGallery = $this->em->find("AntoninRykalsky\Entity\GallerySite", $galerySiteId );
		$this["holderForm"]->setDefaults(array(
			'id' => $siteGallery->getId(),
			'url' => $siteGallery->getUrl(),
			'image' => $siteGallery->getImage(),
			'name' => $siteGallery->getName(),
		));
		$this->invalidateControl();
	}
	
	public function handleDelete( $galerySiteId )
	{
		
		$siteGallery = $this->em->getReference("AntoninRykalsky\Entity\GallerySite", $galerySiteId );
		$this->em->remove( $siteGallery );
		$this->em->flush();
		$this->invalidateControl();
	}
	
	public function handleMenuorder()
	{
		$data = $this->getParameter('data');
		//$data = unserialize($data);
		
		foreach( $data as $v )
		{
			if(is_numeric($v['item_id']))
			{
				$id_menu = $v['item_id'];
				$parent = (is_numeric($v['parent_id']) ? $v['parent_id'] :  0);
				$structure[$parent][] = $id_menu;
			}
		}

		foreach( $structure as $parent_id => $v )
		{
			foreach( $v as $order => $id_menu )
			{
				$gs = $this->em->find("\AntoninRykalsky\Entity\GallerySite", $id_menu );
				$gs->setOrder( $order + 1 );
				
			}
		}

		$this->em->flush();
		$this->terminate();
	}


}
