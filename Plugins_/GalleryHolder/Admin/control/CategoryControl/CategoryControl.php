<?php

namespace AntoninRykalsky;

use	Nette\Application\UI;

use Nette\Utils\Html; #for helpers

class GalleryHolderCategoryControl extends UI\Control
{

	private $sites;
	
	public function setSites($sites) {
		$this->sites = $sites;
	}

	
    private function fullfillTemplate()
	{
//		$context = \Nette\Environment::getContext();
//		$r = $context->getService("router");
		
//		\Doctrine\Common\Util\Debug::dump($r);
//		exit;
//		
		$this->template->items = $this->sites;
//		foreach( $this->sites as $s )
//		{
//			print_r( $s->getUrl() );
//			exit;
//		}
		
		return;
		
		$this->template->menuitem = array(0,1);
		
		$this->menuFacade->getStructured();
		
		



		$mi = $this->template->menuitem = Menu::get()->getMenuItems();
		
//		print_r( $mi );

		if( $this->adminMode )
		{
			$this->template->mainSection = $this->menuFacade->getCompleteMainSections();
			$this->template->parents = Menu::get()->getCompleteChildNodes(0);
//			$this->template->menuitem = Menu::get()->getMenuItems(0);
			$this->template->id_resource = null;
		} else {
			$res = array("news-member", "news-frontend", "guest-only");
			$this->template->mainSection = $this->menuFacade->getMainSections();
			$this->template->parents = Menu::get()->getChildNodes(0, $res);
//			$this->template->menuitem = Menu::get()->getMenuItems(0);
			$this->template->id_resource = $res;
		}

		$this->template->nodes = Menu::get();
		$this->template->primary = 'id_menu';

		$this->template->section = 'frontend';
	}

    public function render()
    {
		$this->fullfillTemplate();

		$this->template->addFilter('CmsActions', array($this, 'cmsActions') );
		$this->template->addFilter('CmsIcon', array($this, 'CmsIcon') );
		
        $this->template->setFile(dirname(__FILE__) .'/category.phtml');
        $this->template->render();
    }

	private $adminMode = 0;
	public function adminMode($adminMode) {
		$this->adminMode = $adminMode;
	}
	
	public function CmsIcon($child, $entity,  $baseUri)
    {
		$el = Html::el('img');
		$el->class[] = 'hastip';

		if( !empty( $child->id_category ))
		{
			return;
		}

		if( $child->homepage == 1 ){
			$el->src[] = $baseUri.'homepage.png';
			$el->title[] = "Homepage";
			return $el;
        }
        if( $entity->getLink() == ':Front:News:show' && $child->id_resource == 'news-member'){
			$el->src[] = $baseUri.'users.gif';
			$el->title[] = "Položka pouze pro klienty";
			return $el;
        }
		elseif( $entity->getLink() == ':Front:News:show' && $child->active ){
			$el->src[] = $baseUri.'page.png';
			$el->title[] = "Veřejná položka";
			return $el;
        }
		elseif( $entity->getLink() == ':Front:News:show' && !$child->active ){
			$el->src[] = $baseUri.'page_delete.png';
			$el->title[] = "Archivní položka - nebude se zobrazovat";
			return $el;
        }
		elseif( $entity->getLink() == '' ){
			$el->src[] = $baseUri.'fingerboard.png';
			$el->title[] = "Archivní položka - nebude se zobrazovat";
			return $el;
        }
        if( $entity->getLink() == ':Front:Site:default' && $child->id_resource == 'news-member'){
			$el->src[] = $baseUri.'users.gif';
			$el->title[] = "Položka pouze pro klienty";
			return $el;
        }
		elseif( $entity->getLink() == ':Front:Site:default' && $child->active ){
			$el->src[] = $baseUri.'page.png';
			$el->title[] = "Veřejná položka";
			return $el;
        }
		elseif( $entity->getLink() == ':Front:Site:default' && !$child->active ){
			$el->src[] = $baseUri.'page_delete.png';
			$el->title[] = "Archivní položka - nebude se zobrazovat";
			return $el;
        }
		elseif( $entity->getLink() == '' ){
			$el->src[] = $baseUri.'page.png';
			$el->title[] = "Nezařazeno v menu";
			return $el;
        } else
		{
			$el->src[] = $baseUri.'box.png';
			$el->title[] = "[".$entity->getLink()."]";
			return $el;
		}
    }


}
