<?php

use AntoninRykalsky as AR,
	Nette\Application\UI,
	Nette\Utils\Html;

class GalleryHolderControl extends UI\Control #implements \AntoninRykalsky\IControl 
{
	private $holderId;

	/** @var  \Doctrine\ORM\EntityManager */
	private $em;
	
	public function setTypeParamByCms( $holderId )
	{
		$this->holderId = $holderId;
	}
	
	public function getItems() {
		$context = \Nette\Environment::getContext();
		$this->em = $context->getByType("AntoninRykalsky\EntityManager")->getEm();
		
		$group = $this->em->find("AntoninRykalsky\Entity\GalleryGroup", $this->holderId );
		
		return $group;
		
	}
	
	public function render() {

		$this->template->section = $this->getItems();
		$this->template->setFile(dirname(__FILE__) . '/temp.latte');
		$this->template->render();
	}

	/**
	 * Forces control to repaint.
	 * @return void
	 */
	function redrawControl() {
		// TODO: Implement redrawControl() method.
	}
}