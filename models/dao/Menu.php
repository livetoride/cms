<?php
/**
 * Model zachĂˇzejĂ­cĂ­ s tabulkou menu
 *
 * @author Bc. AntonĂ­n RykalskĂ˝ <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 AntonĂ­n RykalskĂ˝
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

namespace AntoninRykalsky;
use \dibi;

class Menu extends BaseModel
{
    /** @var DibiConnection */
	private $db;

	protected $table = 'menu';

	protected $primary = 'id_menu';

	protected $sequence = 'menu_id';

	protected $user;

    public function __construct()
	{
        $this->db = $this->connection;
	}

	function isTop( $id_menu )
	{
		return in_array($id_menu, array( 0, 1, 2, 3 ));
	}

	function menu_find_active()
    {
        $menu = dibi::query("SELECT * FROM menu WHERE active = 1 ORDER BY menu_order;" );

        return $menu->fetchAssoc('id_menu');
    }

	protected $vals = array(
		'id_menu' => 'int',
		'menu' => 'varchar',
		'menu_order' => 'int',
		'active' => 'boolean',
		'parent_id' => 'int',
		'link' => 'varchar',
		'param' => 'varchar',
		'id_resource' => 'int'
	);

	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new Menu();
		return $me;
	}

	/**
     * Has parent of node children?
     *
     * @param   integer Parent id
     * @return  integer Number of children
     */
    public function hasChildNodes($parent_id) {
        $sql = dibi::query('SELECT * FROM ['. $this->table .'] WHERE %and;', array('parent_id' => $parent_id));
        return count($sql);
    }

    /**
     * Return all children of specific parent of node
     *
     * @param   integer Parent id
     * @return  object
     */
    public function getMenuItems() {
		$sql = dibi::query('SELECT m.id_menu, m.menu, count(m2.id_menu) AS members, m.active, c.id_clanek, m.link, m.param, c.path, m.id_resource, m.menu_order, c.homepage, m.parent_id
			FROM ['.$this->table.'] AS m
			LEFT JOIN ['.$this->table.'] AS m2 ON m.id_menu=m2.parent_id
			LEFT JOIN [clanky] AS c ON m.param=c.path
			GROUP BY m.id_menu, m.menu, c.id_clanek, m.link, m.param, c.path, m.active, m.id_resource, m.menu_order, c.homepage, m.parent_id
			');

        return $sql->fetchAssoc('id_menu');
    }
    public function getChildNodes($parent_id, $id_resource = null ) {

		if( $id_resource == null )
		{
			$sql = dibi::query('SELECT m.id_menu
			FROM ['.$this->table.'] AS m
			WHERE m.active=1 AND m.parent_id = '.$parent_id.

			'ORDER BY m.menu_order;');
		} else {
			$sql = dibi::query('SELECT m.id_menu
			FROM ['.$this->table.'] AS m
			WHERE m.active=1 AND m.parent_id = '.$parent_id.' AND m.id_resource IN %in '.
			'ORDER BY m.menu_order;', $id_resource);
		}

        return $sql->fetchPairs('id_menu', 'id_menu');
    }
    public function getCompleteChildNodes($parent_id, $id_resource = null ) {
		$sql = dibi::query('SELECT m.id_menu
			FROM ['.$this->table.'] AS m
			WHERE m.active=1 AND m.parent_id = '.$parent_id.'
			ORDER BY m.menu_order;');

        return $sql->fetchPairs('id_menu', 'id_menu');
    }


	/**
     * Return all resources in the tree structure
     *
     * @return  array
     */
    public function getTreeValues( $parent = 0, $onlyActive = 1, $role = null, $where = null, $user ) {
		$this->user = $user;
        $resources = array();
        $this->getParents($parent, $resources, 0, $onlyActive, $role, $where);
        return $resources;
    }


	/**
     * All children of specific parent of resources placed in a array
     *
     * @param   integer Parent id
     * @param   array Array of curent resources
     * @param   integer Depth of tree structure
     */
    public function getParents($parent_id, &$array, $depth, $onlyActive = 1, $role = null, $where = null ) {
		$where['parent_id'] = $parent_id;
		if( $onlyActive ) $where['active'] = 1;
        $sql = dibi::query('SELECT id_menu as id, menu as name, id_resource, active FROM ['.$this->table.'] WHERE %and ORDER BY menu;', $where);

        $rows = $sql->fetchAll();
        foreach ($rows as $row) {

            $array[$row->id] = ($depth ? str_repeat('- - ', $depth) : '').$row->name;
            $this->getParents($row->id, $array, ($depth+1));
        }
    }


    public function getFirstLevel() {

        $menu = dibi::query("
            SELECT m.*, r.resource
            FROM menu m
            LEFT OUTER JOIN resources r ON ( m.id_resource = r.id_resource )

            WHERE active = 1 AND parent_id = 0 ORDER BY `menu-order`;" );

        $menu = $menu->fetchAll();

        return $this->returnMenu($menu);

    }

    private function returnMenu($menu)
    {
        $user = $this->user;

        $count_menu = count($menu);
        for( $i=0; $i<$count_menu; $i++ )
        {
            if( !$user->isAllowed( $menu[$i]['resource'], 'view' )) {
                 unset( $menu[$i]); continue; }

            $menu[$i]['destination'] = ucfirst($menu[$i]['link']);

            if( $menu[$i]['resource'] == 'news' )
            $menu[$i]['param'] = $menu[$i]['menu'];
        }

        return $menu;
    }
}
