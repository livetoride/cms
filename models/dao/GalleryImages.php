<?php
/**
 * Model zacházající s tabulkou článků
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;
use dibi;

class GalleryImages extends BaseModel
{

	/** @var string  table name */
	protected $table = 'gallery_images';

	/** @var string|array  primary key column name */
	protected $primary = 'id_image';

	protected $vals = array(
		'id_image' => 'int',
		'id_gallery' => 'int',
		'image_path' => 'varchar',
		'short_text' => 'varchar',
		'image_order' => 'int'
	);

	/** skeleton pattern */
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new GalleryImages();
		return $me;
	}


	public function imagesByMenu($id_menu)
	{
		$this->connection = dibi::getConnection();

		return $this->connection->select('*')->from($this->table)
				->leftJoin('gallery')->on("gallery.id_gallery = $this->table.id_gallery")
				->where("id_menu=$id_menu");
	}



	public function getDataSource()
    {
        return
		$this->connection->dataSource('SELECT * FROM '.$this->table);
    }

}
