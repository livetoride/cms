<?php
/**
 * Model zacházající s tabulkou článků
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class Newsreel extends BaseModel
{

	/** @var string  table name */
	protected $table = 'newsreel';

	/** @var string|array  primary key column name */
	protected $primary = 'id_newsreel';

	protected $vals = array(
		'id_newsreel' => 'int',
		'long_text' => 'varchar',
		'insert_date' => 'date',
		'insert_time' => 'time',
		'photo' => 'varchar'
	);

	/** skeleton pattern */
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new Newsreel();
		return $me;
	}



}
