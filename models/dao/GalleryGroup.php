<?php
/**
 * Model zacházající s tabulkou článků
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class GalleryGroup extends BaseModel
{

	/** @var string  table name */
	protected $table = 'gallery_group';

	/** @var string|array  primary key column name */
	protected $primary = 'id_menu';

	protected $vals = array(
		'id_menu' => 'int',
		'html_title' => 'varchar',
		'html_description' => 'varchar',
		'html_keywords' => 'varchar',
		'text_top' => 'text',
		'text_bottom' => 'text'

	);

	/** skeleton pattern */
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new GalleryGroup();
		return $me;
	}

}
