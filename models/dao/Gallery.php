<?php
/**
 * Model zacházající s tabulkou článků
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class Gallery extends BaseModel
{

	/** @var string  table name */
	protected $table = 'gallery';

	/** @var string|array  primary key column name */
	protected $primary = 'id_gallery';

	protected $vals = array(
		'id_gallery' => 'int',
		'id_menu' => 'int',
		'name' => 'varchar'
	);

	/** skeleton pattern */
	public static function get()
	{
		static $me = null;
		if ( $me == null )
		$me = new Gallery();
		return $me;
	}


	public function getDataSource()
    {
        return
		$this->connection->dataSource("SELECT * FROM [$this->table]");
    }

}
