<?php

class EditLink
{
	private $link;
	private $text;
	private $idType;

	public function getLink() {
		return $this->link;
	}

	public function getText() {
		return $this->text;
	}

	public function getIdType() {
		return $this->idType;
	}

	public function setLink($link) {
		$this->link = $link;
	}

	public function setText($text) {
		$this->text = $text;
	}

	public function setIdType($idType) {
		$this->idType = $idType;
	}


	const TYPE_ID = 1;
	const TYPE_PARAM = 2;
	const ID = 3;
}
