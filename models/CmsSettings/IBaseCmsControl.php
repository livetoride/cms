<?php

interface IBaseCmsControl
{
	public function getName();
	public function getImg();
	public function getType();
	public function getTypeId();
	public function getControlName();
}
