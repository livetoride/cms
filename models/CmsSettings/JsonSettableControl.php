<?php

use	Nette\Application\UI;

class JsonSettableControl extends \Nette\Application\UI\Control
{
	private $child;

	private $configFile;

	public function registerChild( $child )
	{
		$this->child = $child;
		$reflector = new ReflectionClass( $this->child );
		$fn = $reflector->getFileName();
		$this->configFile = dirname($fn) . '/config.json';
		$this->loadConfiguration();
	}

	public function loadConfiguration()
	{
		if(file_exists( $this->configFile ))
		{
			$config = json_decode( file_get_contents($this->configFile));
			foreach( $config as $key => $value )
			{
				if( property_exists($this->child, $key ))
				{

					$this->child->$key = $value;
				}
			}
		}
	}

	public function getConfiguration()
	{
		if(file_exists( $this->configFile ))
		{
			$config = json_decode( file_get_contents($this->configFile));
			return (array)$config;
		}
	}

	public function storeConfiguration( $configuration )
	{
		$json = json_encode( $configuration );
		file_put_contents($this->configFile, $json);
	}
}
