<?php

interface ICmsControlsService
{

}
use AntoninRykalsky\Cms\ControlConfig as CC;
use AntoninRykalsky\Entity\CmsGrid;

abstract class CmsControlsService implements ICmsControlsService
{
	const GRID_TYPE_ARTICLE = 1;
	const GRID_TYPE_CONTROL = 2;
	const GRID_TYPE_MULTIGRID = 3;

	private $cmsControls = array();

	public function getCmsControls()
	{
		return $this->cmsControls;
	}

	public function addCmsControls( $control )
	{
		$this->cmsControls[] = $control;
	}

	public function getCmsControl( $name )
	{
		/* @var $control \IBaseCmsControl */
		foreach( $this->getCmsControls() as $control )
		{
			if( $control->getName() == $name )
			{
				return $control;
			}
		}
		return new CC\EmptyCmsControl;
	}

	/**
	 * Return an ControlConfig for grid.
	 *
	 * @param $grid
	 * 		CmsGrid
	 * @return CC\EmptyCmsControl|IBaseCmsControl
	 */
	public function findOut( CmsGrid $grid )
	{
		/* @var $control \IBaseCmsControl */
		foreach( $this->getCmsControls() as $control )
		{
			$isSimple = in_array($grid->getType(), [self::GRID_TYPE_ARTICLE, self::GRID_TYPE_MULTIGRID] );
			if( $isSimple && $grid->getType() === $control->getType()) {
				// multigrid nebo článek
				return $control;

			} elseif( $grid->getType() === self::GRID_TYPE_CONTROL ) { // TODO user inheritance

				// komponenta
				if( $grid->getTypeId() === $control->getTypeId() )
				{
					return $control;
				}
			}
		}

		return new CC\EmptyCmsControl;
	}

}
