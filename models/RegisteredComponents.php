<?php
/**
 * Zobrazování novinek
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky\Cms;

/** @deprecated */
class RegisteredComponents {

	/** @deprecated */
	public static function register( $p, $key )
	{
		return;
		$c = \DAO\CmsArticle::get()->find( $this->type_id )->fetch();

		if( !empty(static::$usedControls[$c->control])) {
			static::$usedControls[ $c->control]++;
			$key = $c->control . static::$usedControls[ $c->control];
		} else {
			static::$usedControls[ $c->control ]=1;//actual
			$key = $c->control;
		}

		$key = strtolower($key);

		static::$usedControls[] = $c->control;

		#echo 'new \\'.$c->control.'($p,"'.$key.'");';
		eval('$c = new \\'.$c->control.'($p,"'.$key.'");');
		try {
			if( !method_exists( $c, 'render' ))
					throw new \LogicException('Nelze vykrestlit');
			return $c->render();
		} catch( \LogicException $e )
		{
			// ignore
		}
	}
}
