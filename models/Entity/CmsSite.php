<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CmsSite
 *
 * @ORM\Table(name="cms_site")
 * @ORM\Entity(repositoryClass="AntoninRykalsky\SiteRepository")
 */
class CmsSite {

	const NETTE_LINK = ':Front:Site:default';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cms_site_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", nullable=true)
     */
    private $site;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="homepage", type="decimal", precision=1, scale=0, nullable=true)
     */
    private $isHomepage = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="dontrenderh1", type="decimal", precision=1, scale=0, nullable=true)
     */
    private $dontRenderTitle = '0';

	/**
     * @var CmsWrapper[]
     *
     * @ORM\OneToMany(targetEntity="CmsWrapper", mappedBy="site")
     */
    private $wrappers;

	/**
	 * NOT PERSISTED BY DOCTRINE
	 */
	private $inMenu = [];

	/** @var string */
	private $tmpFulltext = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas semper pretium velit, ullamcorper congue lorem laoreet et. Ut eget urna in ipsum dapibus sollicitudin id eget velit. Phasellus tincidunt lorem justo, sit amet blandit ligula adipiscing at. Proin at lorem turpis. Integer et mi consectetur, ullamcorper velit porta, tincidunt nisi. Etiam faucibus, neque vitae dictum semper, dolor turpis feugiat magna, et pharetra sapien orci adipiscing justo. Aliquam tempus euismod est at consequat. Aenean pellentesque eros ac feugiat sodales. Nullam et tempor metus. Vivamus quis arcu et mauris posuere egestas in sed odio. Nulla tincidunt urna neque, vitae malesuada arcu semper quis. Integer a lectus mauris. Vivamus id mauris nisl. Phasellus in pellentesque nunc, eu rutrum purus. Duis pretium enim elit.';



	public function __construct()
    {
        $this->wrappers = new ArrayCollection;
    }

	public function getId() {
		return $this->id;
	}

	public function getSite() {
		return $this->site;
	}

	public function getTitle() {
		return $this->title;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getKeywords() {
		return $this->keywords;
	}

	public function getUrl() {
		return $this->url;
	}

	public function getIsHomepage() {
		return $this->isHomepage;
	}

	public function getDontRenderTitle() {
		return $this->dontRenderTitle;
	}

	public function getTmpFulltext() {
		return $this->tmpFulltext;
	}

	public function setTmpFulltext($tmpFulltext) {
		$this->tmpFulltext = $tmpFulltext;
	}

		public function setSite($site) {
		$this->site = $site;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function setKeywords($keywords) {
		$this->keywords = $keywords;
	}

	public function setUrl($url) {
		$this->url = $url;
	}

	public function setIsHomepage($isHomepage) {
		$this->isHomepage = $isHomepage;
	}

	public function setDontRenderTitle($dontRenderTitle) {
		$this->dontRenderTitle = $dontRenderTitle;
	}

	/**
	 * @return CmsWrapper[]|ArrayCollection
	 */
	public function getWrappers() {
		return $this->wrappers;
	}

	public function getGrids() {
		$w = $this->getWrappers();
		if( !empty( $w[0] ))
		{
			return $w[0]->getGrids();
		}
	}

	public function addWrapper(CmsWrapper $wrapper) {
		$this->wrappers[] = $wrapper;
	}

	/**
	 * @return Menu[]
	 */
	public function getInMenu() {

		return $this->inMenu;
	}

	/**
	 * @param Menu[] $inMenu
	 */
	public function setInMenu($inMenu) { $this->inMenu = $inMenu; }




}
