<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette;

/**
 * @ORM\Table(name="gallery")
 * @ORM\Entity
 */
class Carousel extends Nette\Object
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_gallery", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="gallery_id_gallery_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
	 * @ORM\Column(name="name", type="string", length=64, nullable=true)
	 */
	private $name;

	/**
	  * @ORM\oneToMany(targetEntity="Images", mappedBy="carousel")
	  */
	private $images;
	
	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}
	
	public function getImages() {
		return $this->images;
	}

	
	public function setCarousel($name) {
		$this->name = $name;
	}


	

	

}
