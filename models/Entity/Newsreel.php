<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Newsreel
 *
 * @ORM\Table(name="newsreel")
 * @ORM\Entity
 */
class Newsreel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_newsreel", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="newsreel_id_newsreel_seq", allocationSize=1, initialValue=1)
     */
    private $idNewsreel;

    /**
     * @var string
     *
     * @ORM\Column(name="long_text", type="text", nullable=true)
     */
    private $longText;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=127, nullable=true)
     */
    private $photo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insert_date", type="date", nullable=true)
     */
    private $insertDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insert_time", type="time", nullable=true)
     */
    private $insertTime;


}
