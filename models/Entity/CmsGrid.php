<?php

namespace AntoninRykalsky\Entity;

use Doctrine\Common\Util\Debug;
use Doctrine\ORM\Mapping as ORM;
use Nette;
use SoftwareStudio\Common\StringUtils;
use Tracy\Debugger;

/**
 * CmsGrid
 *
 * @ORM\Table(name="cms_grid")
 * @ORM\Entity(repositoryClass="AntoninRykalsky\CmsGridRepository")
 */
class CmsGrid extends Nette\Object
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_grid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cms_grid_id_grid_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer", nullable=true)
     */
    private $width;

    /**
     * @var integer
     *
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=true)
     */
    private $typeId;

    /**
     * @var string
     *
     * @ORM\Column(name="type_param", type="string", nullable=true)
     */
    private $typeParam;

	/**
     * @var CmsWrapper
     *
     * @ORM\ManyToOne(targetEntity="CmsWrapper")
     * @ORM\JoinColumn(name="wrapper_id", referencedColumnName="id_wrapper")
     */
    private $wrapper;

	/**
	 * @var CmsGrid[]
     * @ORM\OneToMany(targetEntity="CmsGrid", mappedBy="parentGrid")
	 * @ORM\OrderBy({"order" = "ASC"})
     */
    private $subgrids;

	/**
     * @ORM\ManyToOne(targetEntity="CmsGrid", inversedBy="subgrids")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id_grid")
     */
    private $parentGrid;
	
	
	/**
	 * @ORM\manyToOne(targetEntity="CmsControlSetting", inversedBy="inversed")
	 * @ORM\joinColumn(name="settings_id", referencedColumnName="id")
	 */
	private $setting;

	const ARTICLE = 1;
	const CONTROL = 2;
	static $presenter;
	static $usedControls=array();

	/** @var CmsGrid[] */
	static $usedControlsKeys=array();

	static $keyByLink=[];

	private $myComponentKey;


	private $controlSetting;

	private $cmsReplacing;

	private $fullLinkByUniqueLink=[];

	public function isEditable() {
		return ($this->type == self::ARTICLE? "true" : 'false' );
	}

	/**
	 * Called from site control to initialize components.
	 */
	public function findOut(){
		if( $this->type == self::CONTROL )
		{
			$controlName = $this->getControlSetting()->getControlName();
			if( !empty(static::$usedControls[$controlName])) {
				static::$usedControls[ $controlName]++;
				$key = $controlName . static::$usedControls[ $controlName];
			} else {
				static::$usedControls[ $controlName ]=1;//actual
				$key = $controlName;
			}

			$key = preg_replace('#\\\#', '',  $key);
			$this->myComponentKey = $key = lcfirst($key);
			#echo $key;

			static::$usedControls[] = $controlName;
			static::$usedControlsKeys[$key] = $this;

		}
	}

	public function getControlSetting()
	{
		if( empty( $this->controlSetting ))
		{
			throw new \LogicException("Chybí inject proměnné controlSetting");
		}
		return $this->controlSetting;
	}

	/**
	 * Create an inner component[s] of this component
	 *
	 * @param $p
	 * @param $key
	 * @return null|void
	 * @throws \Exception
	 */
	public function createComponent( Nette\Application\UI\Control $p, $key )
	{
		if( $this->type == self::CONTROL ) {

			Debugger::log(StringUtils::message("Create component in cmsGrid [{}]", $key ));

			if( empty( $this->getControlSetting()->getControlName() ))
			{
				Debugger::log("Missing control name", Debugger::EXCEPTION);
				# empty control
				return;
			}

			/** @var Nette\Application\UI\Control $c  */
			eval('$c = new \\'.$this->getControlSetting()->getControlName().'($p,"'.$key.'");');

			$this->fullLinkByUniqueLink[ $key ] = $c->getUniqueId();
			
			if( empty( $c ))
			{
				Debugger::log("Component initialization of [{}] has failed");

				return null;
				print_r( $this->getControlSetting() );
				exit;
			}
			
			if( !empty( $this->typeId )) {
				if( method_exists ( $c, 'setParamByCms' )) {
					$c->setParamByCms( $this->typeId );
					Debugger::log(StringUtils::message("Setting param by cms [{}]", $this->typeId ), Debugger::INFO);
				}
			}

			if( method_exists ( $c, 'injectSetting' )) {
				$c->injectSetting( $this->getSetting()->getSetting(), Debugger::INFO );
				Debugger::log(StringUtils::message("Injecting setting" ));
			}

			if( method_exists ( $c, 'setGridIdByCms' )) {
				$c->setGridIdByCms( $this->id );
				Debugger::log(StringUtils::message("setGridIdByCms {}", $this->id ), Debugger::INFO);
			}

			if( !empty( $this->typeParam )) {
				if( method_exists ( $c, 'setTypeParamByCms' )) {
					$c->setTypeParamByCms( $this->typeParam );
					Debugger::log(StringUtils::message("setTypeParamByCms {}", $this->typeParam ), Debugger::INFO );
				}
			}

			Debugger::log(StringUtils::message("Inner component created {}",$key ), Debugger::INFO );

			return $c;
		}
	}


	public function render( $simple = false, $registerOnly=false )
	{
		if( $this->type == self::ARTICLE )
		{
			$a = \DAO\CmsArticle::get()->find( $this->typeId )->fetch()->article;
			if( empty( $this->cmsReplacing ))
			{
				throw new \Exception("Neni predan cmsReplacing");
			}
			return $this->cmsReplacing->replacingLoad( $a );
		}
		if( $this->type == self::CONTROL )
		{
			$key = $this->myComponentKey;

			try {
				if( $key == null ) {
					$e = StringUtils::message("Missing key for component {}", $key);
					Debugger::log($e);
					return;
				}
				if( empty( $this->fullLinkByUniqueLink[$key] )) {
					$e = StringUtils::message("Missing key for component {}", $key);
					Debugger::log($e);
					return;
				}
				$c = static::$presenter[ $this->fullLinkByUniqueLink[$key] ];
			} catch (\Exception $e )
			{

				throw $e;
				return;
			}

			try {
				if( !method_exists( $c, 'render' )) {

					$e = StringUtils::message("Missing key for component {}", $key);
					Debugger::log($e);
					return;
				}

				return $c->render();
			} catch( \LogicException $e )
			{
				$e = StringUtils::message("Missing key for component {}", $key);
				Debugger::log($e);
				return;
			}
		}
	}
	
	
	public function getSettingArray() {
		if (!empty($this->setting)) {
			
		return $this->setting->getSetting();
		} else {
			return null;
		}
	}
	public function getSetting() {
		/*if ($this->setting === null)
		{
			$this->setting = \AntoninRykalsky\Cms\ControlConfig\MapControl::getDefaultSettings();
			
		}*/
		return $this->setting;
	}

	public function setSetting($setting) {
		$this->setting = $setting;
	}

		
	private $alpha;
	private $omega;
	
	public function getAlpha() {
		return $this->alpha;
	}

	public function getOmega() {
		return $this->omega;
	}

	public function setAlpha() {
		$this->alpha = 'alpha';
	}

	public function setOmega() {
		$this->omega = 'omega';
	}






	public function getSubGrids() {
		return $this->subgrids;
	}

	public function getParentGrid() {
		return $this->parentGrid;
	}


	public function getId() {
		return $this->id;
	}

	public function getWidth() {
		return $this->width;
	}

	public function getOrder() {
		return $this->order;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getTypeParam() {
		return $this->typeParam;
	}

	public function getWrapper() {
		return $this->wrapper;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function setWidth($width) {
		$this->width = $width;
	}

	public function setOrder($order) {
		$this->order = $order;
	}

	public function setType($type) {
		$this->type = $type;
	}

	public function setTypeId($typeId) {
		$this->typeId = $typeId;
	}

	public function setTypeParam($typeParam) {
		$this->typeParam = $typeParam;
	}

	public function setWrapper(CmsWrapper $wrapper) {
		$this->wrapper = $wrapper;
	}

	public function setCmsControl( \IBaseCmsControl $cmsControl )
	{
		$this->type = $cmsControl->getType();
		$this->typeId = $cmsControl->getTypeId();
	}

	public function setParentGrid( CmsGrid $parentGrid) {
		$this->parentGrid = $parentGrid;
	}




	public function injectControlSetting( $controlSetting) { $this->controlSetting = $controlSetting; }

	public function injectCmsReplacing($cmsReplacing) { $this->cmsReplacing = $cmsReplacing; }


}
