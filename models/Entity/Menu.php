<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Menu
 *
 * @ORM\Table(name="menu", indexes={@ORM\Index(name="menu_idx_parent_id", columns={"parent_id"})})
 * @ORM\Entity
 */
class Menu
{
	const SITE = ":Front:Site:default";

    /**
     * @var integer
     *
     * @ORM\Column(name="id_menu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="menu_id_menu_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=false)
     */
    private $parentId;

    /**
     * @var string
     *
     * @ORM\Column(name="menu", type="string", length=64, nullable=true)
     */
    private $menu;

    /**
     * @var integer
     *
     * @ORM\Column(name="menu_order", type="integer", nullable=true)
     */
    private $menuOrder;

    /**
     * @var string
     *
     * @ORM\Column(name="id_resource", type="string", length=64, nullable=true)
     */
    private $idResource;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $active = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=64, nullable=true)
     */
    private $link;

    /**
     * @var string
     *
     * @ORM\Column(name="param", type="string", length=64, nullable=true)
     */
    private $param;

	public function getParam() {
		return $this->param;
	}

	public function getId() {
		return $this->id;
	}

	public function getParentId() {
		return $this->parentId;
	}

	public function getMenu() {
		return $this->menu;
	}

	public function getMenuOrder() {
		return $this->menuOrder;
	}

	public function getIdResource() {
		return $this->idResource;
	}

	public function getActive() {
		return $this->active;
	}

	public function getLink() {
		return $this->link;
	}

	public function setParentId($parentId) {
		$this->parentId = $parentId;
		return $this;
	}

	public function setMenu($menu) {
		$this->menu = $menu;
		return $this;
	}

	public function setMenuOrder($menuOrder) {
		$this->menuOrder = $menuOrder;
		return $this;
	}

	public function setIdResource($idResource) {
		$this->idResource = $idResource;
		return $this;
	}

	public function setActive($active) {
		$this->active = $active;
		return $this;
	}

	public function setLink($link) {
		$this->link = $link;
		return $this;
	}

	public function setParam($param) {
		$this->param = $param;
		return $this;
	}





}
