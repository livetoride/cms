<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clanky
 *
 * @ORM\Table(name="clanky")
 * @ORM\Entity
 */
class Clanky
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_clanek", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="clanky_id_clanek_seq", allocationSize=1, initialValue=1)
     */
    private $idClanek;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=64, nullable=false)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="nadpis", type="string", length=64, nullable=false)
     */
    private $nadpis;

    /**
     * @var string
     *
     * @ORM\Column(name="clanek", type="text", nullable=true)
     */
    private $clanek;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datum", type="date", nullable=true)
     */
    private $datum;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="zmeneno", type="date", nullable=true)
     */
    private $zmeneno;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="zobrazdo", type="date", nullable=true)
     */
    private $zobrazdo;

    /**
     * @var string
     *
     * @ORM\Column(name="active", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $active = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="keywords", type="string", length=300, nullable=true)
     */
    private $keywords;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=300, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="homepage", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $homepage = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="menunadpis", type="string", length=64, nullable=true)
     */
    private $menunadpis;

    /**
     * @var string
     *
     * @ORM\Column(name="dontrenderh1", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $dontrenderh1 = '0';


}
