<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CmsControl
 *
 * @ORM\Table(name="cms_control_setting")
 * @ORM\Entity
 */
class CmsControlSetting
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cms_control_setting_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="setting", type="json_array", nullable=true)
     */
    private $setting;

	public function getId() {
		return $this->id;
	}

	public function getSetting() {
		return $this->setting;
	}

	public function setSetting($setting) {
		$this->setting = $setting;
	}


}
