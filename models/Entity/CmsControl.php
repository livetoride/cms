<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CmsControl
 *
 * @ORM\Table(name="cms_control")
 * @ORM\Entity
 */
class CmsControl
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_control", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cms_control_id_control_seq", allocationSize=1, initialValue=1)
     */
    private $idControl;

    /**
     * @var string
     *
     * @ORM\Column(name="control", type="string", nullable=true)
     */
    private $control;

    /**
     * @var string
     *
     * @ORM\Column(name="params", type="string", nullable=true)
     */
    private $params;


}
