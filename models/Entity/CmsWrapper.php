<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * CmsWrapper
 *
 * @ORM\Table(name="cms_wrapper")
 * @ORM\Entity
 */
class CmsWrapper
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_wrapper", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cms_wrapper_id_wrapper_seq", allocationSize=1, initialValue=1)
     */
    private $id;

	/**
     * @var CmsSite
     *
     * @ORM\ManyToOne(targetEntity="CmsSite", inversedBy="wrappers")
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id")
     */
    private $site;

    /**
     * @var integer
     *
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;


	/**
     * @var CmsGrid[]
     *
     * @ORM\OneToMany(targetEntity="CmsGrid", mappedBy="wrapper")
	 * @ORM\OrderBy({"order" = "ASC"})
     */
    private $grids;

	public function __construct()
    {
        $this->grids = new ArrayCollection;
    }

	public function getId() {
		return $this->id;
	}

	public function getSite() {
		return $this->site;
	}

	public function getOrder() {
		return $this->order;
	}

	public function setSite(CmsSite $site) {
		$this->site = $site;
		$site->addWrapper( $this );
	}

	public function setOrder($order) {
		$this->order = $order;
	}

	/**
	 * @return CmsGrid[]|ArrayCollection
	 */
	public function getGrids() {
		return $this->grids;
	}

	public function addWrapper(CmsGrid $grid) {
		$this->grids[] = $grid;
	}



}
