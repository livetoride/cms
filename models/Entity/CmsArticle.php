<?php

namespace AntoninRykalsky\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CmsArticle
 *
 * @ORM\Table(name="cms_article")
 * @ORM\Entity
 */
class CmsArticle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_article", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="cms_article_id_article_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="article", type="text", nullable=true)
     */
    private $article;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_insert", type="datetime", nullable=true)
     */
    private $tsInsert;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts_edit", type="datetime", nullable=true)
     */
    private $tsEdit;

	public function __construct() {
		$this->tsInsert = new \DateTime;
	}

	public function setArticle($article) {
		$this->article = $article;
		$this->tsEdit = new \DateTime;
	}

	public function getId() {
		return $this->id;
	}

	public function getArticle() {
		return $this->article;
	}

	public function getTsInsert() {
		return $this->tsInsert;
	}

	public function getTsEdit() {
		return $this->tsEdit;
	}




}
