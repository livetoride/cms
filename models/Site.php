<?php
/**
 * Zobrazování novinek
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky\Cms;

class SiteRow {

	public function __construct($s, $w, $g ) {
		$this->site = $s;
		$this->wrapper = $w;
		$this->grid = $g;
	}
	public $site;
	public $wrapper;
	public $grid;

	public function getContent()
	{
		$in = \DAO\Menu::get()->findAll()->where('link=%s AND param=%s',
				':Front:Site:default',
				$this->site->url
				)->fetchPairs('id_menu', 'parent_id');

		#$r = \DAO\Menu::get()->findAll()->where('parent_id IN (%in)', array_values( $in )  )->fetchAssoc('id_menu');
		$r2 = \DAO\Menu::get()->findAll()->where('id_menu IN %in', array_values( $in )  )->fetchAssoc('id_menu');
		#print_r( $r2 );exit;
		$return=array();
		foreach( $in as $act => $parent )
		{

			$return[] = (object) array(
				'parent_name' => @$r2[$parent]['menu'],
				'menu_id' => $act,
				'parent_id' => $parent,
			);
		}

		#print_r( $return );
		return $return;
	}

	public function registerComponents( $p, $componentName = 'name' )
	{
		if( empty( GridRow::$usedControlsKeys[ $componentName ] )) return;
		#echo $componentName;
		$component = GridRow::$usedControlsKeys[ $componentName ];
		return $component->createComponent( $p, $componentName );

		return;
		if( empty( GridRow::$usedControlsC ))
		{
			// pouze jednou v rámci registrace komponent k presetneru
			$grid = $this->grid;
			foreach( $grid as $w )
			foreach( $w as $g )
			{
				#$g->findOut( $p );
			}
		}
//			print_r( array_keys( GridRow::$usedControlsC ));
//		exit;
		if( empty( GridRow::$usedControlsC[lcfirst($componentName )] ))
			return false;

		#echo $componentName;

		$control = GridRow::$usedControlsC[lcfirst($componentName )];
		#print_r( $control );exit;
		return $control;
	}

}

class GridRow extends \DibiRow2Object
{
	const ARTICLE = 1;
	const CONTROL = 2;
	static $usedControls=array();
	static $usedControlsC=array();
	static $usedControlsR=array();
	static $usedControlsKeys=array();

	private function fixKey( $key )
	{
		return preg_replace("#\\\#", "", $key);
	}

	/** @deprecated */
	public function createComponent( $p, $key )
	{
		if( $this->type == self::CONTROL )
		{
			$key = $this->fixKey( $key );
			eval('$c = new \\'.$this->controlEntity->getControlName().'($p,"'.$key.'");');

//			print_r( $this->content->params );exit;
			if( !empty( $this->content->params ))
				if( method_exists ( $c, 'setParamByCms' ))
					$c->setParamByCms( $this->content->params );

			if( method_exists ( $c, 'setGridIdByCms' ))
				$c->setGridIdByCms( $this->id );
				
			if( !empty( $this->type_param ))
				if( method_exists ( $c, 'setTypeParamByCms' ))
					$c->setTypeParamByCms( $this->type_param );


			static::$usedControlsC[$key] = $c;
			static::$usedControlsR[$this->id_grid] = $c;
				return $c;
		}
	}

	public function findOut(){
		if( $this->type == self::CONTROL )
		{

			$container = \Nette\Environment::getContext();
			$cmsControlService = $container->getByType('\ICmsControlsService');
			$em = $container->getByType('\AntoninRykalsky\EntityManager')->getEm();
			$gridEntity = $em->find("\AntoninRykalsky\Entity\CmsGrid", $this->id_grid );
			$control = $cmsControlService->findOut( $gridEntity );


			$this->controlEntity = $control;
			$controlName = $control->getControlName();





			$this->content = \DAO\CmsControl::get()->find( $this->type_id )->fetch();

			if( $controlName === null )
			{
				throw new \Exception("Nelze najít komponentu č.".$this->type_id );
			}
//			$a = \Nette\Environment::getApplication();
//			$p = $a->getPresenter();

			if( !empty(static::$usedControls[$controlName])) {
				static::$usedControls[ $controlName]++;
				$key = $controlName . static::$usedControls[ $controlName];
			} else {
				static::$usedControls[ $controlName ]=1;//actual
				$key = $controlName;
			}

			$key = lcfirst($key);
//			echo $key;exit

			static::$usedControls[] = $controlName ;
			static::$usedControlsKeys[$key] = $this;

		}
	}

	public function renderSimple2()
	{
		$this->render( true );
	}

	public function renderSimple()
	{
		if( $this->type == self::ARTICLE )
		{
			$this->content = \DAO\CmsArticle::get()->find( $this->type_id )->fetch();
			$r = $this->content->article;
			$r = strip_tags( $r );
//			$r = preg_replace('#%%%#', '<br />', $r);

			return $r;
		}
	}

	public function register( $key )
	{
		$key = 'bflNewsreelPanel';
		$c = '\\\BflNewsreelPanel';
		eval('$c = new \\'.$c.'($p,"'.$key.'");');
		return $c;
	}

	static $presenter;

	/** @var \AntoninRykalsky\CmsReplacing */
	protected $cmsReplacing;


	public function render( $simple = false, $registerOnly=false )
	{
		$container = \Nette\Environment::getContext();
		$this->cmsReplacing = $container->getByType('\AntoninRykalsky\CmsReplacing');

		if( $this->type == self::ARTICLE )
		{
			$this->content = \DAO\CmsArticle::get()->find( $this->type_id )->fetch();
			$return = $this->content->article;
			$return = $this->cmsReplacing->replacingLoad($return);
			return $return;
		}
		if( $this->type == self::CONTROL )
		{
			$key = $this->fixKey( lcfirst( $this->controlEntity->getControlName() ) );
			$c = static::$presenter[ $key ];

				try {
					if( !method_exists( $c, 'render' ))
							throw new \LogicException('Nelze vykrestlit');
					return $c->render();

				} catch( \LogicException $e )
				{
					// ignore
					echo "nelze vykreslit";

				}



//			$c = new \BflContactForm($p, 'asdf');

		}
	}
}

class Site
{
	const CACHE_ALLOW = false;
	const CACHE_DENY = true;

	static $cache=array();

	public static function getBySite( $id )
	{
		$s = \DAO\CmsSite::get()->find( $id )->fetch();
		if( empty( $s->id )) return;
		$w = \DAO\CmsWrapper::get()->findAll()->orderBy('order')->where('site_id=%i', $s->id )->fetchAll();
		$g=array();
		foreach( $w as $wrap )
		{
			$gGroup = \DAO\CmsGrid::get()->findAll()->orderBy('order')->where('wrapper_id=%i', $wrap->id_wrapper )->fetchAll();
			foreach( $gGroup as $iGroup )
			{
				$g[$wrap->id_wrapper][] = new GridRow( $iGroup );
			}
		}

		return new SiteRow($s, $w, $g);
	}


	public static function get( $id, $cache = self::CACHE_DENY )
	{
		$oldId = $id;
		if( $cache == self::CACHE_ALLOW && !empty(self::$cache[ $id ]) )
		{
			#echo "loadin ".$oldId;
			return self::$cache[ $id ];
		}


		if(!is_numeric( $id ))
		{
			$site = \DAO\CmsSite::get()->findAll()->where('url=%s', $id )->fetch();
			if( !empty( $site->id ))
				$id = $site->id;
		}
		if( empty( $id ))
		{
			$site = \DAO\CmsSite::get()->findAll()->where('homepage=1')->fetch();
			if( !empty( $site->id ))
				$id = $site->id;
		}

		self::$cache[ $oldId ] = $site = \AntoninRykalsky\Cms\Site::getBySite( $id );
		#echo "caching ".$id;
		return $site;
	}

	const MENU_OUTOFMENU = 5;

	public static function createTemp()
	{
		$name = 'Tmp '.date('Y-m-d H:i:s', strtotime('now'));
		\DAO\CmsSite::get()->insert(array(
			'site'=> $name,
			'url'=> \Nette\Utils\Strings::webalize( $name )
			));
		$sid = \dibi::getInsertId();
		\DAO\CmsWrapper::get()->insert( array( 'site_id'=> $sid, 'order'=>1 ));
		$wid = \dibi::getInsertId();

		\DAO\CmsArticle::get()->insert( array('article'=>'') );
		$aid = \dibi::getInsertId();

		\DAO\CmsGrid::get()->insert( array( 'wrapper_id'=>$wid, 'order'=>1, 'width' => 12, 'type'=>1, 'type_id'=>$aid ));



		$site = Site::get( $sid );
		$a = array(
			'parent_id'=>  static::MENU_OUTOFMENU,
			'link' => ':Front:Site:default',
			'param' => $site->site->url,
			'menu' => $site->site->site,
			'active' => 1,
			'id_resource' => 'news-frontend'
		);
		\DAO\Menu::get()->insert( $a );


		return self::getBySite( $sid );
	}

}

class Content
{


}
