<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class MenuFacade
{
	/** @var \AntoninRykalsky\EntityManager */
	protected $em;

	private $entity = 'AntoninRykalsky\Entity\Menu';

	public function __construct(
		\AntoninRykalsky\EntityManager $em
	){
		$this->em = $em->getEm();
	}

	/**
	 * Vrací IDčka hlavních sekcí
	 * @return array
	 */
	public function getMainSections()
	{
		return Menu::get()->getChildNodes(0, array("news-member", "news-frontend", "guest-only") );
	}

	public function getCompleteMainSections( $parent_id = 0 )
	{
		$t = new \AntoninRykalsky\Menu;
		$sql = \dibi::query('SELECT m.id_menu
			FROM ['.$t->table.'] AS m
			WHERE m.active=1 AND m.parent_id = '.$parent_id.'
			ORDER BY m.menu_order;');

        return $sql->fetchPairs('id_menu', 'id_menu');
	}

	public function getStructured()
	{
		$menu = $this->em->createQuery("SELECT a FROM {$this->entity} a INDEX BY a.id");
		return $menu->getResult();
	}

	public function updateMenuIds()
	{

		$menuSites = \DAO\Menu::get()->findAll()->where('link=":Front:Site:default"')->fetchAssoc('link');
		$sites = \DAO\CmsSite::get()->findAll()->fetchAll();

		foreach( $sites as $siteKey => $site )
		{
			if(!empty($menuSites[ $site->url ]))
			{

			} else {
				unset( $sites[ $siteKey ]);
			}

		}

		print_r( $sites );
		exit;

		$menu = $this->em->createQuery("SELECT a FROM {$this->entity} a INDEX BY a.id");
		return $menu->getResult();
	}

}
