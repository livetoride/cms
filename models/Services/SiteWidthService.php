<?php
namespace AntoninRykalsky\Cms;

use AntoninRykalsky\Cms\ControlConfig\EmptyCmsControl;
use AntoninRykalsky\CmsReplacing;
use AntoninRykalsky\Entity\CmsGrid;
use AntoninRykalsky\Entity\CmsSite;
use Nette\Utils\ArrayHash;
use Nette\Utils\ArrayList;
use SoftwareStudio\Common\StringUtils;
use Tester\Assert;
use Tracy\Debugger;

/**
 * Responsible about operation with site and injected control service.
 *
 * @package AntoninRykalsky
 */
class SiteWidthService {

	/** @var \CmsControlsService */
	protected $cmsControlsService;

	/** @var \AntoninRykalsky\CmsReplacing */
	protected $cmsReplacing;

	public function __construct(\ICmsControlsService $cmsControlsService, CmsReplacing $cmsReplacing ){
		$this->cmsControlsService = $cmsControlsService;
		$this->cmsReplacing = $cmsReplacing;
	}

	const RIGHT_OFFSET = 0;
	const LEFT_OFFSET = 0;
	private $offsets;
	private $leftOffset = 0;
	private $rightOffset = 12;

	/**
	 * Injects control settings into {@link CmsSite} entity, for proper render.
	 * @param CmsSite $site
	 */
	public function addControlServiceInto( CmsSite $site ) {

		$templateW=[];
		$templateG=[];
		$templateG[1]=[];

		//		$offsets = new ArrayList();
		//		$right= $offsets->offsetSet(self::RIGHT_OFFSET, 0);
		//		$left= $offsets->offsetSet(self::RIGHT_OFFSET, 12);

		/* @var $wrapper \AntoninRykalsky\Entity\CmsWrapper */
		foreach( $site->getWrappers() as $wrapper ) {
			/* @var $grid \AntoninRykalsky\Entity\CmsGrid */
			foreach ($wrapper->getGrids() as $grid) {

				$returned = $this->addControlServiceIntoG( $grid );

				$templateW = array_merge( $templateW, $returned[0]);
				$templateG[1] = array_merge( $templateG[1], $returned[1]);
			}
		}

		$templateW[]=(object)array('id_wrapper'=>1);

		return [$templateW, $templateG];
	}

	/**
	 * Injects control settings into {@link CmsGrid} entity, for proper render.
	 * @param CmsGrid $grid
	 * @param int $leftOffset
	 * 		Keep empty if you dont know.
	 * @param int $rightOffset
	 * 		Keep empty if you dont know.
	 * @param int $wrapperId
	 * 		Keep empty if you dont know.
	 */
	public function addControlServiceIntoG( CmsGrid $grid, $wrapperId=1 ) {


		$width = 0;
		$templateW = [];

		$this->setAlphaOmega( $grid );

		$leftOffsetBefore=$this->leftOffset;
		$this->rightOffset-=$grid->getWidth();

		$grid->findOut( $this );

//		$width+=$grid->getWidth();
//		if( $width > 12 )
//		{
//			$wrapperId+=1;
//			$templateW[] = (object)array('id_wrapper'=>$wrapperId);
//		}

		$templateG[] = $grid;


		$this->recursiveFindout( $grid );

		return [$templateW, $templateG ];

	}



	/**
	 * Sets alpha to first grid, set omega to last grid.
	 *
	 * @param CmsGrid $grid
	 * @param $leftOffset
	 * @param $rightOffset
	 */
	private function setAlphaOmega( CmsGrid $grid )
	{

//		if( $this->rightOffset == 0 )
//		{
//			$grid->setOmega();
//		}
//		if( $this->leftOffset == 0 )
//		{
//			$grid->setAlpha();
//		}
	}

	private function recursiveFindout( CmsGrid $myGrid ) {

		$subGrids = $myGrid->getSubGrids();

		foreach( $subGrids as $grid ) {

			$this->setAlphaOmega( $grid, $this->leftOffset, $this->rightOffset);

			$this->leftOffset+=$grid->getWidth();
			$this->rightOffset-=$grid->getWidth();

			$this->recursiveFindout( $grid );
		}
	}

}
