<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class MultigridCreatorFacade
{
	/** @var \AntoninRykalsky\EntityManager */
	protected $em;

	/** @var SiteService */
	protected $siteService;

	/** @var \ICmsControlsService */
	protected $cmsControlService;

	public function __construct(
		\AntoninRykalsky\EntityManager $em,
		SiteService $siteService,
		\ICmsControlsService $cmsControlService
	){
		$this->em = $em->getEm();
		$this->siteService = $siteService;
		$this->cmsControlService = $cmsControlService;
	}

	public function create( $forEntity )
	{
		\Doctrine\Common\Util\Debug::dump($forEntity);
		exit;
		$what = 'multigrid';
		
		$g = \DAO\CmsGrid::get()->findAll()->orderBy('order')->where('parent_id IN %in', array($gridId) )->fetchAll();
		$order = 0;
		foreach( $g as $grid )
		{
			$order++;
			\DAO\CmsGrid::get()->update( $grid->id_grid, array( 'order' => $order ));
		}

		/* @var $parentGrid \AntoninRykalsky\Entity\CmsGrid */
		$parentGrid = $this->em->find('AntoninRykalsky\Entity\CmsGrid', $gridId );

		$cmsControl = $this->cmsControlService->getCmsControl( $what );
		$gridEntity = new Entity\CmsGrid;
		$gridEntity->setParentGrid( $parentGrid );
		$gridEntity->setWidth( $parentGrid->getWidth() );
		$gridEntity->setOrder( $order+1 );
		$gridEntity->setCmsControl( $cmsControl );

		if( method_exists( $cmsControl, 'initializationId' ))
		{
			$return = $cmsControl->initializationId();
			if( !is_numeric($return))
			{
				$this->em->persist( $return );
				$gridEntity->setTypeId($return->getId());
			}
		}
		if( method_exists( $cmsControl, 'initializationParam' ))
		{
			$return = $cmsControl->initializationParam();
			if( !is_numeric($return))
			{
				$this->em->persist( $return );
				$gridEntity->setTypeParam($return->getId());
			}
		}

		$this->em->persist( $gridEntity );
		$this->em->flush();

		return $gridEntity->getId();
	}

}
