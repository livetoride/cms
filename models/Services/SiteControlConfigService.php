<?php
namespace AntoninRykalsky\Cms;

use AntoninRykalsky\CmsReplacing;
use AntoninRykalsky\Entity\CmsGrid;
use AntoninRykalsky\Entity\CmsSite;
use SoftwareStudio\Common\StringUtils;
use Tracy\Debugger;

/**
 * Responsible about operation with site and injected control service.
 *
 * @package AntoninRykalsky
 */
class SiteControlConfigService {

	/** @var \CmsControlsService */
	protected $cmsControlsService;

	/** @var \AntoninRykalsky\CmsReplacing */
	protected $cmsReplacing;

	public function __construct(\ICmsControlsService $cmsControlsService, CmsReplacing $cmsReplacing ){
		$this->cmsControlsService = $cmsControlsService;
		$this->cmsReplacing = $cmsReplacing;
	}

	/**
	 * Injects control settings into {@link CmsSite} entity, for proper render.
	 * @param CmsSite $site
	 */
	public function addControlServiceInto( CmsSite $site ) {

		/* @var $wrapper \AntoninRykalsky\Entity\CmsWrapper */
		foreach( $site->getWrappers() as $wrapper ) {
			/* @var $grid \AntoninRykalsky\Entity\CmsGrid */
			foreach ($wrapper->getGrids() as $grid) {

				$this->addControlServiceIntoG( $grid );
			}
		}
	}

	/**
	 * Injects control settings into {@link CmsGrid} entity, for proper render.
	 *
	 * @param CmsGrid $grid
	 */
	public function addControlServiceIntoG( CmsGrid $grid ) {

		$this->registerGrid( $grid );

		$this->addToSubGrids( $grid );
	}

	private function addToSubGrids(CmsGrid $parentGrid ) {

		$subGrids = $parentGrid->getSubGrids();

		foreach( $subGrids as $grid ) {

			$this->registerGrid( $grid );

			$this->addToSubGrids( $grid );
		}
	}

	private function registerGrid( CmsGrid $grid ) {

		/** @var IBaseCmsControl $controlSetting */
		$controlSetting = $this->cmsControlsService->findOut( $grid );

		Debugger::log(StringUtils::message("grid [{}]", get_class($controlSetting) ));

		$grid->injectControlSetting( $controlSetting );
		$grid->injectCmsReplacing( $this->cmsReplacing );
		$grid->findOut();
	}
}
