<?php
/**
 * Model zachĂˇzejĂ­cĂ­ s tabulkou produkty
 *
 * @author Bc. AntonĂ­n RykalskĂ˝ <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 AntonĂ­n RykalskĂ˝
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

use AntoninRykalsky\Cms\Site;
use AntoninRykalsky\Entity\CmsGrid;
use AntoninRykalsky\Entity\CmsSite;
use AntoninRykalsky\Entity\Menu;
use AntoninRykalsky\Menu as ARMenu;

use Doctrine\Common\Util\Debug;
use Nette\Environment;
use SoftwareStudio\Common\StringUtils;
use Tracy\Debugger;

class SiteFacade
{
	/** @var \AntoninRykalsky\EntityManager */
	protected $em;

	/** @var SiteService */
	protected $siteService;

	/** @var \ICmsControlsService */
	protected $cmsControlService;

	const MENU_OUTOFMENU = 5;

	const GRIDHOLDER = 3;

	public function __construct(
		\AntoninRykalsky\EntityManager $em,
		SiteService $siteService,
		\ICmsControlsService $cmsControlService
	){
		$this->em = $em->getEm();
		$this->siteService = $siteService;
		$this->cmsControlService = $cmsControlService;
	}

	/**
	 * Creates draft new page.
	 *
	 * @return CmsSite
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 * @throws \Doctrine\ORM\TransactionRequiredException
	 */
	public function createTemp() {

		$name = 'Tmp '.date('Y-m-d H:i:s', strtotime('now'));
		\DAO\CmsSite::get()->insert(array(
			'site'=> $name,
			'url'=> \Nette\Utils\Strings::webalize( $name )
		));
		$sid = \dibi::getInsertId();
		\DAO\CmsWrapper::get()->insert( array( 'site_id'=> $sid, 'order'=>1 ));
		$wid = \dibi::getInsertId();

		\DAO\CmsArticle::get()->insert( array('article'=>'') );
		$aid = \dibi::getInsertId();

		\DAO\CmsGrid::get()->insert( array( 'wrapper_id'=>$wid, 'order'=>1, 'width' => 12, 'type'=>1, 'type_id'=>$aid ));


		$site = Site::get( $sid );
		$a = array(
			'parent_id'=>  static::MENU_OUTOFMENU,
			'link' => ':Front:Site:default',
			'param' => $site->site->url,
			'menu' => $site->site->site,
			'active' => 1,
			'id_resource' => 'news-frontend'
		);
		\DAO\Menu::get()->insert( $a );

		/** @var CmsSite $siteEntity */
		$siteEntity = $this->em->find(CmsSite::class, $sid );

		return $siteEntity;
	}

	/**
	 * Add site to menu as ascendant.
	 *
	 * @param type $siteId
	 * @param type $menuParentId
	 */
	public function addToMenu( $siteId, $menuParentId )
	{
		$site = Cms\Site::get( $siteId );
		
		// <editor-fold defaultstate="collapsed" desc=" delete from nezarezeno v menu">

		$where = array(
			'link' => ':Front:Site:default',
			'param' => $site->site->url,
			'menu' => $site->site->site,
		);
		$sites = \DAO\Menu::get()->findAll()->where($where)->fetchAll();
		foreach ($sites as $s) 		{
			if ($s['parent_id'] == 5)	
			{
				\DAO\Menu::get()->delete( $s['id_menu'] );
			}
		}
		
		// </editor-fold>


		$a = array(
			'parent_id'=>$menuParentId,
			'link' => ':Front:Site:default',
			'param' => $site->site->url,
			'menu' => $site->site->site,
			'active' => 1,
			'id_resource' => 'news-frontend'
		);
		\DAO\Menu::get()->insert( $a );
	}

	/**
	 * Remove site from menu.
	 *
	 * @param $idmenu
	 * @param $siteId
	 */
	public function deleteFromMenu( $idmenu, $siteId )
	{
		$site = Cms\Site::get( $siteId );
		$c = $site->getContent();

		if( count( $c ) == 1)
		{
			$this->addToMenu( $siteId, self::MENU_OUTOFMENU );
		}

		\DAO\Menu::get()->delete($idmenu);
	}

	/**
	 * Page deletion
	 * @param $siteId
	 */
	public function deleteSite( $siteId ) {

		$siteEntity =  $this->em->getRepository( CmsSite::class )->getSiteBy( $siteId );
		if( $siteEntity !== null ) {
			$this->em->getRepository( CmsSite::class )->deleteSite( $siteEntity );
		}
	}

	/**
	 * More pages deletion
	 *
	 * @param $pages
	 */
	public function deleteSites($menuIds) {

		foreach( $menuIds as $menuId ) {

			$menu =  $this->em->getRepository( Menu::class )->find( $menuId );

			Debugger::log(StringUtils::message("Deletion of menu {}", $menu->getId() ));

			try {
				/** @var CmsSite $siteEntity */
				$siteEntity =  $this->em->getRepository( CmsSite::class )->getSiteByLink( $menu->getParam() );
			} catch( \Exception $e ) {

				Debugger::log(StringUtils::message("Site {} was already deleted.", $menu->getParam() ));
			}


			if( $siteEntity !== null ) {
				Debugger::log(StringUtils::message("Deletion of page {}", $siteEntity->getUrl() ));

				$this->em->getRepository( CmsSite::class )->deleteSite( $siteEntity );
			}

			$this->em->remove( $menu );

		}

		// TODO delete sum menu items

		$this->em->flush();

		// obnovíme cache pro menu
		$cache = Environment::getCache('menu_cache');
		$cache['menu_cache'] = null;
	}



	/**
	 * @param $idGrid
	 * @return CmsGrid
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 * @throws \Doctrine\ORM\TransactionRequiredException
	 * @deprecated
	 */
	public function getGrid( $idGrid )
	{
		return $this->em->find('AntoninRykalsky\Entity\CmsGrid', $idGrid );
	}

	/**
	 * @param $idGrid
	 * @return mixed
	 * @deprecated
	 */
	public function getGridInfo( $idGrid )
	{
		return $this->em->getRepository('AntoninRykalsky\Entity\CmsGrid')->findGridNestig( $idGrid );
	}

	/**
	 * Klonuje strrĂˇnku
	 * @param type $id
	 * @return type
	 */
	public function cloneSite( $id )
	{
		$original = $this->em->find( 'AntoninRykalsky\Entity\CmsSite', $id );

		$site = $this->siteService->cloneSite( $original );
		$this->em->flush();

		return $site;
	}

	private function addDefaultSettingToGrid( $gridEntity, $cmsControl )
	{
		if( method_exists( $cmsControl, 'initializationSetting' ))
		{
			$return = $cmsControl->initializationSetting();
			$settings = new \AntoninRykalsky\Entity\CmsControlSetting();
			$settings->setSetting( $return );
			
			$this->em->persist( $settings );
			
			$gridEntity->setSetting( $settings );
		}
		return $gridEntity;
	}
	
	public function addStuffToGrid( $what, $gridId )
	{

		$g = \DAO\CmsGrid::get()->findAll()->orderBy('order')->where('parent_id IN %in', array($gridId) )->fetchAll();
		$order = 0;
		foreach( $g as $grid )
		{
			$order++;
			\DAO\CmsGrid::get()->update( $grid->id_grid, array( 'order' => $order ));
		}

		/* @var $parentGrid \AntoninRykalsky\Entity\CmsGrid */
		$parentGrid = $this->em->find('AntoninRykalsky\Entity\CmsGrid', $gridId );

		$cmsControl = $this->cmsControlService->getCmsControl( $what );
		$gridEntity = new Entity\CmsGrid;
		$gridEntity->setParentGrid( $parentGrid );
		$gridEntity->setWidth( $parentGrid->getWidth() );
		$gridEntity->setOrder( $order+1 );
		$gridEntity->setCmsControl( $cmsControl );

		if( method_exists( $cmsControl, 'initializationId' ))
		{
			$return = $cmsControl->initializationId();
			if( !is_numeric($return))
			{
				$this->em->persist( $return );
				$gridEntity->setTypeId($return->getId());
			}
		}
		
		$gridEntity = $this->addDefaultSettingToGrid($gridEntity, $cmsControl);
		
		if( method_exists( $cmsControl, 'initializationParam' ))
		{
			$return = $cmsControl->initializationParam();
			if( !is_numeric($return))
			{
				$this->em->persist( $return );
				$gridEntity->setTypeParam($return->getId());
			}
		}

		$this->em->persist( $gridEntity );
		$this->em->flush();

		return $gridEntity->getId();
	}

	public function addStuffToSite( $what, $siteId )
	{
		Cms\Site::get( $siteId );

		$ws = \DAO\CmsWrapper::get()->findAll()->where('site_id=%i', $siteId )->fetchPairs('id_wrapper', 'id_wrapper');
//		print_r( $ws );exit;
		$g = \DAO\CmsGrid::get()->findAll()->orderBy('order')->where('wrapper_id IN %in', array_keys($ws) )->fetchAll();

		$order = 0;
		foreach( $g as $grid )
		{
			$order++;
			\DAO\CmsGrid::get()->update( $grid->id_grid, array( 'order' => $order ));
		}

		$lastWrap = max(array_keys($ws));

		$cmsControl = $this->cmsControlService->getCmsControl( $what );
		
		$gridEntity = new Entity\CmsGrid;
		$gridEntity->setWrapper( $this->em->getReference('AntoninRykalsky\Entity\CmsWrapper', $lastWrap) );
		$gridEntity->setWidth( 12 );
		$gridEntity->setOrder( $order+1 );
		$gridEntity->setCmsControl( $cmsControl );

		if( method_exists( $cmsControl, 'initializationId' ))
		{
			$return = $cmsControl->initializationId();
			if( !is_numeric($return))
			{
				$this->em->persist( $return );
				$gridEntity->setTypeId($return->getId());
			}
		}
		
		$gridEntity = $this->addDefaultSettingToGrid($gridEntity, $cmsControl);
		
		if( method_exists( $cmsControl, 'initializationParam' ))
		{
			$return = $cmsControl->initializationParam();
			if( !is_numeric($return))
			{
				$this->em->persist( $return );
				$gridEntity->setTypeParam($return->getId());
			}
		}

		$this->em->persist( $gridEntity );
		$this->em->flush();

		return $gridEntity->getId();
	}





	private function configureStuff( $what, $grid )
	{

		switch ($what) {
			case 'multigrid':
				// add parent grid
				$grid['type'] = self::GRIDHOLDER; // gridholder - fake wrapper
				unset( $grid['type_id'] ); // no type id
				break;
			case 'text':
				\DAO\CmsArticle::get()->insert(array('article'=>''));
				$grid['type_id'] = \dibi::getInsertId();
				$grid['type'] = 1;
				break;
			case 'calendar':
				$grid['type_id'] = 7;
				$grid['type'] = 2;
				break;
			case 'gallery':
				$n = new \DateTime();
				$name = 'tmp-'.$n->format('Y').'-'.$n->format('n').'-'.$n->format('j').'-'.$n->format('His');
				\DAO\Gallery::get()->insert(array('name'=>$name ));

				$grid['type_param'] = \dibi::getInsertId();
				$grid['type_id'] = 5;
				$grid['type'] = 2;
				break;

			case 'fb-comments':
				$grid['type'] = 2;
				$grid['type_id'] = 6;
				break;

			case 'comments':
				$v=array("groupname"=>'new');
				\DAO\CommentGroup::get()->insert($v);
				$id = \dibi::insertId();
				$grid['type'] = 2;
				$grid['type_id'] = 8;
				$grid['type_param'] = $id;
				break;

			case 'poll':
				\DAO\Poll::get()->insert(array('question'=>'' ));
				$grid['type_param'] = \dibi::getInsertId();
				$grid['type'] = 2;
				$grid['type_id'] = 9;
				break;

			case 'youtube':
				$grid['type'] = 2;
				$grid['type_id'] = 10;
				break;

			case 'carousel':
				$grid['type'] = 2;
				$grid['type_id'] = 12;
				break;

			default:
				break;
		}
		return $grid;
	}

	/**
	 * Unlink directory recursive.
	 *
	 * @param $dir
	 */
	private function rmDirRecursive($dir) {

		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (filetype($dir . "/" . $object) == "dir")
						rrmdir($dir . "/" . $object); else unlink($dir . "/" . $object);
				}
			}
			reset($objects);
			rmdir($dir);
		}
	}

	public function handleMenuOrder( $data ) {

		$main = ARMenu::get()->getChildNodes(0);

		foreach( $data as $v )
		{
			if(is_numeric($v['item_id']))
			{
				$id_menu = $v['item_id'];
				$parent = (is_numeric($v['parent_id']) ? $v['parent_id'] :  0);
				$structure[$parent][] = $id_menu;
				if( $parent == 0 && !in_array($id_menu, $main))
				{
					$this->flashMessage('Tato položka nemůže být v nulté linii struktury menu. Vráceno do původního stavu', "ERROR");
					$this->invalidateControl();
					return;
				}

			}
		}

		dibi::begin();
		foreach( $structure as $parent_id => $v )
		{
			foreach( $v as $order => $id_menu )
			{
				$data = array(
					'parent_id' => $parent_id,
					'menu_order' => $order+1
				);
				ARMenu::get()->update($id_menu, $data);
			}
		}

		$this->rrmdir(ROOT_DIR."/temp/cache/_menu_cache");


		dibi::commit();
	}

	/**
	 * Unused, just in case.
	 */
	private function fixDoubleWrapper()
	{
		$wrong = dibi::query('select site_id, count(*), min( id_wrapper ), max(id_wrapper) from cms_wrapper
			group by site_id
			having count(*)>1
			order by site_id')->fetchAll();

		foreach( $wrong as $w )
		{
			\DAO\CmsGrid::get()->updateWhere("wrapper_id=".$w["max"], array('wrapper_id'=> $w['min']) );
			\DAO\CmsWrapper::get()->delete( $w["max"] );
		}

		echo "OK - ".count($wrong)." has been fixed";
	}
}
