<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class GridFacade
{
	/** @var \AntoninRykalsky\EntityManager */
	protected $em;

	/** @var \ICmsControlsService */
	protected $cmsControlService;



	public function __construct(
		\AntoninRykalsky\EntityManager $em,
		\ICmsControlsService $cmsControlService
	){
		$this->em = $em->getEm();
		$this->cmsControlService = $cmsControlService;
	}

	public function getGrid( $idGrid )
	{
		return $this->em->find('AntoninRykalsky\Entity\CmsGrid', $idGrid );
	}


	public function getStuff()
	{
		return $this->cmsControlService->getCmsControls();
	}

	public function getIcon( $grid )
	{
		$control = $this->cmsControlService->findOut( $grid );
		return $control->getImg();
	}



	public function removeGrid( $gridid )
	{

		\DAO\CmsGrid::get()->delete( $gridid );
	}

	public function pasteGridToGrid( $idgrid, $parentGrid ) {

		$g = \DAO\CmsRepository::get()->findAll()->where('grid_id=%s', $idgrid)->fetch();
		$g->serialized = json_decode( $g->serialized );
		$insert = (array)$g->serialized;
		$insert['wrapper_id'] = null;
		$insert['parent_id'] = $parentGrid;
		unset( $insert['id_grid'] );
		\DAO\CmsGrid::get()->insert( $insert );
		\DAO\CmsRepository::get()->delete( $g->id );
	}

	private function duplicateGrid( $ser, $wrapper, $g )
	{
		$insert = (array)$ser;

		if( $ser->type == 1 )
		{
			$article = \DAO\CmsArticle::get()->find( $ser->type_id )->fetch();
			unset( $article['id_article'] );
			\DAO\CmsArticle::get()->insert( (array)$article );
			$insert['type_id'] = \dibi::insertId();
//			print_r( $article );exit;
		}

		$insert['wrapper_id'] = $wrapper;
		unset( $insert['id_grid'] );
		\DAO\CmsGrid::get()->insert( $insert );


	}

	public function pasteGridToSite( $idgrid, $idsite ) {

		$wrapper = null;
		$site = Cms\Site::get( $idsite );
		foreach( $site->wrapper as $w )
		{
			$wrapper = $w->id_wrapper;
			break;
		}

		$g = \DAO\CmsRepository::get()->findAll()->where('grid_id=%s', $idgrid)->fetch();
		$ser = $g->serialized = json_decode( $g->serialized );
		$this->duplicateGrid( $ser, $wrapper, $g );




	}

	public function copyGrid( $idgrid )
	{
		$g = \DAO\CmsGrid::get()->find($idgrid)->fetch();
		$torepo = array(
			'idu'=>  \AntoninRykalsky\SystemUser::get()->idu,
			'shortname'=>  'Prvek 4.'.$idgrid,
			'type'=>  $g->type,
			'serialized' => json_encode( $g ),
			'grid_id' => $idgrid
		);
		\DAO\CmsRepository::get()->insert( $torepo );
	}

	public function deleteFromRepo( $idgrid )
	{
		\DAO\CmsRepository::get()->deleteWhere(array('grid_id' => $idgrid ));
	}

	/**
	 * Function for latte helper. Returns links for editation forms of different controls
	 * @param type $grid
	 * @param type $presenter
	 * @return type
	 */
	public function getGridAdminOption( $grid, $presenter )
	{
		$control = $this->cmsControlService->findOut( $grid );
		$links = (array)$control->getEditLinks();

		$htmlLinks = \Nette\Utils\Html::el('');
		foreach( $links as $link )
		{
			switch ($link->getIdType()) {
				case \EditLink::TYPE_ID:
					$id = $grid->getTypeId();
					break;

				case \EditLink::TYPE_PARAM:
					$id = $grid->getTypeParam();
					break;

//				case \EditLink::TYPE_PARAM_JSON:
//					$json = json_decode($grid->getTypeParam());
//					$id = $json->id;
//					break;

				case \EditLink::ID:
					$id = $grid->getId();
					break;

				default:
					throw new \Exception("TYPE PARAM");
			}

			$htmlLink = \Nette\Utils\Html::el('a');
			$htmlLink->href(
					$presenter->link( $link->getLink(), $id )
				);
			$htmlLink->setText( $link->getText() );
			$htmlLinks->add( $htmlLink );
		}
		return $htmlLinks;
	}

}
