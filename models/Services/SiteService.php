<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

class SiteService
{
	/** @var \AntoninRykalsky\EntityManager */
	protected $em;

	public function __construct(
		\AntoninRykalsky\EntityManager $em
	){
		$this->em = $em->getEm();
	}

	public function cloneSite( Entity\CmsSite $original )
	{
		$clone = clone $original;
		foreach( $clone->getWrappers() as $w )
		{
			$cloneW = clone $w;
			$cloneW->setSite( $clone );
			$this->em->persist( $cloneW );

			foreach ( $cloneW->getGrids() as $g )
			{
				$cloneG = clone $g;
				$cloneG->setWrapper( $cloneW );
				$this->em->persist( $cloneG );
			}
		}
		$this->em->persist( $clone );

		return $clone;
	}
}
