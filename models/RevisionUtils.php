<?php

namespace softwarestudio\cmsModule\models;

class RevisionUtils {
	
	public static function getRevision( $file ) {
		
		$v = '';
		// $v = 'tags/0.0.5';
		// $v = 'remotes/origin/feature-deploy';
		if( file_exists($file)) {
			$v .= file_get_contents($file);
		}
		
		$v = preg_replace('#^tags/#', '', $v );
		$v = preg_replace('#^heads/#', '', $v );
		$v = preg_replace('#^remotes/origin/#', '', $v );
		
		return $v;
	}
	
}