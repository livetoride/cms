<?php


namespace AntoninRykalsky\TryCatcher;

use AntoninRykalsky as AR;
use dibi;
use Exception;
use Flashes;
use Nette\Application\UI\Presenter;

/** @deprecated */
class TryCatcher {

	public function tryCatcher( Presenter $presenter, $functionName, $v, $debug = null, $okMessage = null )
	{

		if(empty( $debug )) $debug = false;
		if(empty( $okMessage )) $okMessage = 'Operace byla úspěšně dokončena';
		$return = '';

		if( $debug )
			try {
				dibi::begin();
				eval( '$return='.$functionName.'($v);' );
				$presenter->flashMessage( $okMessage, AR\Flashes::$success);
				dibi::commit();
			}
			catch ( Exception $e )
			{
				dibi::rollback();
				$presenter->flashMessage( 'Operace se nezdařila', Flashes::$error);
				$presenter->flashMessage( $e->getMessage(), Flashes::$info);
				return 'fail';
			}

		else {
			eval( '$return='.$functionName.'($v);' );
		}
		return $return;
	}

}