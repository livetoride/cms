<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class YoutubeControl implements \IBaseCmsControl
{
	private $name = 'youtubeControl';
	private $img = 'youtube.png';
	private $type = 2;
	private $typeId = 10;
	private $control = 'YoutubeControl';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function initializationParam()
	{
		$article = new \AntoninRykalsky\Entity\EmbedMedia;
		return $article;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:Rozvadimese:youtube:edit');
		$a->setText('nastavení videa');
		$a->setIdType( \EditLink::TYPE_PARAM );
		$return[]=$a;

		return $return;
	}



}
