<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class GalleryHolderControl implements \IBaseCmsControl
{
	private $name = 'galleryholder';
	private $img = 'gallery-holder.png';
	private $type = 2;
	private $typeId = 21;
	private $control = 'GalleryHolderControl';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function initializationParam()
	{
		$gallery = new \AntoninRykalsky\Entity\GalleryGroup;
		return $gallery;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:GalleryHolder:default');
		$a->setText('Nastavení skupiny');
		$a->setIdType( \EditLink::TYPE_PARAM );
		$return[]=$a;

		return $return;
	}



}
