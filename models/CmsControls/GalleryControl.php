<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class GalleryControl implements \IBaseCmsControl
{
	private $name = 'gallery';
	private $img = 'gallery.png';
	private $type = 2;
	private $typeId = 5;
	private $control = 'GalleryControl';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function initializationParam()
	{
		$gallery = new \AntoninRykalsky\Entity\Gallery;
		return $gallery;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:Gallery:pictures');
		$a->setText('editace galerie');
		$a->setIdType( \EditLink::TYPE_PARAM );
		$return[]=$a;

		return $return;
	}



}
