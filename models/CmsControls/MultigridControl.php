<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class MultigridControl implements \IBaseCmsControl
{
	private $name = 'multigrid';
	private $img = 'grid.png';
	private $type = 3;
	private $typeId = null;
	private $control = null;

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:Cms:Grid:detail');
		$a->setText('editace multi-gridu');
		$a->setIdType( \EditLink::ID );
		$return[]=$a;

		return $return;
	}



}
