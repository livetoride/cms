<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class BflContactControl implements \IBaseCmsControl
{
	private $name = 'contact';
	private $img = 'comment.png';
	private $type = 2;
	private $typeId = 12;
	private $control = 'ContactControl';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function initializationParam()
	{
		$commentGroup = new \AntoninRykalsky\Entity\CommentGroup;
		$commentGroup->setGroup("new");
		return $commentGroup;
	}

	public function getEditLinks()
	{
		$return = array();

//		$a = new \EditLink;
//		$a->setLink(':Admin:rozvadimese:Poll:edit');
//		$a->setText('Nastavení kolotoče -- OPRAVIT');
//		$a->setIdType( \EditLink::TYPE_ID );
//		$return[]=$a;
//
		return $return;
	}



}
