<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class FacebookLikeControl implements \IBaseCmsControl
{
	private $name = 'fblikeControl';
	private $img = 'fb-like.png';
	private $type = 2;
	private $typeId = 11;
	private $control = 'AntoninRykalsky\FacebookLikeControl';


	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:rozvadimese:Poll:edit');
		$a->setText('Nastavení kolotoče -- OPRAVIT');
		$a->setIdType( \EditLink::TYPE_ID );
		$return[]=$a;

		return $return;
	}



}
