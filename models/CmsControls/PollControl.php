<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class PollControl implements \IBaseCmsControl
{
	private $name = 'poll';
	private $img = 'poll.png';
	private $type = 2;
	private $typeId = 9;
	private $control = 'PollControl';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function initializationParam()
	{
		$article = new \AntoninRykalsky\Entity\Poll;
		return $article;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:rozvadimese:Poll:edit');
		$a->setText('nastavení ankety');
		$a->setIdType( \EditLink::TYPE_PARAM );
		$return[]=$a;

		return $return;
	}



}
