<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class ContactFormControl implements \IBaseCmsControl
{
	private $name = 'contactForm';
	private $img = 'contactform.png';
	private $type = 2;
	private $typeId = 13;
	private $control = 'ContactFormControl';


	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}


	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:Cms:Contact:edit');
		$a->setText('Nastavení kontaktního formuláře');
		$a->setIdType( \EditLink::TYPE_PARAM );
		$return[]=$a;

		return $return;
	}



}
