<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class EmptyCmsControl implements \IBaseCmsControl
{
	private $name = '';
	private $img = '';
	private $type = 2;
	private $typeId = 0;
	private $control = null;

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function getEditLinks()
	{
		return array();
	}



}
