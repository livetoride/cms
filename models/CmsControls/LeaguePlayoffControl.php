<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class LeaguePlayoffControl implements \IBaseCmsControl
{
	private $name = 'leagueplayoff';
	private $img = 'league-playoff.png';
	private $type = 2;
	private $typeId = 17;
	private $control = 'LeaguePlayoffControl';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:League:SeasonSettings:default');
		$a->setText('nastavení sezóny pro tabulku playoff');
		$a->setIdType( \EditLink::ID);
		$return[]=$a;

		return $return;
	}



}
