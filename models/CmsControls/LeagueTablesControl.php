<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class LeagueTablesControl implements \IBaseCmsControl
{
	private $name = 'leaguetables';
	private $img = 'league-tables.png';
	private $type = 2;
	private $typeId = 15;
	private $control = 'LeagueTablesControl';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:League:SeasonSettings:default');
		$a->setText('nastavení sezóny');
		$a->setIdType( \EditLink::ID );
		$return[]=$a;

		return $return;
	}



}
