<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class LeagueGoolmanControl implements \IBaseCmsControl
{
	private $name = 'leaguegoolman';
	private $img = 'league-goolmans.png';
	private $type = 2;
	private $typeId = 18;
	private $control = 'LeagueGoalmanControl';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:League:SeasonSettings:default');
		$a->setText('nastavení sezóny pro tabulku brankáčů');
		$a->setIdType( \EditLink::ID );
		$return[]=$a;

		return $return;
	}



}
