<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class CarouselControl implements \IBaseCmsControl
{
	private $name = 'carouselControl';
	private $img = 'carousel.png';
	private $type = 2;
	private $typeId = 12;
	private $control = 'CarouselControl';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function initializationParam()
	{
		$carousel = new \AntoninRykalsky\Entity\Carousel;
		$carousel->setCarousel( "Carousel" );

		return $carousel;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:Carousel:pictures');
		$a->setText('Nastavení kolotoče');
		$a->setIdType( \EditLink::TYPE_PARAM );
		$return[]=$a;

		return $return;
	}



}
