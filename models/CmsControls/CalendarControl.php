<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class CalendarControl implements \IBaseCmsControl
{
	private $name = 'calendar';
	private $img = 'calendar-icon.png';
	private $type = 2;
	private $typeId = 7;
	private $control = 'Calendar';


	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}


	public function getEditLinks()
	{
		$return = array();

//		$a = new \EditLink;
//		$a->setLink(':Admin:rozvadimese:Poll:edit');
//		$a->setText('Nastavení kolotoče -- OPRAVIT');
//		$a->setIdType( \EditLink::TYPE_ID );
//		$return[]=$a;

		return $return;
	}



}
