<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class FacebookCommentControl implements \IBaseCmsControl
{
	private $name = 'fb-comments';
	private $img = 'fb-comments-icon.png';
	private $type = 2;
	private $typeId = 6;
	private $control = 'FacebookComments';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function getEditLinks()
	{
		$return = array();

//		$a = new \EditLink;
//		$a->setLink(':Admin:rozvadimese:Poll:edit');
//		$a->setText('Nastavení kolotoče -- OPRAVIT');
//		$a->setIdType( \EditLink::TYPE_ID );
//		$return[]=$a;

		return $return;
	}



}
