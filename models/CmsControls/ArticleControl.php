<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class ArticleControl implements \IBaseCmsControl
{
	private $name = 'text';
	private $img = 'text2.png';
	private $type = 1;
	private $typeId = null;
	private $control = null;

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	public function initializationId()
	{

		$article = new \AntoninRykalsky\Entity\CmsArticle;
		$article->setArticle('');

		return $article;
	}


	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:Cms:Snippet:edit');
		$a->setText('Inline editace článku');
		$a->setIdType( \EditLink::TYPE_ID );
		$return[]=$a;

		$a = new \EditLink;
		$a->setLink(':Admin:Cms:Snippet:classicEdit');
		$a->setText('Pokročilá editace článku');
		$a->setIdType( \EditLink::TYPE_ID );
		$return[]=$a;

		return $return;
	}



}
