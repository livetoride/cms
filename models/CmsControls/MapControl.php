<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class MapControl implements \IBaseCmsControl
{
	private $name = 'mapControl';
	private $img = 'map.png';
	private $type = 2;
	private $typeId = 14;
	private $control = 'MapControl';


	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}

	
	public function initializationSetting()
	{
		return array(
			'x'=>600,
			'y'=>400,
			'gps_n'=>49.8336850,
			'gps_e'=>18.1636014
		);
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:Cms:Map:detail');
		$a->setText('Nastavení komponenty mapy');
		$a->setIdType( \EditLink::ID );
		$return[]=$a;

		return $return;
	}



}
