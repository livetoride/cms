<?php

namespace AntoninRykalsky\Cms\ControlConfig;

class NewsControl implements \IBaseCmsControl
{
	private $name = 'newsControl';
	private $img = 'newsreel-icon.png';
	private $type = 2;
	private $typeId = 20;
	private $control = 'NewsControl';

	public function getName() {
		return $this->name;
	}

	public function getImg() {
		return $this->img;
	}

	public function getType() {
		return $this->type;
	}

	public function getTypeId() {
		return $this->typeId;
	}

	public function getControlName() {
		return $this->control;
	}
	public function initializationParam()
	{
		
		$article = new \Entity\News;
		return $article;
	}

	public function getEditLinks()
	{
		$return = array();

		$a = new \EditLink;
		$a->setLink(':Admin:Cms:Newsreel:create');
		$a->setText('Přidání novinky');
		$a->setIdType( \EditLink::TYPE_PARAM);
		$return[]=$a;
		
		$b = new \EditLink;
		$b->setLink(':Admin:Cms:Newsreel:list');
		$b->setText('Seznam novinek');
		$b->setIdType( \EditLink::TYPE_PARAM);
		$return[]=$b;

		return $return;
	}



}
