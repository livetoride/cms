<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

use AntoninRykalsky\Entity\CmsGrid;
use AntoninRykalsky\Entity\CmsSite;
use AntoninRykalsky\Entity\Menu;
use CmsControlsService;
use DAO\CmsArticle;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityRepository;

class SiteRepository extends EntityRepository {

	/**
	 * Find and return {@link Site} by link.
	 * @param $id
	 * @return null|object
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 * @throws \Doctrine\ORM\TransactionRequiredException
	 * @throws \Nette\Application\BadRequestException
	 */
	public function getSiteByLink( $id )
	{
		$site = $this->_em->getRepository('\AntoninRykalsky\Entity\CmsSite' )->findOneByUrl( $id );
		try {
			if( empty( $site ))
			{
				$site = $this->_em->find('\AntoninRykalsky\Entity\CmsSite', $id );
			}
		} catch( \Doctrine\DBAL\DBALException $e )
		{
			throw new \Nette\Application\BadRequestException("HledanĂˇ strĂˇnka nebyla nalezena");
		}
		return $site;
	}

	public function findSiteWithMenu( $siteId ) {

		/** @var CmsSite $site */
		$site = $this->_em->find(CmsSite::class, $siteId );

		$inMenu = $this->getInMenuForSite( $site );

		$site->setInMenu($inMenu);

		return $site;
	}

	/**
	 * Returns menu, that targeting to site.
	 */
	private function getInMenuForSite( CmsSite $site ) {
		$where = [
			'link' => CmsSite::NETTE_LINK,
			'param' => $site->getUrl()
		];
		return $this->_em->getRepository(Menu::class)->findBy($where);
	}

	/**
	 * Returns site entity by url or ID.
	 *
	 * @param $siteId
	 */
	public function getSiteBy( $siteId ) {

		if(!is_numeric( $siteId ))
		{
			$site = \DAO\CmsSite::get()->findAll()->where('url=%s', $siteId )->fetch();
			if( !empty( $site->id ))
				$siteId = $site->id;
		}
		if( empty( $siteId ))
		{
			$site = \DAO\CmsSite::get()->findAll()->where('homepage=1')->fetch();
			if( !empty( $site->id ))
				$siteId = $site->id;
		}

		return $this->_em->find(CmsSite::class, $siteId);
	}

	public function deleteSite( CmsSite $site ) {

		foreach( $this->getInMenuForSite($site) as $menu ) {
			$this->_em->remove($menu);
		}

		foreach( $site->getWrappers() as $wrapper ) {

			foreach( $wrapper->getGrids() as $grid ) {

				// TODO more comples
				if( $grid->getType() == CmsControlsService::GRID_TYPE_ARTICLE ) {

					\DAO\CmsArticle::get()->delete( $grid->getTypeId() );
				}

				$this->deleteSubgrids($grid);
				$this->_em->remove( $grid );
			}

			$this->_em->remove( $wrapper );
		}
		// TODO components

		$this->_em->remove( $site );
		$this->_em->flush();
	}

	private function deleteSubgrids( CmsGrid $grid ) {

		foreach( $grid->getSubGrids() as $subGrid ) {

			$this->deleteSubgrids( $subGrid );
			$this->_em->remove( $subGrid );
		}
	}

}
