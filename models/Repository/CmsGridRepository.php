<?php
/**
 * Model zacházející s tabulkou produkty
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2013 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AntoninRykalsky;

use Doctrine\ORM\EntityRepository;


class CmsGridRepository extends EntityRepository
{
	private function recursiveFindGrid( $grid, $a = array() )
	{
		$a[] = $grid;
		$parent = $grid->getParentGrid();
		if( $parent != null )
		{
			$a = $this->recursiveFindGrid( $parent, $a );

		}
		return $a;
	}


	private function fulltextGetArticles( $text )
	{
		$items = explode(' ', strtolower($text));

		$text = implode(',', $items);
		$q = \dibi::query("SELECT id_article, article "
				. "FROM cms_article "
				. "WHERE to_tsvector(article) @@ to_tsquery(%s) "
				. "ORDER BY ts_rank(to_tsvector(article), to_tsquery(%s)) DESC, to_tsvector(article);", $text, $text )
				->fetchAssoc('id_article');
		return $q;
	}

	private function fulltextGetSitesIds( $text )
	{
		$items = explode(' ', strtolower($text));
		$text = implode('|', $items);
		$q = \dibi::query("SELECT id FROM cms_site WHERE to_tsvector(site) @@ to_tsquery(%s)", $text )->fetchAssoc('id');
		$keys = array_keys($q);
		return $keys;
	}
	private function fulltextFindoutSiteDesc( $grid, $patterns, $article )
	{
		$minLength = 300;

		$description = '';

		$descriptions = array();
		$tmp1 = explode("\n", strip_tags( $article ) );
		foreach( $tmp1 as $item )
		{
			$item.='## ';
			$tmp2 = explode(".", $item);
			foreach( $tmp2 as $k => $v )
			{
				$tmp2[$k].='. ';
			}
			$descriptions  = array_merge($descriptions, $tmp2) ;
		}

		foreach( $descriptions as $sentence )
		{
			foreach( $patterns as $pattern )
			{
				if( preg_match("#$pattern#", strtolower($sentence)) )
				{
					$sentence = \Nette\utils\strings::truncate($sentence, $minLength);
					$description .= ".. ".$sentence;
					if( count($description) > $minLength )
					{
						return \Nette\utils\strings::truncate($description, $minLength);
					}
				}
			}
		}

		return \Nette\utils\strings::truncate($description, $minLength);
	}

	public function getSiteFulltext( $text )
	{


/*		$sitesId = $this->fulltextGetSitesIds( $text );
		$q = $this->_em->createQuery(
			"select s from AntoninRykalsky\Entity\CmsSite s "
			. "where s.id IN( :in )");
		$q->setParameter('in', $sitesId );
		$sites = $q->getResult();*/

//		foreach( $sites as $site )
//		{
//			echo $site->getSite();
//			echo "<br />";
//			echo "<br />";
//		}
//		exit;

		$items = explode(' ', strtolower($text));

		$sites = array();
		$articlesTmp = $this->fulltextGetArticles( $text );
		$articlesId = array_keys($articlesTmp);


		$q = $this->_em->createQuery(
			"select g from AntoninRykalsky\Entity\CmsGrid g "
			. "where g.type=1 AND g.typeId IN (:expression)");
		$q->setParameter('expression', $articlesId );
		$grids = $q->getResult();
		foreach( $articlesTmp as $articleK => $ariticleItem )
		{
			foreach ($grids as $v )
			{
				$k = $v->getTypeId();
				if( $articleK == $k ){
					$grid = $v;
				}
			}


			$nesting = $this->findGridNestig( $grid->getId() );
			if( $nesting[0] instanceof \AntoninRykalsky\Entity\CmsSite )
			{
				$site = $nesting[0];
				$site->setTmpFulltext( $this->fulltextFindoutSiteDesc( $grid, $items, $ariticleItem['article'] ) );
				if( empty( $sites[ $site->getId() ] ))
				{
					$sites[ $site->getId() ] = $site;
				}
			}
		}
	//	print_r( $sites );exit;
	//	$sites = array_unique($sites);
	/*	foreach( $sites as $site )
		{

			echo 'id = ' . $site->getId();
			echo $site->getTitle();
			echo "<br />";
			echo $site->getTmpFulltext();
			echo "<br />";
			echo "<br />";
		}*/
//



		return $sites;
	}

	public function findGridNestig( $id )
	{
		/* @var $grid \AntoninRykalsky\Entity\CmsGrid */
		/* @var $s \AntoninRykalsky\Entity\CmsSite */
		$grid = $this->find($id);

		$nesting = $multigrids = $this->recursiveFindGrid( $grid );

		$lastGrid = end($multigrids);
		if( empty( $lastGrid ))
		{
			$lastGrid = $grid;
		}


		if( $lastGrid->getParentGrid() === null )
		{
			$w=$lastGrid->getWrapper();
			if( $w !== null )
			{
				$s= $w->getSite();
				$s->setSite( $s->getSite() );
				$nesting[] = $s;
			}
		}
		return array_reverse($nesting);

	}

}
