<?php

namespace AntoninRykalsky;

use	Nette\Application\UI;

use Nette\Utils\Html;
use Tracy\Debugger; #for helpers

interface ICategoryControl
{
	/** @return CategoryControl */
	function create();
}

class CategoryControl extends UI\Control
{
	/** @var MenuFacade */
	protected $menuFacade;

	/** @var SiteFacade */
	protected $siteFacade;

	private $adminMode = 0;




	public function __construct( MenuFacade $menuFacade, SiteFacade $siteFacade ) {

		$this->menuFacade = $menuFacade;
		$this->siteFacade = $siteFacade;
	}

	public function render() {

		$this->fillTemplate();

		$this->template->addFilter('CmsActions', array($this, 'cmsActions') );
		$this->template->addFilter('CmsIcon', array($this, 'CmsIcon') );

		$this->template->setFile(dirname(__FILE__) .'/category.phtml');
		$this->template->render();
	}

	public function handleOperationHandler( array $pages, $operation ) {

		switch($operation) {
			case "delete":
				$this->siteFacade->deleteSites( $pages );
				$this->flashMessage("Vybrané položky byly vymazány", \Flashes::$success);
				break;
			default:
				throw new \Exception("Unallowed operation");
		}

		$this->presenter->redrawControl();
	}

    private function fillTemplate()
	{
		$this->template->items = $this->menuFacade->getStructured();
		$this->template->controlName=$this->getName();


		$this->template->menuitem = Menu::get()->getMenuItems();

		if( $this->adminMode )
		{
			$this->template->mainSection = $this->menuFacade->getCompleteMainSections();
			$this->template->parents = Menu::get()->getCompleteChildNodes(0);
			$this->template->id_resource = null;
		} else {
			$res = array("news-member", "news-frontend", "guest-only");
			$this->template->mainSection = $this->menuFacade->getMainSections();
			$this->template->parents = Menu::get()->getChildNodes(0, $res);
			$this->template->id_resource = $res;
		}

		$this->template->nodes = Menu::get();
		$this->template->primary = 'id_menu';

		$this->template->section = 'frontend';
	}
	
	public function cmsActions( $child, $iconfolder, $entity, $section )
	{
		if( $child->parent_id == 0 ) 
		{
			return '';
		}
		
		elseif ( $entity->getLink() == ':Front:News:show' ) 
		{
			return 
				'<a href="'.$this->presenter->link(':Front:News:show', array($entity->getParam())).'}" target="_blank">
					<img src="'.$iconfolder.'zoom.png" class="hastip showpage" title="Otevřít článek v novém listě" alt="Otevřít článek v novém listě" />
				</a>
				<a href="'.$this->presenter->link('edit', array($child->id_menu, $section)).'">
					<img src="'.$iconfolder.'edit.gif" class="hastip" title="Uprav článek" alt="Uprav článek" />
				</a>
				<a href="'.$this->presenter->link('confirmForm:confirmDelete!', array('id' => $child->id_menu)).'" class="ajax">
					<img src="'.$iconfolder.'delete.gif" class="hastip" title="Vymazat článek" alt="Vymazat článek" />
				</a>';
		}
		
		elseif( $entity->getLink() == ':Front:Site:default' || $entity->getLink()==':Front:Homepage:default') 
		{
			return 
				'<a href="'.$this->presenter->link(':Front:Site:default', array($entity->getParam())).'" target="_blank">
					<img src="'.$iconfolder.'zoom.png" class="hastip showpage" title="Otevřít článek v novém listě" alt="Otevřít článek v novém listě"  />
				</a>
				<a href="'.$this->presenter->link(':Admin:Cms:Site:detail', array($child->param)).'">
					<img src="'.$iconfolder.'edit.gif" class="hastip" title="Uprav článek" alt="Uprav článek" />
				</a>
				<a href="'.$this->presenter->link('confirmForm:confirmDelete!', array('id' => $child->id_menu)).'" class="ajax">
					<img src="'.$iconfolder.'delete.gif" class="hastip" title="Vymazat článek" alt="Vymazat článek" />
				</a>';
			
		} 
		
		elseif ( $entity->getLink() == '' && (empty($child->parent_id) || $child->parent_id != 0) )
		{
			return 
				'<a href="'. $this->presenter->link('edit', array( $child->id_menu, $section) ).'">
					<img src="'.$iconfolder.'edit.gif" class="hastip" title="Uprav článek" alt="Uprav článek" />
				</a>
				<a href="'. $this->presenter->link('confirmForm:confirmDelete!', array( 'id' => $child->id_menu )).'" class="ajax">
					<img src="'.$iconfolder.'delete.gif" class="hastip" title="Vymazat článek" alt="Vymazat článek" />
				</a>';
		}
		
		elseif( $entity->getLink() == ':News:archiveList' )
		{
			return '<a href="'.$this->presenter->link('', array()).'archiveList id => $child->id_clanek}">správa zpravodaje</a>';
		} else {
			return 
				'
				<a href="'.$this->presenter->link('confirmForm:confirmDelete!', array('id' => $child->id_menu)).'" class="ajax">
					<img src="'.$iconfolder.'delete.gif" class="hastip" title="Vymazat článek" alt="Vymazat článek" />
				</a>';
		}
		
	}




	public function adminMode($adminMode) { $this->adminMode = $adminMode; }
	
	public function CmsIcon($child, $entity,  $baseUri)
    {
		$el = Html::el('img');
		$el->class[] = 'hastip';

		if( !empty( $child->id_category ))
		{
			return;
		}

		if( $child->homepage == 1 ){
			$el->src[] = $baseUri.'homepage.png';
			$el->title[] = "Homepage";
			return $el;
        }
        if( $entity->getLink() == ':Front:News:show' && $child->id_resource == 'news-member'){
			$el->src[] = $baseUri.'users.gif';
			$el->title[] = "Položka pouze pro klienty";
			return $el;
        }
		elseif( $entity->getLink() == ':Front:News:show' && $child->active ){
			$el->src[] = $baseUri.'page.png';
			$el->title[] = "Veřejná položka";
			return $el;
        }
		elseif( $entity->getLink() == ':Front:News:show' && !$child->active ){
			$el->src[] = $baseUri.'page_delete.png';
			$el->title[] = "Archivní položka - nebude se zobrazovat";
			return $el;
        }
		elseif( $entity->getLink() == '' ){
			$el->src[] = $baseUri.'fingerboard.png';
			$el->title[] = "Rozcestník";
			return $el;
        }
        if( $entity->getLink() == ':Front:Site:default' && $child->id_resource == 'news-member'){
			$el->src[] = $baseUri.'users.gif';
			$el->title[] = "Položka pouze pro klienty";
			return $el;
        }
		elseif( $entity->getLink() == ':Front:Site:default' && $child->active ){
			$el->src[] = $baseUri.'page.png';
			$el->title[] = "Veřejná položka";
			return $el;
        }
		elseif( $entity->getLink() == ':Front:Site:default' && !$child->active ){
			$el->src[] = $baseUri.'page_delete.png';
			$el->title[] = "Archivní položka - nebude se zobrazovat";
			return $el;
        }
		elseif( $entity->getLink() == '' ){

			$el->src[] = $baseUri.'page.png';
			$el->title[] = "Nezařazeno v menu";
			return $el;

        } else {
			$el->src[] = $baseUri.'box.png';
			$el->title[] = "[".$entity->getLink()."]";
			return $el;
		}
    }


}
