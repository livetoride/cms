<?php

namespace AntoninRykalsky;
use AntoninRykalsky as AR,
	Nette\Application\UI,
	Nette\Utils\Html;


/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class CmsRepo extends UI\Control
{
	/** @var GridFacade */
	protected $gridFacade;

	public function __construct( $gridFacade ) {
		$this->gridFacade = $gridFacade;
	}

	public function handleDeleteFromRepo( $idgrid )
	{
		$this->gridFacade->deleteFromRepo( $idgrid );
		$this->presenter->invalidateControl('cmsRepositorySnippet');
	}

	public function setUser( $idu )
	{
		$r = \DAO\CmsRepository::get()->findAll()->where("idu=%i", $idu)->fetchAll();
		foreach ( $r as &$repoItem)
		{
			$repoItem['serialized'] = json_decode($repoItem['serialized']);
		}
		$this->template->cmsRepo = $r;
	}
    public function render( )
    {
        $this->template->setFile(dirname(__FILE__) .'/cmsRepo.phtml');
        $this->template->render();
    }
}
