<?php

namespace SoftwareStudio\Cms\Controls;

use AntoninRykalsky as AR;
use ConfirmationDialog;
use Nette\Application\UI;
use Nette\DI\Container;


class ConfirmationDialogFactory {

	private $isAjax=true;

	private $container;

	public function __construct( Container $container ) {
		$this->container = $container;
	}

	public function create() {

		$session = $this->container->getByType('Nette\Http\Session');
		$sessionSection = $session->getSection('confirmationDialog');

		ConfirmationDialog::$_strings['yes'] = "ano";
		ConfirmationDialog::$_strings['no'] = "ne";
		ConfirmationDialog::$_strings['expired'] = 'Požadavek potvrzujícího dialogu vypršel. Prosím opakujte tuto akci.';

		$form = new \ConfirmationDialog($sessionSection);
		if( $this->isAjax ) {
			$form->getFormElementPrototype()->addClass('ajax');
		}
		$form->cssClass = 'static_dialog';

		$form->setAttachedJs('<script>AntoninRykalsky.confirmform.register();</script>');

		return $form;
	}

	/**
	 * @param boolean $isAjax
	 */
	public function setIsAjax($isAjax) { $this->isAjax = $isAjax; }
}
