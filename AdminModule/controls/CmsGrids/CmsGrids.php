<?php

namespace AntoninRykalsky;
use AntoninRykalsky as AR,
	Nette\Application\UI,
	Nette\Utils\Html;


/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class CmsGrids extends UI\Control
{
	/** @var GridFacade */
	protected $gridFacade;

	private $siteId;

	private $type;

	const TYPE_GRID='g';
	const TYPE_SITE='s';

	static $presenter;


	public function __construct( $gridFacade ) {
		$this->gridFacade = $gridFacade;
	}


	public function render( )
	{
		$this->template->stuff = $this->gridFacade->getStuff();
		$this->template->componentName = $this->name;

		$this->template->siteId = $this->siteId;

		CmsGrids::$presenter = $this;
		$this->template->registerHelper('icon', '\AntoninRykalsky\CmsGrids::iconHelper');
		$this->template->registerHelper('gridOption', '\AntoninRykalsky\CmsGrids::gridOptionHelper');

		$this->template->setFile(dirname(__FILE__) .'/cmsGrids.phtml');
		$this->template->render();
	}

	/**
	 * Configure {@link CmsGrids} control for rendering site.
	 * @param $siteEntity
	 * @param $site
	 */
	public function set( $siteEntity, $site ) {

		$this->template->maxGridSize = 12;
		$this->siteId = $site->site->id;
		$this->type = self::TYPE_SITE;
		$this->template->grids = $siteEntity->getGrids();
	}

	/**
	 * Configure {@link CmsGrids} control for rendering multigrid.
	 *
	 * @param $maingrid
	 */
	public function setGrid( $maingrid )
	{
		$this->template->maxGridSize = $maingrid->getWidth();
		$this->siteId = $maingrid->getId();
		$this->type = self::TYPE_GRID;
		$this->template->grids = $maingrid->getSubgrids();
	}

	public static function iconHelper( $grid )
	{
		$container = \Nette\Environment::getContext();
		$gridFacade = $container->getByType('\AntoninRykalsky\GridFacade');
		return $gridFacade->getIcon( $grid );
	}


	public static function gridOptionHelper( $grid, $p )
	{
		$container = \Nette\Environment::getContext();
		/* @var $gridFacade \AntoninRykalsky\GridFacade */
		$gridFacade = $container->getByType('\AntoninRykalsky\GridFacade');
		return $gridFacade->getGridAdminOption( $grid, $p );
	}

	public function handleResizeGrid($grid, $width) {

		if( $width > 12 ) return;

		\DAO\CmsGrid::get()->update($grid, array('width' => round($width)));
	}

	/**
	 * Changes order of grid in {@link CmsGrids}.
	 *
	 * @param $order
	 */
	public function handleChangeOrder($order) {
		$order = explode('-', $order);
		$i = 0;

		foreach( $order as $idGrid ){
			$i++;
			\DAO\CmsGrid::get()->update($idGrid, array('order' => $i ));
		}
		$this->redrawControl();
	}

	/**
	 * Handles copying grid intro cms repository.
	 *
	 * @param $idgrid
	 */
	public function handleCopyGrid( $idgrid )
	{
		$this->gridFacade->copyGrid( $idgrid );
		$this->presenter->redrawControl('cmsRepositorySnippet');
		$this->presenter->redrawControl('flashes');
	}

	/**
	 * Paste grid from Cms Repository to CmsGrids component.
	 *
	 * @param $idgrid
	 */
	public function handlePasteGrid( $idgrid ) {

		switch ($this->type) {
			case self::TYPE_GRID:
				$this->gridFacade->pasteGridToGrid( $idgrid, $this->siteId );
				break;

			case self::TYPE_SITE:
				$this->gridFacade->pasteGridToSite( $idgrid, $this->siteId );
				break;

			default:
				throw new \Exception("Probably missing correct initialization. User set() or setGrid()");
				break;
		}
		$this->presenter->redrawControl();
	}

	/**
	 * Remove grid from {@link CmsGrids}.
	 *
	 * @param $gridid
	 */
	public function handleRemoveGrid( $gridid ) {

		$this->gridFacade->removeGrid( $gridid );
		$this->presenter->redrawControl();
	}
}
