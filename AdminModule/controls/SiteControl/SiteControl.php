<?php

namespace SoftwareStudio;

use AntoninRykalsky as AR,
	Nette\Application\UI,
	Nette\Utils\Html;

use Nette\Application\UI\Form;
use AntoninRykalsky;
use Nette\Utils\Strings;


/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class SiteControl extends UI\Control
{
    public function render()
    {
		$this->template->site = $this->siteEntity;
		
		$site = AntoninRykalsky\Cms\Site::getBySite(  $this->siteEntity->getId() );
		$this->template->site = $site;
		$this->template->in = $site->getContent();
		
        $this->template->setFile(dirname(__FILE__) .'/temp.phtml');
        $this->template->render();
    }
	
	
	private $siteEntity;
	public function setSite( $siteEntity )
	{
		$this->siteEntity = $siteEntity;
		$d = \DAO\CmsSite::get()->find( $siteEntity->getId() )->fetch();
		$d['renderh1']=!$d['dontrenderh1'];
			$links = array(':Front:Site:default', '' );
			$menuZarazeni = \DAO\Menu::get()->findAll()->where('(link IN %in OR link IS NULL ) AND param=%s',$links , $siteEntity->getUrl() )->fetchAll();
			foreach( $menuZarazeni as $m )
			{
				$d['lastRenderedAutomatically'] = $m['last_rendered_automatically'];
				$d['registredOnly'] = $m['id_resource'] !== 'news-frontend';
				$d['fingerboard'] = empty( $m['link'] );
			}
			
		$this['siteForm']->setDefaults( (array)$d );
	}
	
	protected function createComponentSiteForm() {
		$form = new Form();
//		$renderer = $form->getRenderer();

		$form->addHidden('id');
		$form->addText('site', 'stránka');
		$form['site']->addRule(Form::FILLED, 'Položka nadpis v menu je povinná.');

		$form->addText('title', 'html title');
		$form->addTextarea('description', 'html description');
		$form->addTextarea('keywords', 'html keywords');
		$form->addCheckbox('homepage', 'domovská stránka');
		$form->addCheckbox('renderh1', 'zobrazit nadpis stránky');

		$form->addCheckbox('fingerboard', 'Pouze rozcestník');
		$form->addCheckbox('lastRenderedAutomatically', 'Nevykresluj podsekce v menu');
		$form->addCheckbox('registredOnly', 'Pouze pro registrované');
		
		$form['fingerboard']->getControlPrototype()->id = "checkbox-fingerboard";
		$form['lastRenderedAutomatically']->getControlPrototype()->id = "checkbox-lastRenderedAutomatically";
		$form['registredOnly']->getControlPrototype()->id = "checkbox-registredOnly";

		$form->addSubmit('save', 'Uložit');
		$form->addSubmit('saveNInclude', 'Uložit a přejít k zařazení');
		$form->onSuccess[] = array($this, 'submitedSiteForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}
	
	function submitedSiteForm(Form $form) {

		if ((isset($form['save']) && $form['save']->isSubmittedBy())
				||
			(isset($form['saveNInclude']) && $form['saveNInclude']->isSubmittedBy())
			) {
			$v = (array) $form->getValues();


			$v['dontrenderh1']=!$v['renderh1'];
			$v['url']= Strings::webalize( $v['site'] );

			if( $v['homepage'] )
			{
				\DAO\CmsSite::get()->updateWhere(array('homepage'=>1), array('homepage'=>0));
			}



			if( !empty( $v['id'] ))
			{
				$old = \DAO\CmsSite::get()->find( $v['id'] )->fetch();



				\DAO\CmsSite::get()->update( $v['id'], $v );

				$links = array(':Front:Site:default', '' );
				$menuZarazeni = \DAO\Menu::get()->findAll()->where('(link IN %in OR link IS NULL ) AND param=%s',$links , $old->url )->fetchAll();
				foreach( $menuZarazeni as $m )
				{
					$params = array('menu'=>$v['site'], 'param'=>$v['url']);

					if( $v['fingerboard'] ) {
						$params['link'] = null;
					} else {
						$params['link'] = ':Front:Site:default';
					}
					if( $v['lastRenderedAutomatically'] ) {
						$params['last_rendered_automatically'] = 1;
					} else {
						$params['last_rendered_automatically'] = 0;
					}
					if( $v['registredOnly'] ) {
						$params['id_resource'] = 'news-member';
					} else {
						$params['id_resource'] = 'news-frontend';
					}

					\DAO\Menu::get()->update($m->id_menu, $params );
				}

			}
			else {
				\DAO\CmsSite::get()->insert( $v );

			}
		}
		if (isset($form['saveNInclude']) && $form['saveNInclude']->isSubmittedBy())
		{
			$id = $this->getParameter('id');
			$site = AntoninRykalsky\Cms\Site::getBySite($id);
			$site->getContent();

			$this->redirect(':Admin:Cms:Pages:list');

		} else
			$this->redirect('this', array('id' => $v['url']));
	}
}
