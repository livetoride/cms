<?php

use AntoninRykalsky as AR,
	Nette\Application\UI,
	Nette\Utils\Html;


/**
 * MenuControl
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class aGalleryControl extends UI\Control
{
	var $id_menu;

	public function setMenuId( $id_menu )
	{
		$this->id_menu = $id_menu;
	}

    public function render( $section='main-menu' )
    {
		if( !is_numeric($this->id_menu) )
		{
			return;
		}

		$m = AR\Menu::get()->find($this->id_menu)->fetch();

		$r = AR\Gallery::get()->findAll()->where('id_menu=%i', $m->id_menu )->fetchAll();
		$ri = AR\GalleryImages::get()->imagesByMenu($m->id_menu)->orderBy('image_order')->fetchAll();
		$rg = AR\GalleryGroup::get()->find($m->id_menu)->fetch();

		$this->template->title = @$rg->html_title;
		$this->template->htmldesctiption = @$rg->html_description;
		$this->template->htmlkeywords = @$rg->html_keywords;

		$ri2=array();
		foreach( $ri as $v)
		{
			$ri2[ $v['id_gallery'] ][] = $v;
		}

		$this->template->ref = $r;
		$this->template->ri = $ri2;
		$this->template->rg = $rg;

        $this->template->setFile(dirname(__FILE__) .'/gallery.phtml');
        $this->template->render();
    }
}
