<?php
use AntoninRykalsky as AR;

/**
 * Styles for pages lists
 *
 * @author Antonin Rykalsky @ 2oo9
 */
class PagesListControl extends MenuBaseControl implements \AntoninRykalsky\IControl
{
	/** links required css and js */
	public function getRequired()
	{
		if(file_exists(WWW_DIR.'/styles/admin-cms-pages-list.css'))
		{
			echo "check if you should delete /styles/admin-cms-pages-list.css";
			exit;
		}
		 return array(
			  'dir' => __DIR__,
			  'class' => preg_replace('#\\\#', '', __CLASS__),
			  'css' => array('admin-cms-pages-list.css'),
			  'js' => array()
		 );
	}
}
