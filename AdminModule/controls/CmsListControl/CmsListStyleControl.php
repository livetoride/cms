<?php
use AntoninRykalsky as AR;

class CmsListStyleControl implements \AntoninRykalsky\IControl
{

    public function render(  )
    {
    }

	/** links required css and js */
	public function getRequired()
	{
		 return array(
			  'dir' => __DIR__,
			  'class' => preg_replace('#\\\#', '', __CLASS__),
			  'css' => array('admin-cms-pages-list.css', 'admin-cms-pages-edit.css'),
			  'js' => array()
		 );
	}
}
