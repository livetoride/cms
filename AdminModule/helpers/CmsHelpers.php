<?php
/**
 * FormatingHelpers
 *
 * @author Antonin Rykalsky @ 2oo9
 */


namespace AdminModule\CmsModule;
use Nette\Utils\Html;

class CmsHelpers extends \StandardHelpers {

	/**
	 *
	 * @return <type> Automatický loader helperů
	 */
	public static function loader($helper)
    {
        if (method_exists(__CLASS__, $helper)) {
            return callback(__CLASS__, $helper);
        }
    }

	public static function CmsIcon($child, $baseUri)
    {
		$el = Html::el('img');
		$el->class[] = 'hastip';

		if( !empty( $child->id_category ))
		{
			return;
		}

		if( $child->homepage == 1 ){
			$el->src[] = $baseUri.'homepage.png';
			$el->title[] = "Homepage";
			return $el;
        }
        if( $child->link == ':Front:News:show' && $child->id_resource == 'news-member'){
			$el->src[] = $baseUri.'users.gif';
			$el->title[] = "Položka pouze pro klienty";
			return $el;
        }
		elseif( $child->link == ':Front:News:show' && $child->active ){
			$el->src[] = $baseUri.'page.png';
			$el->title[] = "Veřejná položka";
			return $el;
        }
		elseif( $child->link == ':Front:News:show' && !$child->active ){
			$el->src[] = $baseUri.'page_delete.png';
			$el->title[] = "Archivní položka - nebude se zobrazovat";
			return $el;
        }
        if( $child->link == ':Front:Site:default' && $child->id_resource == 'news-member'){
			$el->src[] = $baseUri.'users.gif';
			$el->title[] = "Položka pouze pro klienty";
			return $el;
        }
		elseif( $child->link == ':Front:Site:default' && $child->active ){
			$el->src[] = $baseUri.'page.png';
			$el->title[] = "Veřejná položka";
			return $el;
        }
		elseif( $child->link == ':Front:Site:default' && !$child->active ){
			$el->src[] = $baseUri.'page_delete.png';
			$el->title[] = "Archivní položka - nebude se zobrazovat";
			return $el;
        }
		elseif( $child->link == '' ){
			$el->src[] = $baseUri.'page.png';
			$el->title[] = "Nezařazeno v menu";
			return $el;
        } else
		{
			$el->src[] = $baseUri.'box.png';
			$el->title[] = "[".$child->link."]";
			return $el;
		}
    }



}
