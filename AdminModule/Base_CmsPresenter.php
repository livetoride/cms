<?php
/**
 * Základní presenter apliakce
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

use Nette\Environment;

abstract class Base_CmsPresenter extends \BasePresenter
{
	/** @var AntoninRykalsky\SiteFacade */
	protected $siteFacade;

	/** @var AntoninRykalsky\GridFacade */
	protected $gridFacade;

	function injectCmsStaff(
			AntoninRykalsky\SiteFacade $siteFacade,
			AntoninRykalsky\GridFacade $gridFacade ) {
		$this->siteFacade = $siteFacade;
		$this->gridFacade = $gridFacade;
	}

	public function createComponentCmsRepo()
	{
		$r = new AntoninRykalsky\CmsRepo( $this->gridFacade );
		return $r;
	}

	public function createComponentCmsGrids()
	{
		$r = new AntoninRykalsky\CmsGrids( $this->gridFacade );
		return $r;
	}

	protected function startup()
	{
		parent::startup();
		$this->template->registerHelperLoader('AdminModule\CmsModule\CmsHelpers::loader');
		$this->template->layoutFile = APP_DIR.'/modules/AdminModule/templates/@layout.latte';
	}


}
