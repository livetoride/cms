<?php
/**
 * Administrace novinek a článků
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

namespace AdminModule\CmsModule;
use Nette\Utils\Strings;
use Nette\Application\UI\Form;
use AntoninRykalsky;

class GridPresenter extends \Base_CmsPresenter {

	public function actionDetail($id) 	{

		/* @var $grid \AntoninRykalsky\Entity\CmsGrid */
		$grid = $this->siteFacade->getGrid( $id );

		$info = $this->siteFacade->getGridInfo( $id );
		$this->breadDefault = 0;

		$section = $this->getComponent('navigation');
		$sectionLast=null;
		foreach( $info as $infoItem )
		{
			if(method_exists($infoItem, 'getSite'))
			{
				$sectionLast = $section->add( $infoItem->getSite(), 'seznam.cz');

			} else {
				$title = "Multigrid ".$infoItem->getId();
				if( isset( $sectionLast )) # grid which is not owned by site
				{
					$sectionLast = $sectionLast->add( $title, 'seznam.cz');
				}

			}
		}

		if( isset( $sectionLast )) { # grid which is not owned by site
			$section->setCurrent($sectionLast);
		}
		$this->template->title = $title;

		$idu = \AntoninRykalsky\SystemUser::get()->idu;
		$this['cmsGrids']->setGrid( $grid );
		$this['cmsRepo']->setUser( $idu );

		$this->template->grids = $grid->getSubGrids();
		$this->template->maingrid = $grid;
	}
}
