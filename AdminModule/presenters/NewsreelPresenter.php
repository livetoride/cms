<?php

/**
 * Description of NewsreelPresenter
 *
 * @author Honzik
 */
namespace AdminModule\CmsModule;
use DAO\Newsreel;
use ConfirmationDialog;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use Nette\Application\UI\Form;
use Nette\Environment;
use FormatingHelpers;
class NewsreelPresenter extends \Base_CmsPresenter {

	
	/** @var NewsFacade */
	protected $newsFacade;

	function injectPartner(NewsFacade $newsFacade) {
		$this->newsFacade = $newsFacade;
	}
	/**
	 * (non-phpDoc)
	 *
	 * @see Nette\Application\Presenter#startup()
	 */
	protected function startup() {
		parent::startup();
	}

	public function renderList($type_id) {
		// breadcrumb
		$this->template->title = 'Správa aktualit';
		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'správa článků', $this->link('Novinky:list'));
		$subsubsection = $subsection->add( $this->template->title, '');
		$section->setCurrent($subsubsection);

		$this->template->items = Newsreel::get()->findAll()->fetchAll();
		
		$this->template->groupId = $type_id;
	}

	public function renderCreate($type_id) {

		// breadcrumb
		$this->template->title = 'Nová aktualita';
		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'správa článků', $this->link('Novinky:list'));
		$subsection = $section->add( 'správa článků', $this->link('Novinky:list'));
		$subsubsection = $subsection->add( 'správa aktualit', $this->link('Newsreel:list'));
		$subsubsubsection = $subsubsection->add( $this->template->title, '');
		$section->setCurrent($subsubsubsection);

		$this->setView('edit');
	}

	public function renderDelete() {

	}

	public function renderEdit($id_newsreel) {

		// breadcrumb
		$this->template->title = 'Editace aktuality';
		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'správa článků', $this->link('Novinky:list'));
		$subsection = $section->add( 'správa článků', $this->link('Novinky:list'));
		$subsubsection = $subsection->add( 'správa aktualit', $this->link('Newsreel:list'));
		$subsubsubsection = $subsubsection->add( $this->template->title, '');
		$section->setCurrent($subsubsubsection);


		$d = Newsreel::get()->find($id_newsreel)->fetch();
		$f = $this->getComponent('newsreelForm');
		
		$d['insert_date'] = $d['insert_date']->format("j.n.Y");
		$d['type_id'] = $d['group_id'];
		
		$f->setDefaults( $d );
	}

	function stripTags($str)
	{
		return strip_tags($str);
	}

	function uces($value, $data) {
		// jestli je ve výsledku dotazu ve sloupci `aktivni` hodnota 1 (true), připiš to ke jménu:
		$r  = strip_tags($data['short_text']);
		$r = Strings::truncate($r, 100);
		return $r;
	}

	protected function createComponentNewsreelList()
    {
		$model = Newsreel::get()->getDataSource();
		$groupId = $this->getParameter('type_id');
		$model = $model->where('group_id = %i', $groupId );


		$model = new \DataGrid\DataSources\Dibi\DataSource($model);
		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 10;
		$grid->multiOrder = FALSE;

		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, Newsreel::get()->getPrimary()); // allows checkboxes to do operations with more rows

 
		$grid->addColumn('insert_date', 'Datum aktuality')->addDefaultSorting('desc');

		$grid->addColumn('header', 'Nadpis');

		
		$grid->addColumn('short_text', 'Text');
		$grid['insert_date']->formatCallback[] = 'FormatingHelpers::bfl_date_short';
		

		$grid->addActionColumn('akce')->getHeaderPrototype()->style('width: 120px');
		$icon = Html::el('span');


		$grid->addAction('Editace', 'edit', clone $icon->class('icon icon-edit'));
		$grid->addAction('Smazat', 'confirmForm:confirmDeleteNewsreel!', clone $icon->class('icon icon-del'));

		return $grid;
    }


	function confirmedDeleteNewsreel($id_newsreel)
	{

		try{
			Newsreel::get()->delete( $id_newsreel );
			$this->flashMessage('Aktualita byla odstraněna.', 'SUCCESS');
		} catch (Exception $e) {
			dibi::rollback();
			$this->flashMessage('Došlo k chybě, aktualita se nepodařila odstranit', 'ERROR');
		}

		if (!$this->isAjax())
			$this->redirect('this');
		else 
			$this->invalidateControl();

	}
	
	/** @var \ConfirmationDialog @inject */
	public $confirmationDialog;
	
	public function createComponentConfirmForm()
	{
		$form = $this->confirmationDialog;
		$form->getFormElementPrototype()->addClass('ajax');
		$form->cssClass = 'static_dialog';

		$form->addConfirmer(
			'deleteNewsreel',
			array($this, 'confirmedDeleteNewsreel'),
			sprintf('Opravdu chcete smazat vybranou aktualitu ?')
			);

		return $form;
	}

	protected function createComponentNewsreelForm()
	{
		$menu = array();


		$form = new Form($this, 'newsreelForm');
		$form->addHidden( Newsreel::get()->primary );

		$form->addText('insert_date', 'Datum aktuality');
		$form->addText('site_url', 'Odkaz');
		$form->addHidden('type_id');
		$form->addText('header', 'Nadpis');



		$form->addUpload('photo', 'Miniatura:');

		$actionName = $this->getAction();
		$httpRequest = Environment::getHttpRequest();
		$baseUri = $httpRequest->getUrl()->basePath;
		$imgUrl = 'http://'.$_SERVER['SERVER_NAME'].$baseUri;
		if( $actionName != 'create')
		{
			$id = $this->getParam('id_newsreel');
			$item = Newsreel::get()->find( $id )->fetch();



			$el = Html::el();
			if( $item->photo == '' )
				$br = $el->create('img')->src($imgUrl.'images/newsreel/small/no-photo.png' );
			else
				$br = $el->create('img')->src($imgUrl.'images/newsreel/small/'.$item->photo );

			$br->class("form_photo");
			$form['photo']->setOption('description', $el );

		}

		// ck editor
		$editor = $form->addTextArea('short_text', 'Text aktuality')->getControlPrototype();
		//$editor->class = 'ckeditor';

		$form->addSubmit('save', 'Uložit');

		$form->onSuccess[] = array($this, 'newsreelSubmitted');
//		$form->addProtection('Please submit this form again (security token has expired).');

		return $form;
	}

	public function newsreelSubmitted(Form $form)
    {
        /* obsluha tlacitka uložit */
        if ($form['save']->isSubmittedBy()) {
			$data = (array)$form->getValues();
			if (empty($data['type_id'])) {
				
				$data['type_id'] = $this->getParameter('type_id');	
			}
			
			$photo = $data['photo'];
			unset( $data['photo'] );

			if( $photo->isOk() )
			{
				$photoName = Strings::toAscii($photo->getName());
				$fileSmall = WWW_DIR."/images/newsreel/small/".$photoName;
				$fileOrig = WWW_DIR."/images/newsreel/orig/".$photoName;

				@unlink( $fileOrig );
				$successMove = move_uploaded_file(
					$photo->getTemporaryFile(),
					$fileOrig
					);

				if( !$successMove )
					$this->flashMessage ('Obrázek se nepodařilo uložit na serveru', \AntoninRykalsky\Flashes::$info);

				$image = \Nette\Image::fromFile( $fileOrig );
				$image->resize(80, 80,\Nette\Image::EXACT);
				$image->save( $fileSmall );
				$data['photo'] = $photoName;

				$image = \Nette\Image::fromFile( $fileOrig );
				$image->resize(360, 1000);
				$image->save( $fileOrig );
				$data['photo'] = $photoName;
			}
			if (empty($photoName)) {
				$photoName = 0;
				
			}

			$this->newsFacade->setNews($data, $photoName);

			$this->redirect( 'list', array($data['type_id']) );
		}
	}


}
