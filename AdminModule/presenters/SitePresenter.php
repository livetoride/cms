<?php
/**
 * Administrace novinek a článků
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

namespace AdminModule\CmsModule;
use AntoninRykalsky\Entity\CmsSite;
use AntoninRykalsky\SiteFacade;
use Nette\Utils\Strings;
use Nette\Application\UI\Form;
use AntoninRykalsky;
use SoftwareStudio\Cms\Controls\ConfirmationDialogFactory;

class SitePresenter extends \Base_CmsPresenter
{
	/** @var \AntoninRykalsky\EntityManager */
	protected $em;

	public function __construct(
		\AntoninRykalsky\EntityManager $em,
		SiteFacade $siteFacade
	){
		$this->em = $em->getEm();
	}

	public function renderDefault() {

		$this->template->title = "Stránky na webu";
	}

	public function renderCreate() {

		$siteEntity = $this->siteFacade->createTemp();

		$this->redirect('detail', $siteEntity->getId());
	}

	public function actionDetail($id) 	{

		if(!is_numeric( $id ))
		{
			$site = \DAO\CmsSite::get()->findAll()->where('url=%s', $id )->fetch();
			if( !empty( $site->id ))
				$id = $site->id;
		}
		if( empty( $id ))
		{
			$site = \DAO\CmsSite::get()->findAll()->where('homepage=1')->fetch();
			if( !empty( $site->id ))
				$id = $site->id;
		}

		$site = AntoninRykalsky\Cms\Site::getBySite($id);

		$siteEntity = $this->em->find(CmsSite::class, $id );

		$idu = \AntoninRykalsky\SystemUser::get()->idu;
		$this['cmsGrids']->set( $siteEntity, $site );
		$this['cmsRepo']->setUser( $idu );


		$this->template->title = 'stránka ' . Strings::lower( $site->site->site );

		$d = \DAO\CmsSite::get()->find( $id )->fetch();
		$d['renderh1']=!$d['dontrenderh1'];
			$links = array(':Front:Site:default', '' );
			$menuZarazeni = \DAO\Menu::get()->findAll()->where('(link IN %in OR link IS NULL ) AND param=%s',$links , $siteEntity->getUrl() )->fetchAll();
			foreach( $menuZarazeni as $m )
			{
				$d['lastRenderedAutomatically'] = $m['last_rendered_automatically'];
				$d['registredOnly'] = $m['id_resource'] !== 'news-frontend';
				$d['fingerboard'] = empty( $m['link'] );
			}

		$this['siteForm']->setDefaults( (array)$d );
		$this['siteControl']->setSite( $siteEntity );

		$this->template->siteId = $site->site->id;
		$this->template->wrappers = $site->wrapper;
		$this->template->grids = $site->grid;
		$this->template->in = $site->getContent();

		$this->template->site = $site;
	}

	public function renderShow( $id )
	{
		$s = \DAO\CmsSite::get()->find( $id )->fetch();
		$this->redirect(':Front:Site:default', $s->url );
	}

	public function handleClone($id) {

		$newSite = $this->siteFacade->cloneSite($id);

		if ($newSite instanceof AntoninRykalsky\Entity\CmsSite) {
			$this->flashMessage('Stránka byla úspěšně naklonována', \Flashes::$success);
			$this->redirect('detail', $newSite->getId());
		} else {
			$this->flashMessage('Stránku se nepodařilo naklonovat', \Flashes::$error);

			return;
		}
	}

	public function handleAddStuff( $what ) {
		$what = $this->getParameter('what');
		$siteId = $this->getParameter('id');
		$this->siteFacade->addStuffToSite($what, $siteId );
		$this->invalidateControl();
	}

	public function createComponentSiteControl()
	{
		return new \SoftwareStudio\SiteControl();
	}

	/**
	 * Page deletion
	 * @param $siteId
	 */
	public function handleRemove( $siteId ) {

		$siteId = $this->getParameter('id'); // safer
		$this->siteFacade->deleteSite( $siteId );
		$this->flashMessage("Stránka odstraněna", AntoninRykalsky\Flashes::$info );
		$this->redirect(':Admin:Cms:Pages:list');
	}

	public function handleDeleteFromMenu( $idmenu )
	{
		$siteId = $this->getParameter('id');
		$this->siteFacade->deleteFromMenu( $idmenu, $siteId );
		$this->invalidateControl();
		return;
	}
	public function handleAddToMenu( $id, $parentid )
	{
		$this->siteFacade->addToMenu($id, $parentid);
		$this->invalidateControl();
	}

	protected function createComponentList()     {
		$ds = \DAO\CmsSite::get()->getDataSource();
		$model = new \DataGrid\DataSources\Dibi\DataSource($ds);

		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;

		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, \DAO\CmsSite::get()->getPrimary());


		$grid->addColumn('site', 'site');
		$grid['site']->addFilter();
		$grid->addColumn('title', 'title');
		$grid['title']->addFilter();
		$grid->addColumn('url', 'url');
		$grid['url']->addFilter();
		$grid->addColumn('homepage', 'homepage');
		$grid['homepage']->addFilter();

		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = \Nette\Utils\Html::el('span');
		$grid->addAction('show', 'show', clone $icon->class('icon icon-detail'));
		$grid->addAction('edit', 'detail',  clone $icon->class('icon icon-edit'));
		// $grid->addAction('delete', 'confirmForm:confirmDelete!',  clone $icon->class('icon icon-del'), true );
		return $grid;
	}



	protected function createComponentSiteForm() {
		$form = new Form();
//		$renderer = $form->getRenderer();

		$form->addHidden('id');
		$form->addText('site', 'stránka');
		$form['site']->addRule(Form::FILLED, 'Položka nadpis v menu je povinná.');

		$form->addText('title', 'html title');
		$form->addTextarea('description', 'html description');
		$form->addTextarea('keywords', 'html keywords');
		$form->addCheckbox('homepage', 'domovská stránka');
		$form->addCheckbox('renderh1', 'zobrazit nadpis stránky');

		$form->addCheckbox('fingerboard', 'Pouze rozcestník');
		$form->addCheckbox('lastRenderedAutomatically', 'Nevykresluj podsekce v menu');
		$form->addCheckbox('registredOnly', 'Pouze pro registrované');

		$form->addSubmit('save', 'Uložit');
		$form->addSubmit('saveNInclude', 'Uložit a přejít k zařazení');
		$form->onSuccess[] = array($this, 'submitedSiteForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}


	public function getList( $id, $prefix = '', $prefix2 = '' )
	{
		$p = AntoninRykalsky\Menu::get()->getChildNodes( $id );
		$prefix2.='&nbsp;&nbsp;';
		foreach( $p as $idMenu)
		{
			$act = $this->pages[ $idMenu ]['menu'];
			#echo $act . ' (' . $prefix." )<br />";
			$this->pages2[] = array(
					$this->pages[ $idMenu ]['id_menu'],
					$prefix2.  $act # . ' (' . $prefix." )"
					);
			$prefixNew = $prefix.' '.$this->pages[ $idMenu ]['menu'];

			$rnext = $this->getList( $idMenu, $prefixNew, $prefix2 );
		}
	}

	private $pages;
	private $pages2=array();
	public function actionJson( $q='', $idu=null )
	{
		$this->pages  = AntoninRykalsky\Menu::get()->getMenuItems();
		$this->getList( 0 );


		foreach( $this->pages2 as $v )
		{
			$this->payload->results[] =
					array(
				'id'=> $v[0],
				'text'=> $v[1],
				);
		}
		$this->payload->more = false;

		$this->sendPayload();
		exit;
	}


	function submitedSiteForm(Form $form) {

		if ((isset($form['save']) && $form['save']->isSubmittedBy())
				||
			(isset($form['saveNInclude']) && $form['saveNInclude']->isSubmittedBy())
			) {
			$v = (array) $form->getValues();


			$v['dontrenderh1']=!$v['renderh1'];
			$v['url']= Strings::webalize( $v['site'] );

			if( $v['homepage'] )
			{
				\DAO\CmsSite::get()->updateWhere(array('homepage'=>1), array('homepage'=>0));
			}



			if( !empty( $v['id'] ))
			{
				$old = \DAO\CmsSite::get()->find( $v['id'] )->fetch();



				\DAO\CmsSite::get()->update( $v['id'], $v );

				$links = array(':Front:Site:default', '' );
				$menuZarazeni = \DAO\Menu::get()->findAll()->where('(link IN %in OR link IS NULL ) AND param=%s',$links , $old->url )->fetchAll();
				foreach( $menuZarazeni as $m )
				{
					$params = array('menu'=>$v['site'], 'param'=>$v['url']);

					if( $v['fingerboard'] ) {
						$params['link'] = null;
					} else {
						$params['link'] = ':Front:Site:default';
					}
					if( $v['lastRenderedAutomatically'] ) {
						$params['last_rendered_automatically'] = 1;
					} else {
						$params['last_rendered_automatically'] = 0;
					}
					if( $v['registredOnly'] ) {
						$params['id_resource'] = 'news-member';
					} else {
						$params['id_resource'] = 'news-frontend';
					}

					\DAO\Menu::get()->update($m->id_menu, $params );
				}

			}
			else {
				\DAO\CmsSite::get()->insert( $v );

			}
		}
		if (isset($form['saveNInclude']) && $form['saveNInclude']->isSubmittedBy())
		{
			$id = $this->getParameter('id');
			$site = AntoninRykalsky\Cms\Site::getBySite($id);
			$site->getContent();

			$this->redirect(':Admin:Cms:Pages:list');

		} else
			$this->redirect('this', array('id' => $v['url']));
	}

	private function unknownFunction() {

		if (0) {
			$json = (object)['name' => 'Tony', 'address' => (object)['street' => 'tr.Tereskovove', 'no' => '2258', 'cs' => '19']];

			$s = new \AntoninRykalsky\Entity\CmsControlSetting();
			$s->setSetting($json);

			$this->em->persist($s);
			$this->em->flush();

			print_r($s);
			exit;
		} else {

			$s = $this->em->find('AntoninRykalsky\Entity\CmsControlSetting', 3);
			$json = $s->getSetting();
			print_r($json);
		}
		exit;
	}

	/** @deprecated */
	public function handleChangeType($what) {

		//print_r($what);exit;
		$id = $this->getParameter('id');
		if (!is_numeric($id)) {
			$site = \DAO\CmsSite::get()->findAll()->where('url=%s', $id)->fetch();
			if (!empty($site->id))
				$id = $site->id;
		}
		if (empty($id)) {
			$site = \DAO\CmsSite::get()->findAll()->where('homepage=1')->fetch();
			if (!empty($site->id))
				$id = $site->id;
		}

		$article = $this->em->getRepository('\AntoninRykalsky\Entity\CmsSite')->find($id);

		$idInMenu = $article->getSite();
		$menu = $this->em->getRepository('\AntoninRykalsky\Entity\Menu')->findOneByMenu($idInMenu);
		//\Doctrine\Common\Util\Debug::dump($menu);exit;

		switch ($what) {
			case 'page':
				$menu->setLink(':Front:Site:default');
				break;

			case 'finger':
				$menu->setLink('');
				break;

			case 'category':
				$menu->setLink(':Front:Site:default');
				break;

			case 'other':
				$menu->setLink('');
				break;

			default:
				break;
		}

		$this->em->persist($menu);
		$this->em->flush();
	}

}
