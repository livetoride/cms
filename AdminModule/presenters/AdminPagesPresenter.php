<?php
/**
 * Administrace novinek a článků
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

namespace AdminModule\CmsModule;
use ConfirmationDialog;
use Nette\Application\UI\Form;
use Nette;
use AntoninRykalsky as AR;
use dibi;
use Nette\Environment;


class AdminPagesPresenter extends PagesPresenter
{
	public function renderList()
	{
		$this['treeControl']->adminMode( 1 );
		parent::renderList();
	}


}
