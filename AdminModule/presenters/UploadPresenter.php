<?php

/**
 * Description of NewsreelPresenter
 *
 * @author Honzik
 */
namespace AdminModule\CmsModule;
use Nette\Application\UI\Form;
use Nette\Utils\Finder;
use Nette\Utils\Strings;
use Echo511\Plupload;


class UploadPresenter extends \Base_CmsPresenter {

	public function createComponentUpload()
	{
		// Main object
		$uploader = new Plupload\Rooftop();

		// Use magic for loading Js and Css?
		// $uploader->disableMagic();

		// Configuring paths
		$uploader->setWwwDir(WWW_DIR) // Full path to your frontend directory
				 ->setBasePath($this->template->basePath) // BasePath provided by Nette
				 ->setTempLibsDir(WWW_DIR . '/webtemp/plupload'); // Full path to the location of plupload libs (js, css)

		// Configuring plupload
		$uploader->createSettings()
				 ->setRuntimes(array('html5')) // Available: gears, flash, silverlight, browserplus, html5
				 ->setMaxFileSize('1000mb')
				 ->setMaxChunkSize('1mb'); // What is chunk you can find here: http://www.plupload.com/documentation.php

		if( !file_exists(ROOT_DIR . '/temp/upload')) mkdir (ROOT_DIR . '/temp/upload' );
		if( !file_exists(WWW_DIR . '/webtemp/plupload')) mkdir (WWW_DIR . '/webtemp/plupload' );
		chmod(ROOT_DIR . '/temp/upload', 0777);
		chmod(WWW_DIR . '/webtemp/plupload', 0777);

		// Configuring uploader
		$uploader->createUploader()
				 ->setTempUploadsDir(ROOT_DIR . '/temp/upload') // Where should be placed temporaly files
				 ->setToken("ahoj") // Resolves file names collisions in temp directory
				 ->setOnSuccess(array($this, 'tests')); // Callback when upload is successful: returns Nette\Http\FileUpload

		return $uploader->getComponent();
	}

	public function tests(\Nette\Http\FileUpload $file)
	{
		$uniqueFilename = Strings::toAscii( time().'-'.$file->getName() );
		
		$newFilePath = WWW_DIR."/files/".$uniqueFilename;
		@unlink( $newFilePath );

		// V produkčním módu nepřesunujeme soubory...
		if(1 || !Environment::isProduction()) {
			$file->move($newFilePath);
			$this->flashMessage("Soubor ".$file->getName() . " byl úspěšně přesunut!", \Flashes::$success );
		}

		if( $this->isAjax() )
			$this->invalidateControl('galleryList');
		else
			$this->redirect('this');
	}


	/*----------------------------*/

	public function renderDo() {
		// breadcrumb
		$this->template->title = 'Správa aktualit';
	}

	/** @var \ConfirmationDialog @inject */
	public $confirmationDialog;
	
	public function createComponentConfirmForm()
	{
		$form = $this->confirmationDialog;

		// you can easily create AJAX confirm form with eg. jquery.ajaxforms.js
		$form->getFormElementPrototype()->addClass('ajax');
		$form->cssClass = 'static_dialog';

		// create dynamic signal for 'confirmDelete!'
		$form->addConfirmer(
			'delete',
			array($this, 'confirmedDelete'),
			sprintf('Opravdu chcete smazat soubor ?')
			);

		return $form;
	}

	function confirmedDelete($file)
	{
		$prefix = WWW_DIR.'/';
		unlink( $prefix.$file );
		$this->flashMessage('Soubor odstraněn', 'SUCCESS');

		if (!$this->isAjax())
			$this->redirect('this');
		else 
			$this->invalidateControl();

	}
//
//	protected function createComponentUpload()
//    {
//		$f = new Form($this,'upload');
//
//		$f->addMultipleFileUpload("file","Soubory k nahrání",20)
//			->addRule("MultipleFileUpload::validateFilled","Musíte odeslat alespoň jeden soubor!")
//			;#->addRule("MultipleFileUpload::validateFileSize","Soubory jsou dohromady moc veliké!",1024*100); // 1 KB = 1024
//
//		$f->addSubmit('save', 'Uložit')->getControlPrototype()->class('default');
//
//        $f->onSubmit[] = array($this, 'submitedUpload');
//
//        $f->addProtection('Please submit this form again (security token has expired).');
//
//		return $f;
//	}

	function submitedUpload( Form $form )
    {
            $data = $form->getValues();

			// Předáme data do šablony
			$this->template->values = $data;

			$queueId = uniqid();

			// Přesumene uploadované soubory
			foreach($data["file"] AS $file) {

				// $file je instance HttpUploadedFile
				$newFilePath = WWW_DIR."/files/".$file->getName();

				// V produkčním módu nepřesunujeme soubory...
				if(1 || !Environment::isProduction()) {
					if($file->move($newFilePath))
						$this->flashMessage("Soubor ".$file->getName() . " byl úspěšně přesunut!");
					else
						$this->flashMessage("Při přesouvání souboru ".$file->getName() . " nastala chyba! Pro více informací se podívejte do logů.");
				}
			}
			$this->redirect('default');
	}


	public function renderUpload()
	{
		$this->template->title = 'Nahrávání souborů';
	}

	public function renderDefault()
	{
		$this->template->title = 'Nahrané soubory';

		$this->template->path = $dir = "files/";
		
		$files = [];
		foreach (Finder::findFiles('*')->from($dir) as $orig ) {
			$files[] = $orig;
		}


		$path = WWW_DIR.'/'.$this->template->path;
		
		$this->template->files = $files;
	}
}
