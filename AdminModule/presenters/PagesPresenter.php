<?php
/**
 * Administrace novinek a článků
 *
 * @author      Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) Antonín Rykalský
 * @link       http://softwarstudio.cz
 * @package    SoftwareStudio\Cms
 */

namespace AdminModule\CmsModule;

use AntoninRykalsky as AR;
use AntoninRykalsky\ICategoryControl;
use Nette;
use SoftwareStudio\Cms\Controls\ConfirmationDialogFactory;
use SoftwareStudio\Common\StringUtils;


class PagesPresenter extends \Base_CmsPresenter
{

	/** @var \AntoninRykalsky\MenuFacade */
	protected $menuFacade;

	/** @var ConfirmationDialogFactory */
	public $confirmationDialogFactory;
	public $actionName;
	private $categoryControl;

	const NEWS_PRESENTER = ':News:default';

	/**
	 * Block all request whitch do not have allowed show for this resource
	 */
	public function startup() {

		$this->actionName = $actionName = $this->getAction();
		parent::startup();
	}

	public function actionList() {

		$this->template->title = 'Správa článků';
		$this->template->maxLevels = 4;

		// for quick links
		$this->template->mainSection = $this->menuFacade->getMainSections();
		$this->template->menuitem = AR\Menu::get()->getMenuItems();
	}

	/**
	 * Reorder menu items by input array.
	 *
	 * @throws Nette\Application\AbortException
	 */
	public function handleMenuorder() {

		$data = $this->getParameter('data');
		$this->siteFacade->handleMenuOrder($data);
		$this->terminate();
	}

	protected function createComponentTreeControl() {

		return $this->categoryControl->create();
	}

	public function createComponentConfirmForm() {

		$form = $this->confirmationDialogFactory->create();
		$form->addConfirmer('delete', [$this, 'confirmedDelete'], [$this, 'questionDelete'], sprintf(' ?'));
		$form->addConfirmer('deleteMore', [$this, 'deleteMore'], [$this, 'deleteMoreQuestion']);
		return $form;
	}

	/**
	 * Question delete one page.
	 *
	 * @param $dialog
	 * @param $params
	 * @return Nette\Utils\Html
	 * @throws \Exception
	 */
	function questionDelete($dialog, $params) {

		$menu = \DAO\Menu::get()->find( $params['id'] )->fetch();
		$el = Nette\Utils\Html::el('div');
		$el->create('span', 'Opravdu chcete smazat článek ');
		$el->create('strong', '['.$menu->id_menu.'] '.$menu->menu );
		$el->create('span', ' ?' );
		return $el;
	}

	function deleteMore( array $pages, $operation ) {

		/** @var AR\CategoryControl $c */
		$c = $this['treeControl'];
		$c->handleOperationHandler($pages, $operation);
	}

	/**
	 * Format question for delete more pages by menu id.
	 *
	 * @param $form
	 * @param $parameters
	 * @return mixed
	 * @throws \Exception
	 */
	function deleteMoreQuestion( $form, $parameters) {

		$pages = $parameters['pages'];
		$operation = $parameters['operation'];
		return StringUtils::message("Opravdu chcete smazat {} položek?", count($pages));
	}


	/**
	 * Remove site by menu id.
	 * @param $id
	 */
	public function confirmedDelete($id) {

		$this->siteFacade->deleteSites([$id]);
		$this->flashMessage('Článek byl vymazán.', \Flashes::$success );
		$this->redrawControl();
	}

	public function injectCategoryControl(ICategoryControl $categoryControl) { $this->categoryControl = $categoryControl; }

	public function injectPartner(\AntoninRykalsky\MenuFacade $menuFacade) { $this->menuFacade = $menuFacade; }

	public function injectConfirmationDialogFactory(ConfirmationDialogFactory $confirmationDialogFactory) { $this->confirmationDialogFactory = $confirmationDialogFactory; }


}
