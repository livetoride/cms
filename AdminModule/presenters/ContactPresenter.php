<?php

namespace AdminModule\CmsModule;
use Nette\Application\UI\Form;

class ContactPresenter extends \BasePresenter
{
	public function actionEdit( $id ) {
		$this->template->title = "Nastavení kontaktního formuláře";
	}
	
	protected function createComponentContactFormSetting() {
		$form = new Form();
//		$renderer = $form->getRenderer();

		$form->addText('id_status', 'email pro doručení');

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedContactFormSetting');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}


	function submitedContactFormSetting(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = (array) $form->getValues();
			print_r($v);
			exit;
		}
	}
}
