<?php
/**
 * Controller referencí pro Ingenia.cz
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2011 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule;
use AntoninRykalsky as AR;
use AntoninRykalsky\Menu;
use Nette\Application\UI\Form;
use Nette\Utils\Strings as String;
use dibi;
use ConfirmationDialog;

use Nette\Utils\Html;
use Echo511\Plupload;
use Nette\Image;


class GalleryPresenter extends \BasePresenter
{
	public function actionList( $id_menu ) {


		// breadcrumb
		$menu = Menu::get()->find( $id_menu )->fetch();
		$this->template->id_menu = $id_menu;
		if(!empty($menu))
		{
			$this->template->title = 'Galerie :: '.$menu->menu;
			$this->template->menu = $menu;
			$this->breadDefault = 0;
			$section = $this->getComponent('navigation');
			$subsection = $section->add( 'správa článků', $this->link(':Admin:Cms:Pages:list'));
			$subsubsection = $subsection->add( $menu->menu, '');
			$section->setCurrent($subsubsection);
		}

		if( $this->isAjax() )
			$this->setView ('list-ajax');

	}

	public function actionTypes() {
		$this->template->title = 'Typy galerii';
	}

	public function actionCreate( $id_menu ) {


		// breadcrumb
		$this->template->title = 'Nová galerie';
		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'správa článků', $this->link(':Admin:Cms:Pages:list'));
		$menu = Menu::get()->find( $id_menu )->fetch();
		$subsubsection = $subsection->add(
				$menu->menu,
				$this->link('Gallery:list', array('id_menu' => $id_menu) )
				);
		$subsubsubsection = $subsubsection->add( 'Nová galerie', '');
		$section->setCurrent($subsubsubsection);

		$f = $this->getComponent('galleryForm');
		$d['id_menu'] = $id_menu;
		$f->setDefaults($d);

	}

	public function actionEdit( $id_gallery ) {
		$r=AR\Gallery::get()->find($id_gallery)->fetch();
		$menu = Menu::get()->find( $r->id_menu )->fetch();

		// breadcrumb
		$this->template->title = 'Editace galerie';
		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'správa článků', $this->link(':Admin:Cms:Pages:list'));

		$subsubsection = $subsection->add(
				$menu->menu,
				$this->link('Gallery:list', array('id_menu' => $r->id_menu) )
				);
		$subsubsubsection = $subsubsection->add( $this->template->title, '');
		$section->setCurrent($subsubsubsection);

		$f = $this->getComponent('galleryForm');

		$d['name'] = $r->name;
		$d['id_menu'] = $r->id_menu;
		$d['id_gallery'] = $r->id_gallery;
		$f->setDefaults($d);

	}


	public function actionSort( array $data ) {
		$k=0;
		foreach( $data as &$v)
		{
			$k++;
			$v =  preg_replace('/im_(\d+)/', '${1}', $v);
			AR\GalleryImages::get()->update( $v, array('image_order'=> $k ) );
		}
		$this->terminate();
	}

	public function actionPictures( $id_gallery ) {
		// breadcrumb

		$ref = AR\Gallery::get()->find($id_gallery)->fetch();
//		$menu = Menu::get()->find( $ref->id_menu )->fetch();

		$im = AR\GalleryImages::get()->findAll()->where('id_gallery=%i', $id_gallery )->orderBy('image_order')->fetchAll();

//		if(!empty( $menu->id_menu ))
		{

			if( !count( $im ))
				$this->flashMessage ('Nejsou nahrány žádné obrázky', AR\Flashes::$info );
			$this->template->title = 'Galerie :: ';
			$this->breadDefault = 0;
			$section = $this->getComponent('navigation');
			$subsection = $section->add( 'správa článků', $this->link(':Admin:Cms:Pages:list'));
//			$subsubsection = $subsection->add( $menu->menu, $this->link('Gallery:list', $menu->id_menu));
//			$subsubsubsection = $subsubsection->add( $ref->name, '');
//			$section->setCurrent($subsubsubsection);
		}

		foreach( $im as &$v )
		{
			if( empty( $v['short_text']) || $v['short_text']=='' )
				$v['short_text'] = 'Prázdný popisek';
		}

		$this->template->title = $ref['name'];
//		if(!empty( $menu->id_menu ))
//			$this->template->id_menu = $menu->id_menu;

		$this->template->images = $im;
		$this->template->reference = $ref;





	}



	public function actionGroupCreate() {
		$this->template->title = 'Nová skupina fotek';

		$m = Menu::get()->findAll()->where("link='News:reference'")->fetch();

		// vychozí hodnoty
		$d['show_to']=1;
		$d['menu_order']=(int)@$m->menu_order + 1;
		$form = $this->getComponent('groupForm');
		$form->setDefaults($d);
		$this->template->form = $form;
	}

	public function actionGroupEdit( $id_menu ) {
		$menu = Menu::get()->find( $id_menu )->fetch();
		$ref = AR\GalleryGroup::get()->findAll()->where('id_menu=%i', $menu->id_menu)->fetch();

		$this->template->title = 'Editace skupiny';

		// breadcrumb
		$this->template->title = 'Upravit článek';
		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'správa článků', $this->link(':Admin:Cms:Pages:list'));
		$subsubsection = $subsection->add( $menu->menu, '');
		$section->setCurrent($subsubsection);

		// vychozí hodnoty
		$form = $this->getComponent('groupForm');
		$this->template->form = $form;
		if( !$form->isSubmitted() )
		{


			// aktivni archivni
			$menu['show_to'] = $menu['active'];

			$form->setDefaults($menu);
			if( !empty( $ref ))
				$form->setDefaults($ref);
		}
	}

	protected function createComponentGalleryList()
    {
		$model = AR\Gallery::get()->getDataSource();
		$id_menu =  $this->getParam('id_menu');
		$model->where("id_menu=$id_menu");

		//datagrid
		$model = new \DataGrid\DataSources\Dibi\DataSource($model);
		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 10;
		$grid->multiOrder = FALSE;


		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, AR\Gallery::get()->getPrimary()); // allows checkboxes to do operations with more rows

		$grid->addColumn('name', 'Jméno');

		$grid->addActionColumn('akce')->getHeaderPrototype()->style('width: 120px');
		$icon = Html::el('span');

		$grid->addAction('Správa obrázků', 'pictures', clone $icon->class('icon icon-photo'));
		$grid->addAction('Editace', 'edit', clone $icon->class('icon icon-edit'));
		$grid->addAction('Smazat', 'confirmForm:confirmDeleteReference!', clone $icon->class('icon icon-del'));

		return $grid;
    }

	/** @var \ConfirmationDialog @inject */
	public $confirmationDialog;
	
	public function createComponentConfirmForm()
	{
		$form = $this->confirmationDialog;
		$form->getFormElementPrototype()->addClass('ajax');
		$form->cssClass = 'static_dialog';


		// create dynamic signal for 'confirmDelete!'
		$form->addConfirmer(
			'deleteReference',
			array($this, 'confirmedDeleteReference'),
			sprintf('Opravdu chcete smazat galerii i s obrázky ?')
			);

      $form->addConfirmer(
			'deletePhoto',
			array($this, 'confirmedDeletePhoto'),
			sprintf('Opravdu chcete obrázek ?')
			);

		return $form;
	}

	function confirmedDeleteReference($id_gallery)
	{

		try{
			dibi::begin();
			$r = AR\Gallery::get()->find($id_gallery)->fetch();
			AR\Gallery::get()->delete($id_gallery);
			AR\GalleryImages::get()->delete( $id_gallery );
			dibi::commit();
			$this->flashMessage('Galerie byla odstraněna.', 'SUCCESS');
		} catch (Exception $e) {
			dibi::rollback();
			$this->flashMessage('Došlo k chybě, galerii se nepodařilo odstranit', 'ERROR');
		}

		if (!$this->isAjax())
			$this->redirect('this');

	}

  function confirmedDeletePhoto( $id_image )
	{
    $id_gallery = $this->getParam('id_gallery');

		try{
			dibi::begin();
			$r = AR\Gallery::get()->find($id_gallery)->fetch();

			AR\GalleryImages::get()->deleteWhere( 'id_image='.(int)$id_image.' AND '.'id_gallery='.(int)$id_gallery );
			dibi::commit();
			$this->flashMessage('Obrázek byl odstraněn.', 'SUCCESS');
		} catch (Exception $e) {
			dibi::rollback();
			$this->flashMessage('Došlo k chybě, obrázek se nepodařilo odstranit', 'ERROR');
		}

		if (!$this->isAjax())
			$this->redirect('this');

	}


	protected function createComponentGroupList()
    {
		$model = Menu::get();
		$ds = $model->getDataSource();
		$ds->where("link='News:reference' AND parent_id <> 0");
		#$ds->orderBy(array("email_verified" => "DESC", "id_user" => "asc"));

		//datagrid
		$model = new \DataGrid\DataSources\Dibi\DataSource($model);
		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setDataSource( $model );
		$grid->setRenderer($renderer);
		$grid->itemsPerPage = 10;
		$grid->multiOrder = FALSE;


		$operations = array(); // define operations
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, $model->getPrimary()); // allows checkboxes to do operations with more rows

		/*
		// if no columns are defined, takes all cols from given data source
		$grid->addColumn('login', 'login');
		$grid->addColumn('login_sponzor', 'sponzor');
		$grid['login_sponzor']->replacement = array(''=>'-');
		*/
		$grid->addColumn('id_menu', 'Jméno');
		$grid->addColumn('menu', 'přijmení');

		$grid->addActionColumn('akce')->getHeaderPrototype()->style('width: 120px');
		$icon = Html::el('span');
		$grid->addAction('Aktivuj', 'activate');

		//
		// tohle vidí pouze admin
		//
		if( $this->getUser()->isInRole('admin') )
			$grid->addAction('Potvrď email', 'confirmEmail');

		$grid->addAction('Odešli znovu ověřovací email', 'sendMail', clone $icon->class('icon icon-mail'));
		$grid->addAction('Změn ', 'edit', clone $icon->class('icon icon-edit'));

		return $grid;
    }

	public function handleLoadData($value) {
        $form = $this->getComponent('referencesForm');

		$items[1] = array('Neratovice kaple', 'Stavebniny Janík lepené obloukové prvky');
			$items[2] = array('Dolní Životice úschovna kol', 'Neratovice Altán');

        $data = $items[$value];
        $form['item']->setItems($data);
		echo $form['item']->getControl();
        $this->terminate();
	}

	public function handleChangeDesc( $id_image, $desc )
	{
		if(!is_numeric( $id_image))
			return;
		AR\GalleryImages::get()->update( $id_image, array('short_text' => $desc ));

	}

	public function tests($file)
	{
		$paths=array();
		$paths['original'] = WWW_DIR."/images/gallery/original/";
		$paths['small'] = WWW_DIR."/images/gallery/small/";
		$paths['big'] = WWW_DIR."/images/gallery/big/";

		foreach( $paths as $path )
		{
			@mkdir( $path );
			@chmod( $path, 777 );
		}

		$uniqueFilename = time().'-'.String::toAscii($file->getName());
		$newFilePath = $paths['original'].$uniqueFilename;
		$fileSmall = $paths['small'].$uniqueFilename;
		$fileBig = $paths['big'].$uniqueFilename;

		@unlink( $newFilePath );

		$file->move($newFilePath);

		$image = Image::fromFile($newFilePath);
		$image->resize(270, 179);
		$roundedCorners = 0;
		if( $roundedCorners )
		{
			// levy horni roh
			$imageRoh = Image::fromFile(WWW_DIR."/images/gallery/roh.png");
			$image->place($imageRoh, 0, 0); // vložíme na pozici 0px, 0px
			// pravy horni roh
			$imageRoh = $imageRoh->resize('-100%', 0);
			$image->place($imageRoh, $image->width - $imageRoh->width, 0);
			// pravy dolni roh
			$imageRoh = $imageRoh->resize('-100%', '-100%');
			$image->place($imageRoh, 0, $image->height - $imageRoh->height);

			// levy dolni roh
			$imageRoh = $imageRoh->resize('-100%', 0);
			$image->place($imageRoh, $image->width - $imageRoh->width, $image->height - $imageRoh->height);
		}


		$image->save($fileSmall);

		$image2 = Image::fromFile($newFilePath);
		$image2->resize(800, 600);
		$ok = $image2->save($fileBig);


			// ulož obrazek do db
		$d=array();
		$d['id_gallery'] = $this->getParameter('id_gallery');
		$d['image_path'] = $uniqueFilename;
		$d['short_text'] = '';
		$d['image_order'] = 0;
		$last = AR\GalleryImages::get()->findAll()->where('id_gallery=%i', $d['id_gallery'])->orderBy('image_order')->desc()->fetch();
		if( isset( $last->image_order )) {
			$d['image_order'] = $last->image_order+1;
		}
		AR\GalleryImages::get()->insert( $d );

		if( $this->isAjax() )
			$this->invalidateControl();
	}

	public function handleInvalidate()
	{
		if( $this->isAjax() )
			$this->invalidateControl();
	}

	public function createComponentUpload()
	{
		// Main object
		$uploader = new Plupload\Rooftop($this, 'upload');

		// Use magic for loading Js and Css?
		// $uploader->disableMagic();

		// Configuring paths
		$uploader->setWwwDir(WWW_DIR) // Full path to your frontend directory
				 ->setBasePath($this->template->basePath) // BasePath provided by Nette
				 ->setTempLibsDir(WWW_DIR . '/webtemp/plupload'); // Full path to the location of plupload libs (js, css)

		// Configuring plupload
		$uploader->createSettings()
				 ->setRuntimes(array('html5')) // Available: gears, flash, silverlight, browserplus, html5
				 ->setMaxFileSize('1000mb')
				 ->setMaxChunkSize('32mb'); // What is chunk you can find here: http://www.plupload.com/documentation.php

		if( !file_exists(ROOT_DIR . '/temp/upload'))
			mkdir (ROOT_DIR . '/temp/upload');

		chmod(ROOT_DIR . '/temp/upload', 0777);
		chmod(WWW_DIR . '/webtemp/plupload', 0777);

		// Configuring uploader
		$uploader->createUploader()
				 ->setTempUploadsDir(ROOT_DIR . '/temp/upload') // Where should be placed temporaly files
				 ->setToken("ahoj") // Resolves file names collisions in temp directory
				 ->setOnSuccess(array($this, 'tests')); // Callback when upload is successful: returns Nette\Http\FileUpload

		return $uploader->getComponent();
	}

	protected function createComponentGalleryForm()
	{
			$menu = array();


			$form = new Form($this, 'galleryForm');
			$form->addHidden('id_gallery');

			$form->addText('name', 'Jméno galerie');

			$skupiny = AR\Gallery::get()->find( $this->getParam('id_gallery') )->fetch();
			$form->addHidden('id_menu', $skupiny->id_menu );


			$form->addSubmit('save', 'Uložit');

            $form->onSuccess[] = array($this, 'gallerySubmitted');
            $form->addProtection('Please submit this form again (security token has expired).');

            return $form;
	}




	/**
	 * Component factory.
	 * @param  string  component name
	 * @return void
	 */
	protected function createComponentGroupForm()
	{
			$menu = array();


			$form = new Form($this, 'groupForm');
			$form->addHidden('id_menu');

			$form->addText('menu', 'Jméno skupiny');

			$showTo = array(0=>'archivní', 'zařaď to menu');
			$form->addRadioList('show_to', 'Zobrazení', $showTo);

			$form->addText('menu_order', 'Váha v menu');

			$form->addText('html_description', 'html description');

			$form->addText('html_keywords', 'html keywords');

			$form->addText('html_title', 'html title');


			// ck editor
            $editor = $form->addTextArea('text_top', 'Text nahoře')->getControlPrototype();
            $editor->class = 'ckeditor';

			// ck editor
            $editor = $form->addTextArea('text_bottom', 'Text dole')->getControlPrototype();
            $editor->class = 'ckeditor';


			$form->addSubmit('save', 'Uložit');

            $form->onSuccess[] = array($this, 'groupSubmitted');
            $form->addProtection('Please submit this form again (security token has expired).');

            return $form;
	}







	public function uploadSubmitted(Form $form)
    {
        /* obsluha tlacitka uložit */
        if ($form['save']->isSubmittedBy()) {
			$data = (array)$form->getValues();

			// Předáme data do šablony
			$this->template->values = $data;

			$queueId = uniqid();

			// Přesumene uploadované soubory
			foreach($data["file"] AS $file) {

				// $file je instance HttpUploadedFile
				$newFilePath = WWW_DIR."/images/gallery/original/".$file->getName();
				$fileSmall = WWW_DIR."/images/gallery/small/".$file->getName();
				$fileBig = WWW_DIR."/images/gallery/big/".$file->getName();

				@unlink( $newFilePath );

				// V produkčním módu nepřesunujeme soubory...
				if(1 || !Environment::isProduction()) {
					if(@$file->move($newFilePath))
						$this->flashMessage("Soubor ".$file->getName() . " byl úspěšně přesunut!");
					else
						$this->flashMessage("Při přesouvání souboru ".$file->getName() . " nastala chyba! Pro více informací se podívejte do logů.");
				}

				$image = Image::fromFile($newFilePath);
				$image->resize(270, 179);
				// levy horni roh
				$imageRoh = Image::fromFile(WWW_DIR."/images/gallery/roh.png");
				$image->place($imageRoh, 0, 0); // vložíme na pozici 0px, 0px
				// pravy horni roh
				$imageRoh = $imageRoh->resize('-100%', 0);
				$image->place($imageRoh, $image->width - $imageRoh->width, 0);
				// pravy dolni roh
				$imageRoh = $imageRoh->resize('-100%', '-100%');
				$image->place($imageRoh, 0, $image->height - $imageRoh->height);

				// levy dolni roh
				$imageRoh = $imageRoh->resize('-100%', 0);
				$image->place($imageRoh, $image->width - $imageRoh->width, $image->height - $imageRoh->height);


				$image->save($fileSmall);

				$image = Image::fromFile($newFilePath);
				$image->resize(800, 600);
				$image->save($fileBig);


					// ulož obrazek do db
				$d=array();
				$d['id_gallery'] = $data['id_gallery'];
				$d['image_path'] = $file->getName();
				AR\GalleryImages::get()->insert( $d );
			}


		}
		$this->redirect('this');
	}



	public function gallerySubmitted(Form $form)
    {
        /* obsluha tlacitka uložit */
        if ($form['save']->isSubmittedBy()) {
			$data = (array)$form->getValues();

			if(!empty( $data['id_gallery']  ))
			{
				// <editor-fold defaultstate="collapsed" desc="update">
				try {
					AR\Gallery::get()->update($data['id_gallery'], $data );
					$this->flashMessage('Operace byla úspěšně provedena', 'INFO');
				} catch( DibiDriverException $e ) { $this->flashMessage( 'Došlo k chybě, článek se nepodařilo uložit.', 'ERROR' ); }
				// </editor-fold>
			} else {
				// <editor-fold defaultstate="collapsed" desc="insert">
				try {
					unset( $data['id_gallery'] );
					$data['id_menu'] = $this->getParam('id_menu');
					AR\Gallery::get()->insert( $data );
					$this->flashMessage('Operace byla úspěšně provedena', 'INFO');
				} catch( DibiDriverException $e ) { $this->flashMessage( 'Došlo k chybě, článek se nepodařilo uložit.', 'ERROR' ); }
				// </editor-fold>
			}

			$this->redirect('list', $data['id_menu'] );
		}
	}


	public function groupSubmitted(Form $form)
    {
        /* obsluha tlacitka uložit */
        if ($form['save']->isSubmittedBy()) {
			$data = (array)$form->getValues();





			if(!empty( $data['id_menu']  ))
			{
				// <editor-fold defaultstate="collapsed" desc="upravit článek">
				#try {
					dibi::begin();

					$data['param'] = String::webalize($data['menu']);
					$menu = Menu::get()->update( $data['id_menu'], $data );
					$ref = AR\GalleryGroup::get()->update( $data['id_menu'], $data );


					dibi::commit();

					// nastavení článku do menu
					$this->flashMessage('Operace byla úspěšně provedena', 'INFO');


				#} catch( DibiDriverException $e ) { $this->flashMessage( 'Došlo k chybě, článek se nepodařilo uložit.', 'ERROR' ); }
				// </editor-fold>
			}

			else {
				// <editor-fold defaultstate="collapsed" desc="nový článek">
					unset( $data['id_menu'] );
					$data['parent_id'] = 0;
					$data['id_resource'] = 'news-frontend';
					$data['active'] = 1;
					$data['link'] = ':Front:News:reference';


					$data['param'] = String::webalize($data['menu']);
					dibi::begin();
					$menu = Menu::get()->insert( $data );
					$data['id_menu'] = dibi::getInsertId();
//					print_r( $data );exit;
					AR\GalleryGroup::get()->insert( $data );
					dibi::commit();
			   // </editor-fold>
			}

			$this->redirect(':Admin:Cms:Pages:list');

		}
	}




}
