<?php
/**
 * Administrace novinek a �l�nk�
 *
 * @author Bc. Anton�n Rykalsk� <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Anton�n Rykalsk�
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

namespace AdminModule\CmsModule;
use \ConfirmationDialog,
		\Nette\Application\UI\Form,
		AntoninRykalsky as AR,
		dibi,
		Nette\Environment;


class PresenterPresenter extends \Base_CmsPresenter
{
	public function actionDefault()
	{
		\Panel\Navigation::register($this);
		
	}
}
