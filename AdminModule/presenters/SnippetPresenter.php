<?php
/**
 * Administrace novinek a článků
 *
 * @author Bc. Antonín Rykalský <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Antonín Rykalský
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */

namespace AdminModule\CmsModule;
use \ConfirmationDialog,
		\Nette\Application\UI\Form,
		AntoninRykalsky as AR,
		dibi,
		Nette\Environment;


class SnippetPresenter extends \Base_CmsPresenter
{
	public $actionName;

	/**
	 * Block all request whitch do not have allowed show for this resource
	 */
	public function startup()
	{
            $this->actionName = $actionName = $this->getAction();

            parent::startup();
            if(!$this->getUser()->isAllowed('novinky', 'show'))
                $this->redirect('Oops:forbidden');
	}

    const NEWS_PRESENTER = ':News:default';

	public function renderBestpractice()
	{
		$this->template->title = 'Tipy k tvorbě článků';
	}

	public function handleMenuorder()
	{
		$data = $this->getParameter('data');
		//$data = unserialize($data);
		foreach( $data as $v )
		if(is_numeric($v['item_id']))
		{
			$id_menu = $v['item_id'];
			$parent = (is_numeric($v['parent_id']) ? $v['parent_id'] :  0);
			$structure[$parent][] = $id_menu;

		}
//		print_r( $structure );exit;
		/*
		 Array
		(
			[0] => Array
				(
					[0] => 187
					[1] => 188
				)

			[187] => Array
				(
					[0] => 189
					[1] => 199
				)

		)
		 */

		dibi::begin();
		foreach( $structure as $parent_id => $v )
		foreach( $v as $order => $id_menu )
		{
			$data = array(
				'parent_id' => $parent_id,
				'menu_order' => $order
			);
			AR\Menu::get()->update($id_menu, $data);
		}
		dibi::commit();
		$this->terminate();
	}

	public function renderFiles()
	{

		$this->template->path = "files/";
		$path = WWW_DIR.'/'.$this->template->path;
		$handle=opendir($path);
		while (false!==($file = readdir($handle)))
		{
			if ($file != "." && $file != "..")
			{
				$f['filename'] = $file;
				$f['size'] = filesize($path.$file);
				$files[] = $f;
			}
		}
		#print_r( $files );
		closedir($handle);
		$this->template->files = $files;
	}


    public function renderCreate( $section = null, $news_id = null )
	{
		$this->template->form = $f = $this->getComponent('newsForm');

		// breadcrumb
		$this->template->pageId = "mlmadmin-novinky-edit";
		$this->template->title = "Vytvořit článek";
		$this->breadDefault = 0;
		$section = $this->getComponent('navigation');
		$subsection = $section->add( 'správa článků', $this->link('Novinky:list'));
		$subsubsection = $subsection->add( $this->template->title, '');
		$section->setCurrent($subsubsection);

		$defaults['datum'] = Date("j.m.Y", strtotime('now'));
		$defaults['show_to'] = 1;
		$defaults['menu_order'] = 1;
		$f->setDefaults($defaults);

		$this->setView('edit');

	}

	/** @var \ConfirmationDialog @inject */
	public $confirmationDialog;
	
	public function createComponentConfirmForm()
	{
		$form = $this->confirmationDialog;

		// you can easily create AJAX confirm form with eg. jquery.ajaxforms.js
		$form->getFormElementPrototype()->addClass('ajax');
		$form->cssClass = 'static_dialog';

		// create dynamic signal for 'confirmDelete!'
		$form->addConfirmer(
			'delete',
			array($this, 'confirmedDelete'),
			sprintf('Opravdu chcete smazat článek ?')
			);
		$form->addConfirmer(
			'deletePage',
			array($this, 'confirmedDeletePage'),
			sprintf('Opravdu chcete smazat článek ?')
			);

		$form->addConfirmer(
			'deleteRecursive',
			array($this, 'confirmedDeleteRecursive'),
			sprintf('Vymazány budou i všechny podčlánky. Opravdu chcete smazat článek ?')
			);

		return $form;
	}

	function confirmedDeletePage( $id_clanek )
	{
		try{
			dibi::begin();
			AR\Clanky::get()->delete($id_clanek);
			dibi::commit();

			// obnovíme cache pro menu
			$cache = Environment::getCache('menu_cache');
			$cache['menu_cache'] = null;

			$this->flashMessage('Článek byl vymazán.', 'SUCCESS');
		} catch (Exception $e) {
			dibi::rollback();
			$this->flashMessage('Článek se nepodařilo odstranit', 'ERROR');
		}

		if (!$this->isAjax())
			$this->redirect('this');
	}


	function confirmedDelete($id )
	{
		try{
			dibi::begin();
			$cl = AR\Menu::get()->find( $id )->fetch();
			AR\Menu::get()->deleteWhere( "param = '$cl->param' AND link = ':Front:News:show'" );
			AR\Menu::get()->delete( $id );
			#Kategorie::get()->deleteWhere('id_clanek = '.$id);
			AR\Clanky::get()->deleteWhere("path = '$cl->param'");

			dibi::commit();

			// obnovíme cache pro menu
			$cache = Environment::getCache('menu_cache');
			$cache['menu_cache'] = null;

			$this->flashMessage('Článek byl vymazán.', 'SUCCESS');
		} catch (Exception $e) {
			dibi::rollback();
			$this->flashMessage('Článek se nepodařilo odstranit', 'ERROR');
		}

		if (!$this->isAjax())
			$this->redirect('this');

	}


	public function renderList()
	{

	}

	protected function createComponentArticles()     {
		$ds = \DAO\CmsArticle::get()->getDataSource();
		$model = new \DataGrid\DataSources\Dibi\DataSource($ds);

		$grid = new \DataGrid\DataGrid;
		$renderer = new \DataGrid\Renderers\Conventional;
		$renderer->paginatorFormat = '%input%';
		$grid->setRenderer($renderer);

		$grid->itemsPerPage = 10;
		$grid->setDataSource($model);
		$grid->multiOrder = FALSE;

		$operations = array();
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, \DAO\CmsArticle::get()->getPrimary());

		$grid->addColumn('id_article', 'ID');
		$grid['id_article']->addFilter();

		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = \Nette\Utils\Html::el('span');
		$grid->addAction('edit', 'edit', clone $icon->class('icon icon-detail'));
		return $grid;
	}



	/**
	 * Jména typů v podkategoriích
	 */
	public $types = array(
		':Front:News:show' => 'Článek',
		'News:seznam' => 'Tiskové zprávy',
		'News:contact' => 'Kontaktní form',
	);

	public $mode;
	public function createComponentTree()
	{
		$tree = new TreeView();
		$tree->primaryKey = 'id_menu';
		$tree->parentColumn = 'parent_id';
		$tree->useAjax = true;
		$mode = null === $this->mode ? 1 : $this->mode;
		$session = Environment::getSession();
		$tree->mode = $mode;
		$tree->rememberState = true;
		$tree->addLink('edit', 'menu', 'id_menu', true, $this->presenter);
		$tree->dataSource = AR\Menu::get()->findAll("id_resource IN ('news-guest')");

		#print_r( AR\Menu::get()->findAll()->fetchAll() );exit;

		$tree->renderer->wrappers['link']['collapse'] = 'a class="ui-icon ui-icon-circlesmall-minus" style="float: left"';
		$tree->renderer->wrappers['link']['expand'] = 'a class="ui-icon ui-icon-circlesmall-plus" style="float: left"';
		$tree->renderer->wrappers['node']['icon'] = 'span class="ui-icon ui-icon-document" style="float: left"';

		return $tree;
	}


	public function handleSaveArticle($aid, $article)
	{
//
//		$article = preg_replace('<ul>#', '<ul class="list-style-type-disc"', $article );
//		$article = preg_replace('#style="list-style-type: disc;"#', 'class="list-style-type-disc"', $article );
//		$article = preg_replace('#style="list-style-type: disc;"#', 'class="list-style-type-disc"', $article );
//		echo $article;
//
//		exit;
		\DAO\CmsArticle::get()->update($aid, array('article'=>$article) );
		$this->flashMessage('Článek by editován', "SUCCESS");

		if ($this->isAjax()) {
	        $this->invalidateControl();
	    } else {
			$this->redirect('this');
		}
	}

	public function renderEdit( $id_article )
	{
		$g = \DAO\CmsGrid::get()->findAll()->where('type_id=%i AND type=%i', $id_article, 1 )->fetch();
		$this->template->grid = $g->width;

		$layout = \AntoninRykalsky\Configs::get()->byContent('settings.layout_name');
		$this->setLayout($layout);
		$def = \DAO\CmsArticle::get()->find( $id_article )->fetch();
		$this->template->article = $def;

		// změn [+baseUrl] na $baseUri
		$find = $this->template->baseUri;
		$def['article'] = str_replace("[+baseUri+]", $find, $def['article']);

		// nastavi default hodnoty
		$form = $this->getComponent('newsForm');
		$form->setDefaults((array)$def);

		// breadcrumb
		$this->template->title = 'Upravit článek';

		$this->template->form = $form;

//		$this->breadDefault = 0;
//		$section = $this->getComponent('navigation');
//		$subsection = $section->add( 'správa článků', $this->link('Novinky:list'));
//		$subsubsection = $subsection->add( $def->nadpis, '');
//		$section->setCurrent($subsubsection);
	}

	public function renderClassicEdit( $id_article )
	{
		$g = \DAO\CmsGrid::get()->findAll()->where('type_id=%i AND type=%i', $id_article, 1 )->fetch();
		$this->template->grid = $g->width;

		$def = \DAO\CmsArticle::get()->find( $id_article )->fetch();
		$this->template->article = $def;

		// změn [+baseUrl] na $baseUri
		$find = $this->template->baseUri;
		$def['article'] = str_replace("[+baseUri+]", $find, $def['article']);

		// nastavi default hodnoty
		$form = $this->getComponent('newsForm');
		$form->setDefaults((array)$def);

		// breadcrumb
		$this->template->title = 'Upravit článek';

		$this->template->form = $form;

//		$this->breadDefault = 0;
//		$section = $this->getComponent('navigation');
//		$subsection = $section->add( 'správa článků', $this->link('Novinky:list'));
//		$subsubsection = $subsection->add( $def->nadpis, '');
//		$section->setCurrent($subsubsection);
	}

	public function renderShow( $id_article )
	{
		\Nette\Diagnostics\Debugger::$bar = FALSE;


		$g = \DAO\CmsGrid::get()->findAll()->where('type_id=%i AND type=%i', $id_article, 1 )->fetch();
		$this->template->grid = $g->width;

		$def = \DAO\CmsArticle::get()->find( $id_article )->fetch();
		$this->template->article = $def;

		// změn [+baseUrl] na $baseUri
		$find = $this->template->baseUri;
		$def['article'] = str_replace("[+baseUri+]", $find, $def['article']);

		// nastavi default hodnoty
		$form = $this->getComponent('newsForm');
		$form->setDefaults((array)$def);

		// breadcrumb
		$this->template->title = 'Upravit článek';

		$this->template->form = $form;

//		$this->breadDefault = 0;
//		$section = $this->getComponent('navigation');
//		$subsection = $section->add( 'správa článků', $this->link('Novinky:list'));
//		$subsubsection = $subsection->add( $def->nadpis, '');
//		$section->setCurrent($subsubsection);
	}

    public function renderEdi2t($id_article )
	{
		$form = $this->getComponent('newsForm');
		$this->template->form = $form;

		// editujeme pouze článek
		if( $id_menu == null && $id_clanek != null )
		{
			$this->renderEditPage( $id_clanek );
			return;
		}

		if( !$form->isSubmitted() )
		{
			$menu = AR\Menu::get()->find( $id_menu )->fetch();
			if( !empty( $menu ))
			{
				$this->template->id_menu = $menu->id_menu;
				$row = AR\Clanky::get()->find( $menu->param )->fetch();


				// breadcrumb
				$this->template->title = 'Upravit článek';
				$this->breadDefault = 0;
				$section = $this->getComponent('navigation');
				$subsection = $section->add( 'správa článků', $this->link('Novinky:list'));
				$subsubsection = $subsection->add( $menu->menu, '');
				$section->setCurrent($subsubsection);
			}

			// převedeme na lidské datum
			@$row->datum = CmsHelpers::PgDate2CzDate( $row->datum );
			@$row->zobrazdo = CmsHelpers::PgDate2CzDate( $row->zobrazdo );

			// změn [+baseUrl] na $baseUri
			!isset( $row->clanek ) ? $row->clanek = '' : null;
			$find = $this->template->baseUri;
			$row->clanek = str_replace("[[replacement_map]]", '<img src="[+baseUri+]/images/css-admin/replacement/replacement_map_354.png" />', $row->clanek);
			$row->clanek = str_replace("[[replacement_contactform]]", '<img src="[+baseUri+]/images/css-admin/replacement/replacement_contactform.png" />', $row->clanek);
			$row->clanek = str_replace("[+baseUri+]", $find, $row->clanek);


            // upravi data pro potreby pgsql
            #$row->povol_komentare=='t' ? $row->povol_komentare=true : $row->povol_komentare=false;
			if( $menu->id_resource=='news-member' )
			{
				$row->active_klient=true;
				$row->active=false;
				$row->show_to=2;

			}
			else {
				$menu->active ? $row->show_to=1 : $row->show_to=0;
			}
			$row->menu_order=$menu->menu_order;
			$row->parent_id = $menu->parent_id;


			if (!$row) {
				throw new BadRequestException('Record not found');
			}

			if( $menu->link == '' )
				$row->show_to = 4;



            // nastavi default hodnoty
			$form->setDefaults((array)$row);
		}
	}

    public function renderDelete($id_clanek)
	{
        // vytvoří komponentu
        $c = new CRM();

        if(!is_array($id_clanek)){
            // jeden článek
            $id_clanek = str_replace('_', '-', $id_clanek);
            $pages = $c->news_find($id_clanek);
            $arrayGrid->bindDataTable( array($pages) );
        } else {
            throw new BadRequestException('You have to delete by one item');
        }

        // vloží komponentu s tabulkou
        $this->addComponent($arrayGrid, 'dg');
		$this->template->arrayGrid = $arrayGrid;

        // vloží komponentu s tlačítky
		$form = $this->getComponent('deleteForm');
		$this->template->form = $form;

	}


	/** @var \AntoninRykalsky\CmsReplacing */
	protected $cmsReplacing;

	function injectPartner2(\AntoninRykalsky\CmsReplacing $cmsReplacing) {
		$this->cmsReplacing = $cmsReplacing;
	}

	/** @var \AntoninRykalsky\SiteService */
	protected $siteService;

	function injectPartner(\AntoninRykalsky\SiteService $siteService) {
		$this->siteService = $siteService;
	}

    public function submittedNewsForm($form)
    {
        /* obsluha tlacitka uložit */
        if ($form['save']->isSubmittedBy()) {
            $v = (array)$form->getValues();
			$v['ts_update']='now';
			$id = $this->getParameter('id_article');

			$v['article']  = $this->cmsReplacing->replacingStore($v['article'] );
			\DAO\CmsArticle::get()->update( $id, $v );

			$this->redirect( 'this' );
        }

    }


    public function deleteFormSubmitted( AppForm $form) {
		// akce po zmacknuti delete
		if ($form['delete']->isSubmittedBy()) {
			$v = $form->getValues();
			$v['ids'] =  unserialize( $v['ids'] );

			try {
				$c = new CRM();
				$c->news_remove( $v['ids'] );
			} catch( DibiDriverException $e ) {
				$this->flashMessage( 'Vybrané články byly odstraněny.' );
			}

			// obnovíme cache pro menu
			$cache = Environment::getCache('menu_cache');
			$cache['menu_cache'] = null;

			$this->redirect( 'Novinky:default' );
		}
    }

	/**
     * Vytvori form pro získání ID uživatele
     * @return AppForm
     */
    protected function createComponentNewsList()
    {
		$actionName = $this->getAction();
		switch ($actionName) {
			case 'guest':
				$dataSource = AR\Clanky::get()->getDataSource_guest();
				break;
			case 'klient':
				$dataSource = AR\Clanky::get()->getDataSource_klient();
				break;

			default:
				$dataSource = AR\Clanky::get()->getDataSource();
		}


		$grid = new DataGrid;

		$renderer = new DataGridRenderer;
		$renderer->paginatorFormat = '%input%'; // customize format of paginator
		$grid->setRenderer($renderer);

		$grid->itemsPerPage = 10; // display 10 rows per page
		$grid->bindDataTable( $dataSource );
		$grid->multiOrder = FALSE; // order by one column only

		#$operations = array('delete' => 'smaž'); // define operations
		$operations = array(); // define operations
		// in czech for example: $operations = array('delete' => 'smazat', 'deal' => 'vyřídit', 'print' => 'tisk', 'forward' => 'předat');
		// or you can left translate values by translator adapter
		$callback = array($this, 'gridOperationHandler');
		$grid->allowOperations($operations, $callback, 'id_clanek'); // allows checkboxes to do operations with more rows


		// if no columns are defined, takes all cols from given data source
		$grid->addColumn('nadpis', 'Nadpis');
		#$grid->addDateColumn('datum', 'Datum akce', '%m/%d/%Y');
		#$grid->addDateColumn('zobrazdo', 'Zobraz do', '%m/%d/%Y');
		$grid->addCheckboxColumn('active', 'Zobraz');
		#$grid->addDateColumn('zmeneno', 'Naposledy změněno', '%m/%d/%Y');
		#$grid->addCheckboxColumn('povol_komentare', 'Povol komentáře');


		$renderer = new DataGridRenderer;
		$renderer->paginatorFormat = '%input%'; // customize format of paginator
		$grid->setRenderer($renderer);

		$grid->addActionColumn('Actions')->getHeaderPrototype()->style('width: 98px');
		$icon = Html::el('span');
		#$grid->addAction('Detail', 'News:default', clone $icon->class('icon icon-detail'), 1, 'asdf');
		$grid->addAction('Edit', "Novinky:edit", clone $icon->class('icon icon-edit'), FALSE, DataGridAction::WITH_KEY, array('section'=>$actionName) );
		#$grid->addAction('Delete', 'Novinky:delete', clone $icon->class('icon icon-del'));

		$this->addComponent($grid, 'newsList');
		return $grid;
	}


    /**
	 * Component factory.
	 * @param  string  component name
	 * @return void
	 */
	protected function createComponentNewsForm()
	{
			$id = $this->getParam('id');

			$menu = array();

			$form = new Form($this, 'newsForm');
			$form->addHidden('id_snippet');

			// ck editor
            $editor = $form->addTextArea('article', '')->getControlPrototype();
            $editor->class = 'ckeditor';

            $form->addSubmit('save', 'Uložit')->getControlPrototype()->class('default');

            $form->onSuccess[] = array($this, 'submittedNewsForm');
            $form->addProtection('Please submit this form again (security token has expired).');

            return $form;
	}




	/**
     * Vytvori form pro získání ID uživatele
     * @return AppForm
     */
    protected function createComponentDeleteForm()
    {
			$form = new AppForm($this, $name);

            if(isset( $_POST['items'] ))
                $ids = serialize( $_POST['items'] );
                else {
                    $ids = (string) $this->getParam('id');
                    #$ids = array( $ids );
                    $ids = serialize( $ids );
                }

            $form->addHidden('ids')->setValue( $ids );

			$form->addSubmit('delete', 'Smaž')->getControlPrototype()->class('default');
            $form->addSubmit('cancel', 'Zpět');
			$form->onSubmit[] = array($this, 'deleteFormSubmitted');

			$form->addProtection('Please submit this form again (security token has expired).');
			return $form;
	}

    /*
     * Custom group operations handler.
	 * @param  SubmitButton
	 * @return void
	 */
	public function gridOperationHandler(SubmitButton $button)
	{
		// how to findout which checkboxes in checker was checked?  $values['checker']['ID'] => bool(TRUE)
		$form = $button->getParent();
		$grid = $this->getComponent('customersGrid');

		// was submitted?
		if ($form->isSubmitted() && $form->isValid()) {
			$values = $form->getValues();

			if ($button->getName() === 'operationSubmit') {
				$operation = $values['operations'];
			} else {
				throw new InvalidArgumentException("Unknown submit button '" . $button->getName() . "'.");
			}

			$rows = array();
			foreach ($values['checker'] as $k => $v) {
				if ($v) $rows[] = $k;
			}


            #print_r( $rows );
            #exit;

			if (count($rows) > 0) {
				$msg = $grid->translate("Operation %s over row %s succesfully done.", count($rows), $grid->translate($operation), implode(', ', $rows));
				$grid->flashMessage($msg, 'success');
				$msg = $grid->translate("This is demo application only, changes will not be done.");
				$grid->flashMessage($msg, 'info');

                #$args['id_clanek']=$rows;
                #$this->redirect( 'Novinky:delete', $args );
			} else {
				$msg = $grid->translate("Není vybrán žádný záznam.");
				$grid->flashMessage($msg, 'warning');
			}
		}

		$grid->invalidateControl();
		if (!$this->presenter->isAjax()) $this->presenter->redirect('this');
	}
}
