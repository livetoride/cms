<?php
/**
 * GUI for Acl
 *
 * @copyright  Copyright (c) 2010 Tomas Marcanik
 * @package    GUI for Acl
 */

namespace AdminModule;
use AntoninRykalsky as AR;
use Nette\Application\UI\Form;
use Nette\Environment;
use SoftwareStudio\SiteSpecifics\ICustomResource;


/**
 * Login
 *
 */
class LoginPresenter extends \BasePresenter implements ICustomResource {

	/** základ pro společné oprávnění */
	var $resource = 'mlmadmin_resource';

	var $maxTry = 2;

	const MLMADMIN_PRIVILEGE = 'mlmadmin_access';
	const ACL_CACHING = false;

	public function handleShowLogin()
	{

	}

	private function getSecuredLink()
	{
		if( empty( $_SERVER["SERVER_NAME"] )) {
			# notices on console
			$_SERVER["SERVER_NAME"] = 'http://xlastrona.softwarestudio.cz';
		}

		$link = $_SERVER["SERVER_NAME"]."/login";

		$p = $this->getContext()->getParameters();
		if( isset($p['sslServers']) && !in_array( $_SERVER["SERVER_NAME"], $p['sslServers'] ))
		{
			return "http://".$link;
		} else {
			return "https://".$link;
		}
	}

	/******************
	 * Default
	 ******************/
	public function renderDefault() {
		$this->template->securedLink = $this->getSecuredLink();

		if( empty( $_SERVER["REQUEST_URI"] )) {
			# notices on console
			$_SERVER["REQUEST_URI"] = "/admin";
		}
		$this->template->showLogin = $_SERVER["REQUEST_URI"] != "/";

		$this->template->admintitle = AR\Configs::get()->byContent('general.admintitle');

		if ($this->getUser()->isAllowed($this->resource, self::MLMADMIN_PRIVILEGE)) {
			$this->redirect(':Admin:Default:default');
		}

		$lastronaGraphics = Environment::getSession('admin-user');
		if( $lastronaGraphics->loginTry>=$this->maxTry ) {
			require_once( LIBS_DIR.'/Nette.Extras/reCaptcha/recaptchalib.php' );
			$this->template->captha = recaptcha_get_html("6LekScgSAAAAAIPWb5eFwK_yV5hC6O5MnLcF_YWc");
			$this->template->showCaptcha = 1;
		}

		$this->template->title = 'Přihlaste se';
		$this->template->form = $this->getComponent('login');
	}

	protected function createComponentLogin() {
		$form = new Form($this, 'login');

		// session pro captchu
		$this->getUser()->getStorage()->setNamespace('mlmadmin');

		$renderer = $form->getRenderer();
		$renderer->wrappers['label']['suffix'] = ':';
		//$form->addGroup('Login');
		$form->addText('name', 'přihlaš.jméno', 25)
			->addRule(Form::FILLED, 'Není vyplněno přihlašovací jméno.');
		$form->addPassword('password', 'heslo', 25)
			->addRule(Form::FILLED, 'Není vyplněno heslo');

		!empty($_SERVER['SERVER_NAME']) ? $sName = $_SERVER['SERVER_NAME'] : $sName='localhost';

		$form->addHidden('page', 'http://' . $sName . $_SERVER['REQUEST_URI']);

		//$form->addProtection('Security token did not match. Possible CSRF attack.');
		$form->addSubmit('signon', 'přihlásit');
		$form->onSuccess[] = callback($this, 'SignOnFormSubmitted');
	}

	public function SignOnFormSubmitted($form)
	{
		$v = $form->getValues();
		$this->getUser()->getStorage()->setNamespace('mlmadmin');
		$s = Environment::getSession('admin-user');
		try {

			if( 1 && $s->loginTry>=$this->maxTry && !empty($_POST["recaptcha_challenge_field"]))
			{
				// CAPTCHA
				require_once( LIBS_DIR.'/Nette.Extras/reCaptcha/recaptchalib.php' );
				$privatekey = "6LekScgSAAAAAPJFIkNKHO6baAkhzrsnG1GdPtmM";
				$resp = recaptcha_check_answer ($privatekey,
					$_SERVER["REMOTE_ADDR"],
					$_POST["recaptcha_challenge_field"],
					$_POST["recaptcha_response_field"]);

				if (!$resp->is_valid) {
					// What happens when the CAPTCHA was entered incorrectly
					$this->flashMessage("Obrázkový kód nebyl opsán správně, prosím zkuste to znovu.", Flashes::$info );
					return;
				} else {
					// Your code here to handle a successful verification
				}
			}

			$credentials = array(
				'username' =>  $v['name'],
				'password' => $v['password'],
				'extra' => null
			);


			$this->getUser()->login( $credentials );
			$this->getUser()->setExpiration(10800, TRUE, TRUE); // set expiration 1 hour
			if (self::ACL_CACHING) {
				unset($this->cache['gui_acl']); // invalidate cache
			}

			//			if (!$this->getUser()->isAllowed($this->resource, self::MLMADMIN_PRIVILEGE)) {
			//				$this->getUser()->logout(TRUE); // TRUE - delete identity
			//			}
			$s['loginTry']=0;
			//$this->redirect( 'Default:' );
			$this->redirectUrl( $v['page'] );
			//$this->redirect( 'this' );
		} catch ( \Nette\Security\AuthenticationException $e ) {

			$form->addError('Zadán špatný login nebo heslo.');
			//            $form->addError( $e->__toString() );
			//            $form->setValues(array('name' => ''));
			//			if(is_numeric($s['loginTry']))
			//				$s['loginTry']+=1;
			//			else
			//				$s['loginTry'] = 1;

		}
		$this->template->show = 1;

	}

	/******************
	 * Logout
	 ******************/
	public function actionLogout() {
		//        $this->flashMessage('Sing off');
		$s = Environment::getSession('admin-user');
		unset( $s->idu );
		unset( $s->role );
		unset( $s->imgdir );
		unset( $s->clientID );

		$this->getUser()->logout(TRUE); // TRUE - delete identity

		$this->redirect('Login:default');
	}

	public function getCustomResource() { return 'news-guest'; }
	public function getCustomPrivilege() { return 'show'; }
}
