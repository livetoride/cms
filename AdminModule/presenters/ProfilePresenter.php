<?php
/**
 * Nastavení profilu
 *
 * @author Bc. Anton §n Rykalsk   <antonin.rykalsky@gmail.com>
 * @copyright  Copyright (c) 2009, 2010 Anton §n Rykalsk
 * @link       http://mlm-soft.cz
 * @package    mlm-soft.cz
 */
namespace AdminModule;

use AntoninRykalsky\Uzivatele;
use Nette\Application\UI\Form;
use Nette\Environment;

class ProfilePresenter extends \BasePresenter
{
	var $admin = 'admin-';

	var $resource = 'backend_resource';

	public function startup() {
		parent::startup();
		$this->getUser()->getStorage()->setNamespace('mlmadmin');
	}

	public function actionPassChange() {
		$this->template->title = 'Změna hesla';
        $this->template->passForm = $this->getComponent('passChange');
    }

    public function actionProfileEdit() {

		$this->template->title = 'Editace profilu';
		$idu = \AntoninRykalsky\SystemUser::get()->getIdu();
		$d = \DAO\Uzivatele::get()->find( $idu )->fetch();
		$this['profileForm']->setDefaults($d);
    }

	protected function createComponentProfileForm() {
		$form = new Form();
		$renderer = $form->getRenderer();

		$form->addText('jmeno', 'Jméno uživatele');
		$form->addText('prijmeni', 'Příjmení');
		$form->addText('telefon', 'Telefon');
		$form->addText('email', 'E-mail');
		$form->addText('nick', 'Login');

		$form->addSubmit('save', 'Uložit');
		$form->onSuccess[] = array($this, 'submitedProfileForm');
		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}


	function submitedProfileForm(Form $form) {
		if (isset($form['save']) && $form['save']->isSubmittedBy()) {
			$v = (array) $form->getValues();

			// kontrola jmena
			$duplicity = \DAO\Uzivatele::get()->findAll()->where('nick=%s', $v['nick'] )->fetchAll();
			if( count($duplicity))
			{
				$this->flashMessage("Tento login nelze použí, již je obsazen.", \Flashes::$error );
				return;
			}

			$idu = \AntoninRykalsky\SystemUser::get()->getIdu();
			$d = \DAO\Uzivatele::get()->update( $idu, $v );

	   		$s = Environment::getSession('admin-user');
	    	$s->login = $v['nick'];
			$s->name = $v['jmeno'];
			$s->surname = $v['prijmeni'];


			$this->flashMessage('Profil byl aktualizován', 'SUCCESS');

			$this->redirect( 'Profile:Default' );
		}
	}

	protected function createComponentPassChange()
	{
		$form = new Form($this, 'passChange');
		$form->addPassword('old_pass', 'Staré heslo:')
			->addRule(Form::FILLED, 'Položka staré heslo je povinná.');

		$form->addPassword('new_pass', 'Nové heslo:')
			->addRule(Form::FILLED, 'Položka nov ř heslo je povinná.');

		$form->addPassword('new_pass2', 'Potvrzení nového hesla:')
			->addRule(Form::FILLED, 'Položka potvrzení nového hesla je povinná.');


		$form->addSubmit('save', 'Uložit a použít')->getControlPrototype()->class('btn-white');
		$form->onSuccess[] = array($this, 'passChangeSubmited');

		$form->addProtection('Please submit this form again (security token has expired).');
		return $form;
	}


	public function passChangeSubmited($form)
    {
        if ($form['save']->isSubmittedBy()) {
            $v = $form->getValues();
			$idu = Environment::getSession('admin-user')->idu;

			// spatne zadané heslo
			$user = Uzivatele::get()->find( $idu )->fetch();
			if( $user->pass != md5( $v['old_pass'] ))
			{
				$this->flashMessage('Staré heslo zadáno chybně. Uživatel byl ze sysému odhlášen.', 'ERROR');
				$this->getUser()->signOut(TRUE);
				$this->redirect( 'News:default' );
			}

			// hesla se neshodují
			if( $v['new_pass'] != $v['new_pass2'])
			{
				$this->flashMessage('Hesla se neshodují', 'ERROR');
				$this->redirect( ':PassChange' );
			}

			Uzivatele::get()->update( $idu, array('pass' => md5($v['new_pass'])) );

			$this->flashMessage('Heslo bylo změněno', 'SUCCESS');

			$this->redirect( 'Profile:PassChange' );

        }
        if ($form['cancel']->isSubmittedBy()) {
            $this->redirect( 'Profile:PassChange' );
        }
    }
}
