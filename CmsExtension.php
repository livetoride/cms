<?php

namespace SoftwareStudio\Cms;

class CmsExtension extends \Nette\Config\CompilerExtension
{
    public $defaults = [];

    public function loadConfiguration()
    {
		$config = $this->getConfig($this->defaults);
		$builder = $this->getContainerBuilder();

		$this->compiler->parseServices($builder, $this->loadFromFile(__DIR__ . '/configs/cms-config.neon'));
	}

	public function registerParameters( $configurator )
	{
		$configurator->addConfig(__DIR__ . '/configs/cms-parameters.neon', false);
	}

}
